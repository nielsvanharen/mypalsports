package nl.mypalsports;

import org.apache.wicket.markup.head.CssContentHeaderItem;
import org.apache.wicket.markup.head.IHeaderResponse;
import org.apache.wicket.markup.head.JavaScriptHeaderItem;

public class ResourceReferenceUtil {

	private ResourceReferenceUtil() {

	}

	/**
	 * Voegt het menu toe.
	 * 
	 * @param response
	 */
	public static void addMenu(IHeaderResponse response) {
		response.render(JavaScriptHeaderItem
				.forUrl("assets/js/jquery-ui-1.10.3.custom.min.js"));
		response.render(JavaScriptHeaderItem
				.forUrl("assets/js/menu/superfish.js"));
		response.render(JavaScriptHeaderItem
				.forUrl("assets/js/menu/jquery.hoverIntent.js"));
		response.render(CssContentHeaderItem
				.forUrl("assets/css/menu/superfish-navbar.css"));
		response.render(CssContentHeaderItem
				.forUrl("assets/css/menu/superfish.css"));
		response.render(CssContentHeaderItem
				.forUrl("assets/css/jquery-ui-1.10.3.custom.min.css"));
	}

	/**
	 * Voegt het accordion menu toe.
	 * 
	 * @param response
	 */
	public static void addAccordionMenu(IHeaderResponse response) {
		response.render(JavaScriptHeaderItem
				.forUrl("assets/js/menu/jquery.navgoco.js"));
		response.render(JavaScriptHeaderItem
				.forUrl("assets/js/menu/jquery.cookie.js"));
		response.render(CssContentHeaderItem
				.forUrl("assets/css/menu/jquery.navgoco.css"));
	}

	/**
	 * Add Jquery Chosen reference.
	 * 
	 * @param response
	 */
	public static void addChosenDropdown(IHeaderResponse response) {
		response.render(JavaScriptHeaderItem
				.forUrl("assets/js/chosen/chosen.jquery.js"));
		response.render(JavaScriptHeaderItem
				.forUrl("assets/js/chosen/chosen.proto.min.js"));
		response.render(CssContentHeaderItem
				.forUrl("assets/css/chosen/chosen.css"));
	}

	public static void addFancyForm(IHeaderResponse response) {
		response.render(JavaScriptHeaderItem
				.forUrl("assets/js/fancyform/fancyfields-1.2.js"));
		response.render(JavaScriptHeaderItem
				.forUrl("assets/js/fancyform/fancyfields.csb.js"));
		response.render(CssContentHeaderItem
				.forUrl("assets/css/fancyform/fancyfields.css"));
	}

}
