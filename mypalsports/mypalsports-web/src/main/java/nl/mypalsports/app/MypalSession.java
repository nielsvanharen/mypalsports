package nl.mypalsports.app;

/**
 * 
 */

import java.io.Serializable;

import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

import nl.mypalsports.common.util.StringUtil;
import nl.mypalsports.entities.account.Account;
import nl.myspalsports.service.account.AccountService;

import org.apache.wicket.Session;
import org.apache.wicket.spring.injection.annot.SpringBean;

/**
 * @author Niels
 * 
 */
@Named("mypalSession")
@SessionScoped
public class MypalSession implements Serializable {

	private static final long serialVersionUID = 1L;

	@SpringBean
	private AccountService service;

	private String gebruikersnaam;

	/**
	 * @return username
	 */
	public String getGebruikersnaam() {
		return !StringUtil.isEmpty(gebruikersnaam) ? gebruikersnaam : "";
	}

	/**
	 * @param username
	 */
	public void setGebruikersnaam(String username) {
		this.gebruikersnaam = username;
	}

	/**
	 * 
	 */
	public void logout() {
		setGebruikersnaam(null);
		Session.get().invalidate();
	}

	/**
	 * @return of de gebruiker ingelogd is.
	 */
	public boolean isLoggedIn() {
		return !StringUtil.isEmpty(getGebruikersnaam());
	}

	/**
	 * @return het ingelogde account.
	 */
	public Account getLoggedInAccount() {
		if (!isLoggedIn())
			return null;
		return this.service.getAccountByGebruikersnaam(getGebruikersnaam());
	}

}
