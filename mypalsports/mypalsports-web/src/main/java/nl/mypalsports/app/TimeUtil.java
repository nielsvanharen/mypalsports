package nl.mypalsports.app;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Allerlei util methods voor tijd.
 * 
 * @author Niels
 * 
 */
public class TimeUtil {

	private TimeUtil() {
		// Exists only to defeat instantiation.
	}

	private static Logger log = LoggerFactory.getLogger(TimeUtil.class);

	private static String[] maandKeys = { "januari", "februari", "maart", "april", "mei", "juni", "juli", "augustus",
			"september", "oktober", "november", "december" };

	public static Integer getCurrentDaysOfMonth() {
		return getDaysOfMonth(Calendar.getInstance().get(Calendar.YEAR), Calendar.getInstance().get(Calendar.MONTH));
	}

	public static String getMonthAsText(int month) {
		return maandKeys[month];
	}

	public static List<Integer> getDaysOfMonthAsList(int year, int month) {
		List<Integer> daysList = new ArrayList<Integer>();
		Integer days = getDaysOfMonth(year, month);
		for (int i = 1; i <= days; i++) {
			daysList.add(Integer.valueOf(i));
		}
		return daysList;
	}

	public static Integer getDaysOfMonth(int year, int month) {
		Calendar calendar = new GregorianCalendar(year, month, 1);
		int days = calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
		return Integer.valueOf(days);
	}

	/**
	 * maanden zijn van 0 t/m 11 omdat de calendar van java begint op 0...
	 * 
	 * @return lijst van maanden.
	 */
	public static List<Integer> getMonthsOfYear() {
		List<Integer> monthList = new ArrayList<Integer>();
		for (int i = 0; i <= 11; i++) {
			monthList.add(Integer.valueOf(i));
		}
		return monthList;
	}

	public static List<Integer> getYears(int start) {
		List<Integer> yearList = new ArrayList<Integer>();
		int currentYear = Calendar.getInstance().get(Calendar.YEAR);
		if (start < currentYear) {
			for (int i = start; i < currentYear; i++) {
				yearList.add(Integer.valueOf(i));
			}
		} else {
			yearList.add(currentYear);
		}

		return yearList;
	}

	public static int getYearBeforeNow() {
		int currentYear = Calendar.getInstance().get(Calendar.YEAR);
		return currentYear - 1;
	}

	public static Date parseDate(int year, int month, int day) {
		Calendar calendar = new GregorianCalendar(year, month, day);
		calendar.setLenient(false);
		try {
			return calendar.getTime();
		} catch (IllegalArgumentException e) {
			log.warn("invalid date, returning current date");
		}
		return new Date();
	}

	public static boolean isValidDate(int year, int month, int day) {
		Calendar calendar = new GregorianCalendar(year, month, day);
		calendar.setLenient(false);
		try {
			calendar.getTime();
		} catch (IllegalArgumentException e) {
			return false;
		}
		return true;
	}
}
