package nl.mypalsports.web.components.menu;

import java.util.List;

import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.ListView;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.IModel;

/**
 * Panel voor een menu item.
 * 
 * @author Niels
 * 
 */
public abstract class BaseMenuItemPanel extends Panel {

	private static final long serialVersionUID = 1L;

	private IMenuItem item;

	private String iconClass;

	public BaseMenuItemPanel(IMenuItem menuItem, String iconClass) {
		super("mainMenuItem");
		this.iconClass = iconClass;
		this.item = menuItem;
		ListView<BaseMenuItemLinkPanel> menuItems = new ListView<BaseMenuItemLinkPanel>(
				"mainMenuItemLinks", createMenuLinkPanels()) {
			private static final long serialVersionUID = 1L;

			@Override
			protected void populateItem(ListItem<BaseMenuItemLinkPanel> item) {
				item.add(item.getModelObject());
			}
		};
		add(menuItems);
	}

	public abstract List<BaseMenuItemLinkPanel> createMenuLinkPanels();

	public IMenuItem getItem() {
		return item;
	}

	public IModel<String> getLabel() {
		return getItem().getLabel();
	}

	public String getIconClass() {
		return iconClass;
	}

}
