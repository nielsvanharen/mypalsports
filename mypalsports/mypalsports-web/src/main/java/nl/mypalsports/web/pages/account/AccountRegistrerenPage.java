package nl.mypalsports.web.pages.account;

import nl.mypalsports.web.components.container.DefaultContainer;
import nl.mypalsports.web.components.menu.SuperFishMenuItem;
import nl.mypalsports.web.components.menu.account.RegistratieMenu;
import nl.mypalsports.web.pages.BasePage;
import nl.mypalsports.web.pages.account.stappen.RegistratieContainer;

import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.model.ResourceModel;

/**
 * Account registratie pagina.
 * 
 * @author Niels
 * 
 */
public class AccountRegistrerenPage extends BasePage {

	private static final long serialVersionUID = 1L;

	public AccountRegistrerenPage() {
		super(SuperFishMenuItem.REGISTREREN);
		RegistratieContainer stappenContainer = new RegistratieContainer("stappen", new ResourceModel(
				"registreren.stappen")) {

			private static final long serialVersionUID = 1L;

			@Override
			public WebMarkupContainer createContent(String id) {
				return new RegistratieMenu(id);
			}

			@Override
			public String getCssClass() {
				return "container350w450h";
			}
		};
		add(stappenContainer);
		add(new RegistratieContainer("registratie", new ResourceModel("registreren.type")));
	}

}
