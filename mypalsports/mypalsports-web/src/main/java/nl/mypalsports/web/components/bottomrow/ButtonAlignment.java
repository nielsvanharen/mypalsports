package nl.mypalsports.web.components.bottomrow;

/**
 * Alignment voor buttons.
 * 
 * @author Niels
 * 
 */
public enum ButtonAlignment {

	LEFT,

	RIGHT,

	CENTER;
}
