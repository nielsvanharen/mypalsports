package nl.mypalsports.web.components.menu.main;

import nl.mypalsports.web.components.menu.SuperFishMenuItem;
import nl.mypalsports.web.components.menu.SuperFishMenuItemPanel;

import org.apache.wicket.markup.html.WebMarkupContainer;

public class MogelijkHedenMainMenuItem extends SuperFishMenuItemPanel {

	private static final long serialVersionUID = 1L;

	public MogelijkHedenMainMenuItem() {
		super(SuperFishMenuItem.MOGELIJKHEDEN);
	}

	@Override
	protected WebMarkupContainer getLink(String id) {
		return new WebMarkupContainer(id);
	}

}
