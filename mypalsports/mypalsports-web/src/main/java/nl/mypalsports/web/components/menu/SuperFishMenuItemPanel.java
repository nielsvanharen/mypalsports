package nl.mypalsports.web.components.menu;

import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.IModel;

/**
 * Panel voor een menu item.
 * 
 * @author Niels
 * 
 */
public abstract class SuperFishMenuItemPanel extends Panel {

	private static final long serialVersionUID = 1L;

	private IMenuItem item;

	private boolean active = false;

	private WebMarkupContainer link;

	public SuperFishMenuItemPanel(IMenuItem menuItem) {
		super("superfishMenuItem");
		this.item = menuItem;
		this.link = getLink("link");
		this.link.add(new Label("lbl", getLabel()));
		add(this.link);

	}
	protected abstract WebMarkupContainer getLink(String id);

	public IMenuItem getItem() {
		return item;
	}

	public IModel<String> getLabel() {
		return getItem().getLabel();
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}
	public WebMarkupContainer getLink() {
		return link;
	}

}
