package nl.mypalsports.web.components.buttons;

import nl.mypalsports.web.components.bottomrow.BottomRowPanel;
import nl.mypalsports.web.components.bottomrow.ButtonAlignment;

import org.apache.wicket.Page;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.link.IPageLink;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;

public class AbstractPageBottomRowButton extends AbstractBottomRowButton {

	private static final long serialVersionUID = 1L;

	private IPageLink pageLink;

	private Class<? extends Page> pageClass;

	private Page pageInstance;

	public AbstractPageBottomRowButton(BottomRowPanel bottomRow, String label,
			ButtonAlignment alignment) {
		this(bottomRow, new Model<String>(label), alignment);
	}

	public AbstractPageBottomRowButton(BottomRowPanel bottomRow,
			IModel<String> label, ButtonAlignment alignment) {
		super(bottomRow, label, alignment);
	}

	public AbstractPageBottomRowButton(BottomRowPanel bottomRow, String label,
			ButtonAlignment alignment, IPageLink pageLink) {
		this(bottomRow, new Model<String>(label), alignment, pageLink);
	}

	public AbstractPageBottomRowButton(BottomRowPanel bottomRow,
			IModel<String> label, ButtonAlignment alignment, IPageLink pageLink) {
		super(bottomRow, label, alignment);
		if (pageLink == null)
			throw new NullPointerException("pageLink cannot be null");
		this.pageLink = pageLink;
	}

	public AbstractPageBottomRowButton(BottomRowPanel bottomRow, String label,
			ButtonAlignment alignment, final Page page) {
		this(bottomRow, new Model<String>(label), alignment, page);
	}

	public AbstractPageBottomRowButton(BottomRowPanel bottomRow,
			IModel<String> label, ButtonAlignment alignment, final Page page) {
		super(bottomRow, label, alignment);
		if (page == null)
			throw new NullPointerException("page cannot be null");
		this.pageInstance = page;
	}

	public AbstractPageBottomRowButton(BottomRowPanel bottomRow, String label,
			ButtonAlignment alignment, Class<? extends Page> pageClass) {
		this(bottomRow, new Model<String>(label), alignment, pageClass);
	}

	public AbstractPageBottomRowButton(BottomRowPanel bottomRow,
			IModel<String> label, ButtonAlignment alignment,
			Class<? extends Page> pageClass) {
		super(bottomRow, label, alignment);
		if (pageClass == null)
			throw new NullPointerException("pageClass cannot be null");
		this.pageClass = pageClass;
	}

	public IPageLink getPageLink() {
		return pageLink;
	}

	public AbstractBottomRowButton setPageLink(IPageLink pageLink) {
		this.pageLink = pageLink;
		return this;
	}

	public Class<? extends Page> getPageClass() {
		return pageClass;
	}

	public AbstractBottomRowButton setPageClass(Class<? extends Page> pageClass) {
		this.pageClass = pageClass;
		return this;
	}

	public Page getPageInstance() {
		return pageInstance;
	}

	public void setPageInstance(Page page) {
		this.pageInstance = page;
	}

	@Override
	protected WebMarkupContainer getLink(String linkId) {
		// Page page = getPageInstance();
		// if (page != null)
		// return new TargetBasedSecurePageLink<Object>(linkId, page);
		//
		// IPageLink link = getPageLink();
		// if (link != null)
		// return new TargetBasedSecurePageLink<Object>(linkId, link);
		//
		// if (getPageClass() != null)
		// return new TargetBasedSecureBookmarkablePageLink<Object>(linkId,
		// getPageClass());
		// throw new IllegalStateException(
		// "Er is geen page, geen link en geen class");
		return new WebMarkupContainer(linkId);
	}

	public void followLink() {
		if (isVisible()) {
			Page page = getPageInstance();
			IPageLink link = getPageLink();
			if (page != null)
				setResponsePage(page);
			else if (link != null)
				setResponsePage(link.getPage());
			else
				setResponsePage(getPageClass());
		}
	}
}