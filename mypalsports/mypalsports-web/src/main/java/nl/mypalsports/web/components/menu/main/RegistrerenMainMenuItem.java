package nl.mypalsports.web.components.menu.main;

import nl.mypalsports.web.components.menu.SuperFishMenuItem;
import nl.mypalsports.web.components.menu.SuperFishMenuItemPanel;
import nl.mypalsports.web.pages.account.AccountRegistrerenPage;

import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.link.Link;

public class RegistrerenMainMenuItem extends SuperFishMenuItemPanel {

	private static final long serialVersionUID = 1L;

	public RegistrerenMainMenuItem() {
		super(SuperFishMenuItem.REGISTREREN);
	}

	@Override
	protected WebMarkupContainer getLink(String id) {
		return new Link<Void>(id) {

			private static final long serialVersionUID = 1L;

			@Override
			public void onClick() {
				setResponsePage(new AccountRegistrerenPage());
			}

		};
	}

}
