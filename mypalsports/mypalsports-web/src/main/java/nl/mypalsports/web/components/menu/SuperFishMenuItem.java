package nl.mypalsports.web.components.menu;

import org.apache.wicket.model.IModel;
import org.apache.wicket.model.ResourceModel;
import org.apache.wicket.util.lang.Classes;

public enum SuperFishMenuItem implements IMenuItem {

	HOME,

	OVERZICHT,

	MOGELIJKHEDEN,

	DEMO,

	REGISTREREN,

	MYACCOUNT;

	@Override
	public IModel<String> getLabel() {
		return new ResourceModel(Classes.simpleName(getDeclaringClass()) + '.'
				+ name(), name());
	}

}
