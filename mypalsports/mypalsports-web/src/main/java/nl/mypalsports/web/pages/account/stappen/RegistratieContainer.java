package nl.mypalsports.web.pages.account.stappen;

import nl.mypalsports.web.components.container.AbstractContainer;

import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.model.IModel;

public class RegistratieContainer extends AbstractContainer {

	private static final long serialVersionUID = 1L;

	public RegistratieContainer(String id, IModel<String> title) {
		super(id, title);
	}

	@Override
	public WebMarkupContainer createContent(String id) {
		return new RegistratieWizardPanel(id);
	}
	
	@Override
	public String getCssClass() {
		return "container700w";
		
	}
	
	
	@Override
	public String getContentCssClass() {
		return "registratiecontent";
	}

}
