package nl.mypalsports.web.components.buttons;

import nl.mypalsports.web.components.bottomrow.BottomRowPanel;
import nl.mypalsports.web.components.bottomrow.ButtonAlignment;

import org.apache.wicket.AttributeModifier;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.AbstractReadOnlyModel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;

public abstract class AbstractBottomRowButton extends Panel {

	private static final long serialVersionUID = 1L;

	private WebMarkupContainer link;

	private IModel<String> label;

	private ButtonAlignment alignment;

	public AbstractBottomRowButton(BottomRowPanel bottomRow, String label,
			ButtonAlignment alignment) {
		this(bottomRow, new Model<String>(label), alignment);
	}

	public AbstractBottomRowButton(BottomRowPanel bottomRow,
			IModel<String> label, ButtonAlignment alignment) {
		super(bottomRow.getNextButtonId());
		this.label = label;
		this.alignment = alignment;
	}

	@Override
	protected void onBeforeRender() {
		super.onBeforeRender();
		if (!hasBeenRendered()) {
			addOrReplace(link = getLink("link"));
			link.add(getTitleModifier());
			link.add(new Label("label", new AbstractReadOnlyModel<String>() {
				private static final long serialVersionUID = 1L;

				@Override
				public String getObject() {
					return getLabel();
				}
			}));

		}
		link.setEnabled(isEnabled());
	}

	protected AttributeModifier getTitleModifier() {
		return new AttributeModifier("title",
				new AbstractReadOnlyModel<String>() {
					private static final long serialVersionUID = 1L;

					@Override
					public String getObject() {
						return getLabel();
					}
				});
	}

	protected abstract WebMarkupContainer getLink(String linkId);

	public String getLabel() {
		return getLabelModel().getObject();
	}

	public IModel<String> getLabelModel() {
		return label;
	}

	public AbstractBottomRowButton setLabel(String label) {
		return setLabel(new Model<String>(label));
	}

	public AbstractBottomRowButton setLabel(IModel<String> label) {
		this.label = label;
		return this;
	}

	public ButtonAlignment getAlignment() {
		return alignment;
	}

	public AbstractBottomRowButton setAlignment(ButtonAlignment alignment) {
		this.alignment = alignment;
		return this;
	}

	@Override
	protected void onDetach() {
		super.onDetach();
		label.detach();
	}

}
