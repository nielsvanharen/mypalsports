package nl.mypalsports.web.components.bottomrow;

import nl.mypalsports.web.components.buttons.AbstractBottomRowButton;

import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.markup.repeater.RepeatingView;

/**
 * Container voor buttons.
 * 
 * @author Niels
 * 
 */
public class BottomRowPanel extends Panel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private RepeatingView leftButtons;

	private RepeatingView rightButtons;

	private RepeatingView centerButtons;

	private int buttonCount = 0;

	public BottomRowPanel(String id) {
		super(id);
		add(leftButtons = new RepeatingView("leftButtons"));
		add(centerButtons = new RepeatingView("centerButtons"));
		add(rightButtons = new RepeatingView("rightButtons"));
	}

	public String getNextButtonId() {
		return Integer.toString(buttonCount++);
	}

	public void addButton(AbstractBottomRowButton button) {
		if (ButtonAlignment.LEFT.equals(button.getAlignment())) {
			leftButtons.add(button);
		} else if (ButtonAlignment.CENTER.equals(button.getAlignment())) {
			centerButtons.add(button);
		} else {
			rightButtons.add(button);
		}
	}

	@Override
	protected void onBeforeRender() {
		super.onBeforeRender();

		// if (Application.DEVELOPMENT.equals(getApplication()
		// .getConfigurationType())) {
		// visitChildren(new IVisitor<Component>() {
		// @Override
		// public Object component(Component component) {
		// if (component instanceof AbstractBottomRowButton) {
		// throw new WicketRuntimeException(
		// "Button "
		// + component
		// + " direct aan panel toegevoegd ipv via BottomRowPanel#addButton");
		// }
		// return IVisitor.CONTINUE_TRAVERSAL_BUT_DONT_GO_DEEPER;
		// }
		// });
		// }
	}

}
