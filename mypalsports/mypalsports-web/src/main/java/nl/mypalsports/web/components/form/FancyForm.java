package nl.mypalsports.web.components.form;

import nl.mypalsports.ResourceReferenceUtil;

import org.apache.wicket.Component;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.markup.head.IHeaderResponse;
import org.apache.wicket.markup.head.OnDomReadyHeaderItem;
import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.model.IModel;
import org.apache.wicket.util.string.StringValue;
import org.apache.wicket.util.visit.IVisit;
import org.apache.wicket.util.visit.IVisitor;

/**
 * Form met fancyform jquery support. Dit form doet een onselection changed
 * update for selects. Dit wordt vervolgens in wicket naar de juiste dropdown
 * gestuurd.
 * 
 * @author Niels
 * 
 * @param <T>
 */
public class FancyForm<T> extends Form<T> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	FancyFormSelectBehaviour behaviour;

	public FancyForm(String id, IModel<T> model) {
		super(id, model);
		setOutputMarkupId(true);
		this.behaviour = new FancyFormSelectBehaviour() {

			private static final long serialVersionUID = -7212271627885048710L;

			@Override
			public void onSelectionChanged(final AjaxRequestTarget target, final StringValue newValue,
					final String componentId) {
				visitChildren(new IVisitor<Component, String>() {

					@Override
					public void component(Component object, IVisit<String> visit) {
						if (object instanceof FancyDropDownChoice) {
							FancyDropDownChoice<?> choice = (FancyDropDownChoice<?>) object;
							if (choice.getMarkupId().equals(componentId)) {
								choice.onSelectionChanged(target, newValue);
								visit.dontGoDeeper();
							}
						}
					}
				});
			}
		};
		add(this.behaviour);
	}

	@Override
	public void renderHead(IHeaderResponse response) {
		super.renderHead(response);
		ResourceReferenceUtil.addFancyForm(response);
		response.render(OnDomReadyHeaderItem.forScript("$('#" + getMarkupId()
				+ "').fancyfields({onSelectChange: function (input,text,val){ " + "var wcall = Wicket.Ajax.get({u: \""
				+ behaviour.getCallbackUrl()
				+ "\",c: input.attr(\"id\"),ep:{'newValue':val,'componentId' :input.attr(\"id\")}})" + "}})"));
	}

	@Override
	protected void onBeforeRender() {
		super.onBeforeRender();
		visitChildren(new IVisitor<Component, String>() {

			@Override
			public void component(Component object, IVisit<String> visit) {
				if (object instanceof DropDownChoice) {
					if (!(object instanceof FancyDropDownChoice)) {
						throw new FancyDropDownChoiceException(
								"Alleen FancyDropdownChoices zijn toegestaan op dit form");
					}
				}
			}
		});
	}

}
