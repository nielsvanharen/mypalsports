package nl.mypalsports.web.components.form;

import java.util.List;

import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.markup.html.form.IChoiceRenderer;
import org.apache.wicket.model.IModel;
import org.apache.wicket.util.string.StringValue;

/**
 * Dropdown voor een FancyForm. Doet ajax afhandeling en update het model. Het
 * component zelf is niet met target.add() te gebruiken omdat fancy forms een
 * wrapper gebruikt.
 * 
 * @author Niels
 * 
 * @param <T>
 */
public class FancyDropDownChoice<T> extends DropDownChoice<T> {

	private static final long serialVersionUID = 1L;

	public FancyDropDownChoice(String id, IModel<T> model, IModel<? extends List<? extends T>> choices) {
		super(id, model, choices);
		if (!isNullValid()) {
			setChoiceRenderer(new IChoiceRenderer<T>() {

				private static final long serialVersionUID = 1L;

				@Override
				public Object getDisplayValue(T object) {
					return object;
				}

				@Override
				public String getIdValue(T object, int index) {
					return String.valueOf(index + 1);
				}

			});
		}
	}

	public FancyDropDownChoice(String id, IModel<? extends List<? extends T>> choices,
			IChoiceRenderer<? super T> renderer) {
		super(id, choices, renderer);
	}

	public FancyDropDownChoice(String id, IModel<T> model, IModel<? extends List<? extends T>> choices,
			IChoiceRenderer<? super T> renderer) {
		super(id, model, choices, renderer);
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public void onSelectionChanged(AjaxRequestTarget target, StringValue newValue) {
		T obj = getChoiceModel().getObject();
		if (obj instanceof Integer) {
			setModelObject((T) newValue.toInteger());
		} else if (obj instanceof String) {
			setModelObject((T) newValue.toString());
		} else if (obj instanceof Enum) {
			setModelObject((T) newValue.toEnum((Class) obj.getClass()));
		} else {
			throw new FancyDropDownChoiceException("type of " + obj.getClass().getName() + " not supported yet");
		}
		onSelectionChanged(target, getChoiceModel().getObject());
	}

	public void onSelectionChanged(AjaxRequestTarget target, T newValue) {

	}

	public void setOptions(IModel<List<T>> choices, AjaxRequestTarget target) {
		List<T> options = choices.getObject();
		StringBuilder builder = new StringBuilder();
		builder.append("[");
		for (T option : options) {
			builder.append("[\"" + option + "\",\"" + option + "\"]");
			builder.append(",");
		}
		builder = builder.deleteCharAt(builder.length() - 1);
		builder.append("]");
		target.appendJavaScript("$('#" + getMarkupId() + "').setOptions(" + builder.toString() + ");");
	}

	@SuppressWarnings("unchecked")
	public IModel<T> getChoiceModel() {
		return (IModel<T>) getDefaultModel();
	}

	protected void onUpdate(AjaxRequestTarget target) {

	}

}
