package nl.mypalsports.web.components.form;

import javax.validation.groups.Default;

import nl.mypalsports.web.validators.BeanPropertyValidator;

import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.model.IModel;

/**
 * Textfield met JSR303 validation.
 * 
 * @author Niels
 * 
 * @param <T>
 */
public class JSR303TextField<T> extends TextField<T> {

	private static final long serialVersionUID = 1L;

	/**
	 * 
	 * @param id
	 * @param clazz
	 * @param model
	 * @param propertyName
	 * @param groups
	 */
	public JSR303TextField(String id, Class<?> clazz, IModel<T> model, String propertyName) {
		super(id, model);
		add(new BeanPropertyValidator<T>(clazz, propertyName, Default.class));
	}

	/**
	 * 
	 * @param id
	 * @param clazz
	 * @param model
	 * @param propertyName
	 * @param groups
	 */
	public JSR303TextField(String id, Class<?> clazz, IModel<T> model, String propertyName, Class<?>[] groups) {
		super(id, model);
		add(new BeanPropertyValidator<T>(clazz, propertyName, groups));
	}
}
