package nl.mypalsports.web.components.buttons;

import nl.mypalsports.web.components.bottomrow.BottomRowPanel;
import nl.mypalsports.web.components.bottomrow.ButtonAlignment;

import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;

public abstract class AbstractLinkButton extends AbstractBottomRowButton {
	private static final long serialVersionUID = 1L;

	public AbstractLinkButton(BottomRowPanel bottomRow, String label,
			ButtonAlignment alignment) {
		this(bottomRow, new Model<String>(label), alignment);
	}

	public AbstractLinkButton(BottomRowPanel bottomRow, IModel<String> label,
			ButtonAlignment alignment) {
		super(bottomRow, label, alignment);
	}

	@Override
	protected WebMarkupContainer getLink(String linkId) {
		return new Link<Void>(linkId) {
			private static final long serialVersionUID = 1L;

			@Override
			public void onClick() {
				AbstractLinkButton.this.onClick();
			}
		};
	}

	protected abstract void onClick();
}
