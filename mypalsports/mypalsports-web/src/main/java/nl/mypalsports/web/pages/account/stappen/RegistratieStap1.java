package nl.mypalsports.web.pages.account.stappen;

import java.util.Arrays;

import nl.mypalsports.app.TimeUtil;
import nl.mypalsports.entities.persoon.Persoon;
import nl.mypalsports.entities.persoon.Persoon.AanhefEnum;
import nl.mypalsports.web.components.form.JSR303TextField;
import nl.mypalsports.web.components.form.MaandRenderer;

import org.apache.wicket.extensions.wizard.WizardStep;
import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.markup.html.form.EnumChoiceRenderer;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.model.ResourceModel;
import org.apache.wicket.model.util.ListModel;

public class RegistratieStap1 extends WizardStep {

	private static final long serialVersionUID = 1L;

	private Integer day = 1;

	private Integer month = 0;

	private DropDownChoice<Integer> dayChoice;

	private Integer year = TimeUtil.getYearBeforeNow();

	private FeedbackPanel feedbackPanel;

	public RegistratieStap1(IModel<Persoon> model) {
		super(new ResourceModel("registratie.persoonlijkegegevens"), new ResourceModel(
				"registratie.persoonlijkegegevens.samenvatting"), model);
		Form<Persoon> form = new Form<>("form", getContextModel());
		this.feedbackPanel = new FeedbackPanel("feedbackpanel");
		this.feedbackPanel.setOutputMarkupId(true);

		DropDownChoice<AanhefEnum> choice = createAanhefDropDown();
		this.dayChoice = createDayChoice();
		DropDownChoice<Integer> monthChoice = createMonthChoice();
		DropDownChoice<Integer> yearChoice = createYearChoice();
		JSR303TextField<String> titel = createTitel();
		JSR303TextField<String> voorvoegsel = createVoorvoegsel();
		JSR303TextField<String> voorletters = createVoorletters();
		JSR303TextField<String> voornaam = createVoornaam();
		JSR303TextField<String> achternaam = createAchternaam();

		form.add(choice);
		form.add(titel);
		form.add(voorvoegsel);
		form.add(voorletters);
		form.add(voornaam);
		form.add(achternaam);
		form.add(this.dayChoice);
		form.add(monthChoice);
		form.add(yearChoice);
		form.add(this.feedbackPanel);
		add(form);
	}

	private JSR303TextField<String> createTitel() {
		JSR303TextField<String> titel = new JSR303TextField<String>("titel", Persoon.class, new PropertyModel<String>(
				getContextModel(), "titel"), "titel");
		titel.setRequired(true);
		return titel;
	}

	private JSR303TextField<String> createVoorvoegsel() {
		JSR303TextField<String> titel = new JSR303TextField<String>("voorvoegsel", Persoon.class,
				new PropertyModel<String>(getContextModel(), "voorvoegsel"), "voorvoegsel");
		return titel;
	}

	private JSR303TextField<String> createVoorletters() {
		JSR303TextField<String> titel = new JSR303TextField<String>("voorletters", Persoon.class,
				new PropertyModel<String>(getContextModel(), "voorletters"), "voorletters");
		return titel;
	}

	private JSR303TextField<String> createVoornaam() {
		JSR303TextField<String> titel = new JSR303TextField<String>("voornaam", Persoon.class,
				new PropertyModel<String>(getContextModel(), "voornaam"), "voornaam");
		titel.setRequired(true);
		return titel;
	}

	private JSR303TextField<String> createAchternaam() {
		JSR303TextField<String> titel = new JSR303TextField<String>("achternaam", Persoon.class,
				new PropertyModel<String>(getContextModel(), "achternaam"), "achternaam");
		titel.setRequired(true);
		return titel;
	}

	private DropDownChoice<AanhefEnum> createAanhefDropDown() {
		DropDownChoice<AanhefEnum> choice = new DropDownChoice<AanhefEnum>("aanhef",
				new PropertyModel<AanhefEnum>(getDefaultModel(), "aanhef"), new ListModel<AanhefEnum>(
						Arrays.asList(AanhefEnum.values())), new EnumChoiceRenderer<AanhefEnum>(this));
		choice.setNullValid(false);
		choice.setRequired(true);
		return choice;
	}

	private DropDownChoice<Integer> createDayChoice() {
//		FancyDropDownChoice<Integer> choice = new FancyDropDownChoice<Integer>("day", new PropertyModel<Integer>(this,
//				"day"), new ListModel<Integer>(TimeUtil.getDaysOfMonthAsList(getYear(), getMonth()))) {
//
//			private static final long serialVersionUID = 6437322797935693066L;
//
//			@Override
//			public void onSelectionChanged(AjaxRequestTarget target, Integer newValue) {
//				if (dateValid()) {
//					getContextModel().getObject().setGeboortedatum(TimeUtil.parseDate(getYear(), getMonth(), getDay()));
//				}
//				target.add(getFeedbackPanel());
//			}
//		};
		DropDownChoice<Integer> choice = new DropDownChoice<Integer>("day", new PropertyModel<Integer>(this,
				"day"), new ListModel<Integer>(TimeUtil.getDaysOfMonthAsList(getYear(), getMonth())));
		choice.setNullValid(false);
		choice.setRequired(true);
		choice.setOutputMarkupId(true);
		return choice;
	}

	private DropDownChoice<Integer> createMonthChoice() {
		// FancyDropDownChoice<Integer> choice = new
		// FancyDropDownChoice<Integer>("month", new PropertyModel<Integer>(
		// this, "month"), new ListModel<Integer>(TimeUtil.getMonthsOfYear()),
		// new MaandRenderer()) {
		//
		// private static final long serialVersionUID = 1L;
		//
		// @Override
		// public void onSelectionChanged(AjaxRequestTarget target, Integer
		// value) {
		// if (dateValid()) {
		// getContextModel().getObject().setGeboortedatum(TimeUtil.parseDate(getYear(),
		// getMonth(), getDay()));
		// }
		// target.add(getFeedbackPanel());
		// }
		// };

		DropDownChoice<Integer> choice = new DropDownChoice<Integer>("month",
				new PropertyModel<Integer>(this, "month"), new ListModel<Integer>(TimeUtil.getMonthsOfYear()),
				new MaandRenderer());
		choice.setNullValid(false);
		choice.setRequired(true);
		choice.setOutputMarkupId(true);
		return choice;
	}

	private DropDownChoice<Integer> createYearChoice() {
		// FancyDropDownChoice<Integer> choice = new
		// FancyDropDownChoice<Integer>("year", new PropertyModel<Integer>(this,
		// "year"), new ListModel<Integer>(TimeUtil.getYears(1900))) {
		//
		// private static final long serialVersionUID = -2771451826019815213L;
		//
		// @Override
		// public void onSelectionChanged(AjaxRequestTarget target, Integer
		// newValue) {
		// if (dateValid()) {
		// getContextModel().getObject().setGeboortedatum(TimeUtil.parseDate(getYear(),
		// getMonth(), getDay()));
		// }
		// target.add(getFeedbackPanel());
		// }
		//
		// };
		DropDownChoice<Integer> choice = new DropDownChoice<Integer>("year", new PropertyModel<Integer>(this, "year"),
				new ListModel<Integer>(TimeUtil.getYears(1900)));
		choice.setNullValid(false);
		choice.setRequired(true);
		choice.setOutputMarkupId(true);
		return choice;
	}

	@SuppressWarnings("unchecked")
	public IModel<Persoon> getContextModel() {
		return (IModel<Persoon>) getDefaultModel();
	}

	public DropDownChoice<Integer> getDayChoice() {
		return dayChoice;
	}

	public Integer getMonth() {
		return month;
	}

	public Integer getYear() {
		return year;
	}

	public Integer getDay() {
		return day;
	}

	public FeedbackPanel getFeedbackPanel() {
		return feedbackPanel;
	}

	protected boolean dateValid() {
		if (!TimeUtil.isValidDate(getYear(), getMonth(), getDay())) {
			error("de gekozen datum is ongeldig");
			return false;
		}
		return true;
	}
}
