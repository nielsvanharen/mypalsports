package nl.mypalsports.web.components.form;

import org.apache.wicket.WicketRuntimeException;

/**
 * Exception voor fancydropdown fouten.
 * 
 * @author Niels
 * 
 */
public class FancyDropDownChoiceException extends WicketRuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public FancyDropDownChoiceException(String message) {
		super(message);
	}
}