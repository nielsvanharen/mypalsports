package nl.mypalsports.web.components.menu;

import org.apache.wicket.model.IModel;

/**
 * Interface voor een menuitem
 * 
 * @author Niels
 * 
 */
public interface IMenuItem {

	IModel<String> getLabel();
}
