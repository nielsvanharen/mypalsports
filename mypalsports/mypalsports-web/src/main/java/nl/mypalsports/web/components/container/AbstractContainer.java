package nl.mypalsports.web.components.container;

import nl.mypalsports.web.components.bottomrow.BottomRowPanel;

import org.apache.wicket.AttributeModifier;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.IModel;

/**
 * Panel met inhoud en ronde hoeken.
 * 
 * @author Niels
 * 
 */
public abstract class AbstractContainer extends Panel {

	private static final long serialVersionUID = 1L;

	private BottomRowPanel bottomRowPanel;

	private WebMarkupContainer content;

	public AbstractContainer(String id, IModel<String> title) {
		super(id);
		add(new Label("title", title));
		this.content = createContent("content");
		add(this.content);
	}

	@Override
	protected void onBeforeRender() {
		if (!hasBeenRendered()) {
			getContent().add(AttributeModifier.replace("class", getContentCssClass()));
			add(AttributeModifier.append("class", getCssClass()));
			this.bottomRowPanel = new BottomRowPanel("bottomrow");
			add(bottomRowPanel);
			fillBottomRow(bottomRowPanel);
		}
		super.onBeforeRender();
	}

	public abstract WebMarkupContainer createContent(String id);

	/**
	 * het knoppen panel.
	 * 
	 * @param panel
	 */
	protected void fillBottomRow(BottomRowPanel panel) {

	}

	public BottomRowPanel getBottomRowPanel() {
		return bottomRowPanel;
	}

	public String getCssClass() {
		return "containercontent";
	}
	
	public String getContentCssClass() {
		return "containercontent";
	}

	private WebMarkupContainer getContent() {
		return content;

	}

}
