package nl.mypalsports.web.components.menu;

import java.util.ArrayList;
import java.util.List;

import nl.mypalsports.web.components.menu.main.AccountMainMenuItem;
import nl.mypalsports.web.components.menu.main.DemoMainMenuItem;
import nl.mypalsports.web.components.menu.main.HomeMainMenuItem;
import nl.mypalsports.web.components.menu.main.MogelijkHedenMainMenuItem;
import nl.mypalsports.web.components.menu.main.OverzichtMainMenuItem;
import nl.mypalsports.web.components.menu.main.RegistrerenMainMenuItem;

import org.apache.wicket.AttributeModifier;
import org.apache.wicket.markup.head.IHeaderResponse;
import org.apache.wicket.markup.head.JavaScriptHeaderItem;
import org.apache.wicket.markup.head.OnDomReadyHeaderItem;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.ListView;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.Model;

public class SuperFishMenu extends Panel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private WebMarkupContainer superfishMenu;

	private SuperFishMenuItem selectedItem;

	public SuperFishMenu(String id, SuperFishMenuItem selectedItem) {
		super(id);
		this.selectedItem = selectedItem;
	}

	@Override
	protected void onInitialize() {
		super.onInitialize();
		this.superfishMenu = new WebMarkupContainer("superfishMenu");
		List<SuperFishMenuItemPanel> items = new ArrayList<SuperFishMenuItemPanel>();
		items.add(new HomeMainMenuItem());
		items.add(new OverzichtMainMenuItem());
		items.add(new MogelijkHedenMainMenuItem());
		items.add(new DemoMainMenuItem());
		items.add(new RegistrerenMainMenuItem());
		items.add(new AccountMainMenuItem());
		ListView<SuperFishMenuItemPanel> menuItems = new ListView<SuperFishMenuItemPanel>("superfishMenuItems", items) {
			private static final long serialVersionUID = 1L;

			@Override
			protected void populateItem(ListItem<SuperFishMenuItemPanel> item) {
				SuperFishMenuItemPanel panel = item.getModelObject();
				if (getSelectedMenuItem().equals(panel.getItem())) {
					panel.getLink().add(AttributeModifier.replace("class", new Model<String>("active")));
				} else {
					panel.getLink().add(AttributeModifier.replace("class", new Model<String>("inactive")));
				}
				item.add(panel);
			}
		};
		superfishMenu.add(menuItems);
		add(superfishMenu);
	}

	public SuperFishMenuItem getSelectedMenuItem() {
		return this.selectedItem;
	}

	/**
	 * Responsive menu.
	 */
	@Override
	public void renderHead(IHeaderResponse response) {
		super.renderHead(response);
		response.render(JavaScriptHeaderItem.forReference(getApplication().getJavaScriptLibrarySettings()
				.getJQueryReference()));
		response.render(OnDomReadyHeaderItem
				.forScript(" $('#pull').on('click', function(e) {  e.preventDefault();  $('nav ul').slideToggle();  })"));
		response.render(OnDomReadyHeaderItem
				.forScript("$(window).resize(function(){  var w = $(window).width();  if(w > 320 && $('nav ul').is(':hidden')) {  $('nav ul').removeAttr('style');  }  })"));
	}

}
