package nl.mypalsports.web.components.menu.account;

import nl.mypalsports.web.components.menu.IMenuItem;

import org.apache.wicket.model.IModel;
import org.apache.wicket.model.ResourceModel;
import org.apache.wicket.util.lang.Classes;

/**
 * Stappen bij het registreren.
 * 
 * @author Niels
 * 
 */
public enum RegistratieMenuItem implements IMenuItem {

	TYPEACCOUNT,

	SPORTGEGEVENS,

	BETALINGSMETHODE,

	AFRONDEN;

	@Override
	public IModel<String> getLabel() {
		return new ResourceModel(Classes.simpleName(getDeclaringClass()) + '.'
				+ name(), name());
	}

}
