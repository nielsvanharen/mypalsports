package nl.mypalsports.web.exception;

import org.apache.wicket.WicketRuntimeException;

/**
 * Exception class voor de default container.
 * 
 * @author Niels
 * 
 */
public class ContainerDimensionException extends WicketRuntimeException {

	private static final long serialVersionUID = 1L;

	public ContainerDimensionException(int width) {
		super("Een breedte van" + width + " px is niet toegestaan");
	}

}
