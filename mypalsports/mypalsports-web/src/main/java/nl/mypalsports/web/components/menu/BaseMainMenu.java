package nl.mypalsports.web.components.menu;

import java.util.List;

import nl.mypalsports.ResourceReferenceUtil;

import org.apache.wicket.AttributeModifier;
import org.apache.wicket.markup.head.IHeaderResponse;
import org.apache.wicket.markup.head.JavaScriptHeaderItem;
import org.apache.wicket.markup.head.OnDomReadyHeaderItem;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.ListView;
import org.apache.wicket.markup.html.panel.Panel;

/**
 * Menu panel voor Mypalsports.
 * 
 * @author Niels
 * 
 */
public abstract class BaseMainMenu extends Panel {

	private static final long serialVersionUID = 1L;

	public BaseMainMenu(String id) {
		super(id);
		ListView<BaseMenuItemPanel> menuItems = new ListView<BaseMenuItemPanel>(
				"mainMenuItems", createMenuPanels()) {
			private static final long serialVersionUID = 1L;

			@Override
			protected void populateItem(ListItem<BaseMenuItemPanel> item) {
				BaseMenuItemPanel panel = item.getModelObject();
				Label label = new Label("label", panel.getItem().getLabel());
				label.add(AttributeModifier.replace("class",
						panel.getIconClass()));
				item.add(label);
				item.add(panel);
			}
		};
		add(menuItems);
	}

	public abstract List<BaseMenuItemPanel> createMenuPanels();

	@Override
	public void renderHead(IHeaderResponse response) {
		super.renderHead(response);
		response.render(JavaScriptHeaderItem.forReference(getApplication().getJavaScriptLibrarySettings()
				.getJQueryReference()));
		ResourceReferenceUtil.addAccordionMenu(response);
		response.render(OnDomReadyHeaderItem.forScript("$('.nav').navgoco()"));
	}

}
