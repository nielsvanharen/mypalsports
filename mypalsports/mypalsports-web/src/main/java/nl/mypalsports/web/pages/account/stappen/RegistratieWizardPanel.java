package nl.mypalsports.web.pages.account.stappen;

import nl.mypalsports.entities.account.Account;
import nl.mypalsports.entities.persoon.Persoon;

import org.apache.wicket.extensions.wizard.Wizard;
import org.apache.wicket.extensions.wizard.WizardModel;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.PropertyModel;

/**
 * Registratie wizard.
 * 
 * @author Niels
 * 
 */
public class RegistratieWizardPanel extends Wizard {

	private static final long serialVersionUID = 1L;

	public RegistratieWizardPanel(String id) {
		super(id);
		Account account = new Account();
		account.setPersoon(new Persoon());
		setDefaultModel(new CompoundPropertyModel<Account>(account));
		WizardModel model = new WizardModel();
		model.add(new RegistratieStap1(new PropertyModel<Persoon>(getDefaultModel(),
				"persoon")));
		init(model);
	}

}
