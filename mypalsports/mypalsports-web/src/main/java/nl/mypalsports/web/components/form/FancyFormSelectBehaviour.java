package nl.mypalsports.web.components.form;

import org.apache.wicket.ajax.AbstractDefaultAjaxBehavior;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.request.cycle.RequestCycle;
import org.apache.wicket.util.string.StringValue;

public class FancyFormSelectBehaviour extends AbstractDefaultAjaxBehavior {

	private static final long serialVersionUID = 1L;

	@Override
	protected void respond(AjaxRequestTarget target) {
		StringValue value = RequestCycle.get().getRequest().getRequestParameters().getParameterValue("newValue");
		StringValue componentId = RequestCycle.get().getRequest().getRequestParameters()
				.getParameterValue("componentId");
		onSelectionChanged(target, value, componentId.toString());
	}

	public void onSelectionChanged(AjaxRequestTarget target, StringValue newValue, String componentId) {

	}

}
