package nl.mypalsports.web.components.menu.main;

import nl.mypalsports.HomePage;
import nl.mypalsports.web.components.menu.SuperFishMenuItem;
import nl.mypalsports.web.components.menu.SuperFishMenuItemPanel;

import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.link.Link;

public class HomeMainMenuItem extends SuperFishMenuItemPanel {

	private static final long serialVersionUID = 1L;

	public HomeMainMenuItem() {
		super(SuperFishMenuItem.HOME);
	}

	@Override
	protected WebMarkupContainer getLink(String id) {
		return new Link<Void>(id){

			private static final long serialVersionUID = 1L;

			@Override
			public void onClick() {
				setResponsePage(new HomePage());				
			}
			
		};
	}

}
