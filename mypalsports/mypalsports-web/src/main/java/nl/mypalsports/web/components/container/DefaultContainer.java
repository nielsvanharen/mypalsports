package nl.mypalsports.web.components.container;

import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.model.IModel;

/**
 * Standaard container voor artikelen.
 * 
 * @author Niels
 * 
 */
public class DefaultContainer extends AbstractContainer {

	private static final long serialVersionUID = 1L;

	/**
	 * Constructor.
	 * 
	 * @param id
	 * @param titleModel
	 */
	public DefaultContainer(String id, IModel<String> titleModel) {
		super(id, titleModel);
	}

	@Override
	public WebMarkupContainer createContent(String id) {
		return new LoremIpsumPanel(id);
	}
}
