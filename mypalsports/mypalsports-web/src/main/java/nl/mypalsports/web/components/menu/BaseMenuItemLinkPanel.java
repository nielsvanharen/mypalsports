package nl.mypalsports.web.components.menu;

import org.apache.wicket.AttributeModifier;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.IModel;

public abstract class BaseMenuItemLinkPanel extends Panel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String iconClass;

	private IMenuItem item;

	public BaseMenuItemLinkPanel(IMenuItem menuItem, String iconClass) {
		super("mainMenuItemLink");
		this.iconClass = iconClass;
		this.item = menuItem;
		WebMarkupContainer link = getLink("link");
		setRenderBodyOnly(true);
		Label label = new Label("lbl", getLabel());
		//label.setRenderBodyOnly(true);
		label.add(AttributeModifier.replace("class", getIconClass()));
		link.add(label);
		add(link);
	}

	protected abstract WebMarkupContainer getLink(String id);

	public IMenuItem getItem() {
		return item;
	}

	public IModel<String> getLabel() {
		return getItem().getLabel();
	}

	public String getIconClass() {
		return iconClass;
	}

}
