package nl.mypalsports.web.components.menu.account;

import java.util.ArrayList;
import java.util.List;

import nl.mypalsports.web.components.menu.BaseMenuItemLinkPanel;
import nl.mypalsports.web.components.menu.BaseMenuItemPanel;

public class RegistratieMenuPanel extends BaseMenuItemPanel {

	private static final long serialVersionUID = 1L;

	public RegistratieMenuPanel() {
		super(RegistratieMenuItem.TYPEACCOUNT,"icon-globe");
	}

	@Override
	public List<BaseMenuItemLinkPanel> createMenuLinkPanels() {
		List<BaseMenuItemLinkPanel> panels = new ArrayList<BaseMenuItemLinkPanel>();
		panels.add(new RegistratieTypeMenuPanel());
		return panels;
	}

}
