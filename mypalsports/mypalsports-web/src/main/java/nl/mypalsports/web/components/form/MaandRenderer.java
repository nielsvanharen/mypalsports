package nl.mypalsports.web.components.form;

import nl.mypalsports.app.TimeUtil;

import org.apache.wicket.markup.html.form.IChoiceRenderer;
import org.apache.wicket.model.ResourceModel;

/**
 * Converteert een Integer naar een MaandString
 * 
 * @author Niels
 * 
 */
public class MaandRenderer implements IChoiceRenderer<Integer> {

	private static final long serialVersionUID = 1L;

	@Override
	public Object getDisplayValue(Integer object) {
		return new ResourceModel(TimeUtil.getMonthAsText(object.intValue())).getObject();
	}

	@Override
	public String getIdValue(Integer object, int index) {
		return String.valueOf(index);
	}

}
