package nl.mypalsports.web.pages;

import nl.mypalsports.web.components.menu.SuperFishMenu;
import nl.mypalsports.web.components.menu.SuperFishMenuItem;

import org.apache.wicket.markup.html.WebPage;

/**
 * Basis non-secure pagina.
 * 
 * @author Niels
 * 
 */
public class BasePage extends WebPage {

	private static final long serialVersionUID = 1L;

	/**
	 * Constructor.
	 * 
	 * @param params
	 */
	public BasePage(SuperFishMenuItem selectedItem) {
		super();
		add(new SuperFishMenu("menu", selectedItem));
	}

	protected void createMenu() {

	}

}
