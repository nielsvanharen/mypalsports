package nl.mypalsports.web.components.menu;

import java.util.ArrayList;
import java.util.List;

import nl.mypalsports.ResourceReferenceUtil;
import nl.mypalsports.web.components.menu.main.AccountMainMenuItem;
import nl.mypalsports.web.components.menu.main.DemoMainMenuItem;
import nl.mypalsports.web.components.menu.main.HomeMainMenuItem;
import nl.mypalsports.web.components.menu.main.MogelijkHedenMainMenuItem;
import nl.mypalsports.web.components.menu.main.OverzichtMainMenuItem;
import nl.mypalsports.web.components.menu.main.RegistrerenMainMenuItem;

import org.apache.wicket.markup.head.IHeaderResponse;
import org.apache.wicket.markup.head.JavaScriptHeaderItem;
import org.apache.wicket.markup.head.OnDomReadyHeaderItem;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.ListView;
import org.apache.wicket.markup.html.panel.Panel;

public class SuperFishNavMenu extends Panel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private WebMarkupContainer superfishMenu;

	public SuperFishNavMenu(String id) {
		super(id);
	}

	@Override
	protected void onInitialize() {
		super.onInitialize();
		this.superfishMenu = new WebMarkupContainer("superfishMenu");
		List<SuperFishMenuItemPanel> items = new ArrayList<SuperFishMenuItemPanel>();
		items.add(new HomeMainMenuItem());
		items.add(new OverzichtMainMenuItem());
		items.add(new MogelijkHedenMainMenuItem());
		items.add(new DemoMainMenuItem());
		items.add(new RegistrerenMainMenuItem());
		items.add(new AccountMainMenuItem());
		ListView<SuperFishMenuItemPanel> menuItems = new ListView<SuperFishMenuItemPanel>(
				"mainMenuItems", items) {
			private static final long serialVersionUID = 1L;

			@Override
			protected void populateItem(ListItem<SuperFishMenuItemPanel> item) {
				item.add(item.getModelObject());
			}
		};
		superfishMenu.add(menuItems);
		add(superfishMenu);
	}

	@Override
	public void renderHead(IHeaderResponse response) {
		response.render(JavaScriptHeaderItem.forReference(getApplication()
				.getJavaScriptLibrarySettings().getJQueryReference()));
		ResourceReferenceUtil.addMenu(response);
		response.render(OnDomReadyHeaderItem.forScript("$('ul.sf-menu').superfish()"));
	}

}
