package nl.mypalsports.web.components.menu.account;

import java.util.ArrayList;
import java.util.List;

import nl.mypalsports.web.components.menu.BaseMainMenu;
import nl.mypalsports.web.components.menu.BaseMenuItemPanel;

/**
 * Registratie menu.
 * 
 * @author Niels
 * 
 */
public class RegistratieMenu extends BaseMainMenu {

	private static final long serialVersionUID = 1L;

	public RegistratieMenu(String id) {
		super(id);
	}

	@Override
	public List<BaseMenuItemPanel> createMenuPanels() {
		List<BaseMenuItemPanel> panels = new ArrayList<BaseMenuItemPanel>();
		panels.add(new RegistratieMenuPanel());
		return panels;
	}

}
