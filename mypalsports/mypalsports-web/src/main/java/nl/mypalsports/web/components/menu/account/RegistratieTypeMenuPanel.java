package nl.mypalsports.web.components.menu.account;

import org.apache.wicket.markup.html.WebMarkupContainer;

import nl.mypalsports.web.components.menu.BaseMenuItemLinkPanel;

public class RegistratieTypeMenuPanel extends BaseMenuItemLinkPanel {

	private static final long serialVersionUID = 1L;

	public RegistratieTypeMenuPanel() {
		super(TypeRegistratieMenuItem.REGISTRATIE_MIJNGEGEVENS, "icon-home");
	}

	@Override
	protected WebMarkupContainer getLink(String id) {
		return new WebMarkupContainer(id);
	}

}
