package nl.mypalsports.web.components.menu.account;

import nl.mypalsports.web.components.menu.IMenuItem;

import org.apache.wicket.model.IModel;
import org.apache.wicket.model.ResourceModel;

/**
 * Stappen bij het registreren.
 * 
 * @author Niels
 * 
 */
public enum TypeRegistratieMenuItem implements IMenuItem {

	REGISTRATIE_MIJNGEGEVENS;

	@Override
	public IModel<String> getLabel() {
		return new ResourceModel(name().toLowerCase());
	}

}
