package nl.mypalsports.hibernate.interceptor;

/**
 * 
 */

import java.io.Serializable;
import java.util.Date;

import javax.inject.Inject;

import nl.mypalsports.app.MypalSession;
import nl.mypalsports.entities.common.idObject;

import org.hibernate.EmptyInterceptor;
import org.hibernate.type.Type;
import org.springframework.transaction.annotation.Transactional;

/**
 * Interceptor die create en last modified properties bij een save/update actie
 * opslaat.
 * 
 * @author Niels
 * 
 */
public class EntiteitInterceptor extends EmptyInterceptor
{
	private static final long serialVersionUID = 1L;

	@Inject
	private MypalSession session;

	@Transactional
	public boolean onFlushDirty(Object entity, Serializable id, Object[] currentState, Object[] previousState,
			String[] propertyNames, Type[] types)
	{
		boolean theReturn = false;
		if (entity instanceof idObject)
		{
			for (int i = 0; i < propertyNames.length; i++)
			{
				if ("lastModifiedAt".equals(propertyNames[i]))
				{
					currentState[i] = new Date();
					theReturn = true;
				}
				if ("lastModifiedBy".equals(propertyNames[i]))
				{
					currentState[i] = this.session.getLoggedInAccount();
					theReturn = true;
				}
			}
		}
		return theReturn;
	}

	@Transactional
	public boolean onSave(Object entity, Serializable id, Object[] state, String[] propertyNames, Type[] types)
	{
		boolean theReturn = false;
		if (entity instanceof idObject)
		{
			for (int i = 0; i < propertyNames.length; i++)
			{
				if ("createdAt".equals(propertyNames[i]))
				{
					state[i] = new Date();
					theReturn = true;
				}
				if ("createdBy".equals(propertyNames[i]))
				{
					state[i] = this.session.getLoggedInAccount();
					theReturn = true;
				}
				if ("lastModifiedAt".equals(propertyNames[i]))
				{
					state[i] = new Date();
					theReturn = true;
				}
				if ("lastModifiedBy".equals(propertyNames[i]))
				{
					state[i] = this.session.getLoggedInAccount();
					theReturn = true;
				}
			}
		}
		return theReturn;
	}
}
