package nl.mypalsports;

import nl.mypalsports.web.components.bottomrow.BottomRowPanel;
import nl.mypalsports.web.components.bottomrow.ButtonAlignment;
import nl.mypalsports.web.components.buttons.AbstractLinkButton;
import nl.mypalsports.web.components.container.DefaultContainer;
import nl.mypalsports.web.components.menu.SuperFishMenuItem;
import nl.mypalsports.web.pages.BasePage;
import nl.mypalsports.web.pages.account.AccountRegistrerenPage;

import org.apache.wicket.model.Model;
import org.apache.wicket.model.StringResourceModel;

public class HomePage extends BasePage {
	private static final long serialVersionUID = 1L;

	public HomePage() {
		super(SuperFishMenuItem.HOME);
		add(new DefaultContainer("presteren", new StringResourceModel("home.title", new Model<String>(
				"Beter presteren met MypalSports!"))) {

			private static final long serialVersionUID = 1L;

			@Override
			protected void fillBottomRow(BottomRowPanel panel) {
				panel.addButton(new AbstractLinkButton(panel, new StringResourceModel("home.productinformatie",
						new Model<String>("Product Informatie")), ButtonAlignment.CENTER) {

					private static final long serialVersionUID = 1L;

					@Override
					protected void onClick() {
						setResponsePage(new AccountRegistrerenPage());
					}

				});
			}

			@Override
			public String getCssClass() {
				return "container700w450h";
			}
		});
		add(new DefaultContainer("meldjeaan", new StringResourceModel("home.meldjeaan", new Model<String>(
				"Meld je aan als sporter"))) {

			private static final long serialVersionUID = 1L;

			@Override
			protected void fillBottomRow(BottomRowPanel panel) {
				panel.addButton(new AbstractLinkButton(panel, new StringResourceModel("home.registreren",
						new Model<String>("Direct Registreren")), ButtonAlignment.CENTER) {

					private static final long serialVersionUID = 1L;

					@Override
					protected void onClick() {
						setResponsePage(new AccountRegistrerenPage());
					}

				});
			}
			@Override
			public String getCssClass() {
				return "container350w450h";
			}
		});
	}

}
