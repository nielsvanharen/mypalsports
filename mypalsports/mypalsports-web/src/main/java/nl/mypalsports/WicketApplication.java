package nl.mypalsports;

import org.apache.wicket.RuntimeConfigurationType;
import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.protocol.http.WebApplication;
import org.apache.wicket.request.resource.ContextRelativeResource;
import org.apache.wicket.request.resource.SharedResourceReference;
import org.apache.wicket.spring.injection.annot.SpringComponentInjector;

/**
 * Application object for your web application. If you want to run this
 * application without deploying, run the Start class.
 * 
 * @see nl.mypalsports.Start#main(String[])
 */
public class WicketApplication extends WebApplication {
	/**
	 * @see org.apache.wicket.Application#getHomePage()
	 */
	@Override
	public Class<? extends WebPage> getHomePage() {
		return HomePage.class;
	}

	/**
	 * @see org.apache.wicket.Application#init()
	 */
	@Override
	public void init() {
		super.init();
		getSharedResources().add("jquery-2.0", new ContextRelativeResource("assets/js/jquery-2.0.3.min.js"));
		getJavaScriptLibrarySettings().setJQueryReference(new SharedResourceReference("jquery-2.0"));
		if (getConfigurationType().equals(RuntimeConfigurationType.DEVELOPMENT)) {
			getDebugSettings().setAjaxDebugModeEnabled(true);
		}
		getComponentInstantiationListeners().add(new SpringComponentInjector(this));
		getRequestCycleSettings().setGatherExtendedBrowserInfo(true);
	}
}
