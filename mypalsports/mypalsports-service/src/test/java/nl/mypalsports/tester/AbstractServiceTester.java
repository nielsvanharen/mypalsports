/**
 * 
 */
package nl.mypalsports.tester;

import java.util.HashMap;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.dialect.HSQLDialect;
import org.hibernate.dialect.function.SQLFunction;
import org.hibernate.dialect.function.SQLFunctionRegistry;
import org.hibernate.engine.spi.SessionFactoryImplementor;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.test.context.ContextConfiguration;

/**
 * @author Niels
 * 
 */
@RunWith(MockitoJUnitRunner.class)
@ContextConfiguration(locations = "classpath:spring-test.xml")
public abstract class AbstractServiceTester
{
	protected SessionFactory sf;
	
	protected SessionFactoryImplementor sfi;
	
	@Mock
	protected Session session;

	/**
	 * @throws Exception
	 */
	@Before
	public void setUp() throws Exception
	{
		sf = Mockito.mock(SessionFactory.class, Mockito.withSettings().extraInterfaces(SessionFactoryImplementor.class));
		sfi = (SessionFactoryImplementor) sf;
		Mockito.when(sf.openSession()).thenReturn(session);
		Mockito.when(sfi.getSqlFunctionRegistry()).thenReturn(
				new SQLFunctionRegistry(new HSQLDialect(), new HashMap<String, SQLFunction>()));
	}
}
