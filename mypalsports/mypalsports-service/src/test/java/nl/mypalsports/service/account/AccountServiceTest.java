/**
 * 
 */
package nl.mypalsports.service.account;

import junit.framework.Assert;
import nl.mypalsports.dao.account.AccountDao;
import nl.mypalsports.entities.account.Account;
import nl.mypalsports.tester.AbstractServiceTester;
import nl.myspalsports.service.account.AccountServiceImpl;

import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;

/**
 * @author Niels
 * 
 */
public class AccountServiceTest extends AbstractServiceTester
{
	@Mock
	private AccountDao dao;

	@InjectMocks
	private AccountServiceImpl service;

	/**
	 * 
	 */
	@Test
	public void readAccount()
	{
		Account account = createAccount();
		Mockito.when(this.dao.get(Account.class, new Long(1))).thenReturn(account);
		Account serviceAccount = this.service.get(Account.class, new Long(1));
		Assert.assertEquals(account, serviceAccount);
	}

	private Account createAccount()
	{
		Account account = new Account();
		account.setId(new Long(1));
		return account;
	}
}
