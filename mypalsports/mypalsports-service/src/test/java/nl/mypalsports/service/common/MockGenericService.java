/**
 * 
 */
package nl.mypalsports.service.common;

import java.util.List;

import nl.mypalsports.dao.common.GenericDao;
import nl.mypalsports.entities.common.Entiteit;
import nl.mypalsports.service.zoekfilter.ZoekSortering;

import org.hibernate.Criteria;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author Niels
 * 
 */
public class MockGenericService extends
		GenericServiceImpl<Entiteit, ZoekSortering<Entiteit>> {

	private static final long serialVersionUID = 1L;

	@Autowired
	private GenericDao<Entiteit> dao;

	/*
	 * (non-Javadoc)
	 * 
	 * @see nl.mypalsports.service.common.GenericServiceImpl#getDao()
	 */
	@Override
	protected GenericDao<Entiteit> getDao() {
		return this.dao;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see nl.mypalsports.service.common.GenericServiceImpl#createCriteria(nl.
	 * mypalsports.service.zoekfilter.ZoekSortering)
	 */
	@Override
	protected Criteria createCriteria(ZoekSortering<Entiteit> filter) {
		Criteria criteria = createCriteria(Entiteit.class);
		return criteria;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * nl.mypalsports.service.common.GenericServiceImpl#addOrders(org.hibernate
	 * .Criteria, java.util.List, boolean)
	 */
	@Override
	protected void addOrders(Criteria criteria, List<String> orderByList,
			boolean ascending) {
		// MOCK;
	}

}
