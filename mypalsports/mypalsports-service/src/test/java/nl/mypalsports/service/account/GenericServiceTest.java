/**
 * 
 */
package nl.mypalsports.service.account;

import java.util.ArrayList;
import java.util.List;

import junit.framework.Assert;
import nl.mypalsports.dao.common.GenericDao;
import nl.mypalsports.entities.common.Entiteit;
import nl.mypalsports.service.common.MockGenericService;
import nl.mypalsports.service.common.MockZoekFilter;
import nl.mypalsports.tester.AbstractServiceTester;

import org.hibernate.Criteria;
import org.junit.Ignore;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;

/**
 * @author Niels
 * 
 */
public class GenericServiceTest extends AbstractServiceTester {

	@InjectMocks
	private MockGenericService service;

	@Mock
	private GenericDao<Entiteit> dao;

	@Mock
	private Criteria criteria;

	/**
	 * 
	 */
	@Ignore
	@Test
	public void findListResult() {
		Mockito.when(this.dao.createCriteria(Entiteit.class)).thenReturn(
				criteria);
		Mockito.when(this.service.list(new MockZoekFilter())).thenReturn(
				createAccountList());
		List<Entiteit> accounts = this.service.list(new MockZoekFilter());
		Assert.assertEquals(3, accounts.size());
	}

	private Entiteit createAccount(Long id) {
		Entiteit account = new Entiteit() {

			private static final long serialVersionUID = 1L;

		};
		account.setId(id);
		return account;
	}

	private List<Entiteit> createAccountList() {
		List<Entiteit> accounts = new ArrayList<Entiteit>();
		accounts.add(createAccount(new Long(1)));
		accounts.add(createAccount(new Long(2)));
		accounts.add(createAccount(new Long(3)));
		return accounts;
	}
}
