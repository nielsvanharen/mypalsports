/**
 * 
 */
package nl.myspalsports.service.account;

import nl.mypalsports.entities.account.Account;
import nl.mypalsports.service.common.GenericService;
import nl.mypalsports.service.zoekfilter.AccountZoekFilter;

/**
 * @author Niels
 *
 */
public interface AccountService extends GenericService<Account, AccountZoekFilter>
{
	/**
	 * @param gebruikersnaam
	 * @return de account.
	 */
	Account getAccountByGebruikersnaam(String gebruikersnaam);
}
