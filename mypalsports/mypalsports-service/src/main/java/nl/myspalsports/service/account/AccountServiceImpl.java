/**
 * 
 */
package nl.myspalsports.service.account;

import nl.mypalsports.dao.account.AccountDao;
import nl.mypalsports.entities.account.Account;
import nl.mypalsports.service.common.GenericServiceImpl;
import nl.mypalsports.service.zoekfilter.AccountZoekFilter;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author Niels
 * 
 */
@Service
public class AccountServiceImpl extends
		GenericServiceImpl<Account, AccountZoekFilter> implements
		AccountService {

	private static final long serialVersionUID = 1L;

	@Autowired
	private AccountDao accountDao;

	/*
	 * (non-Javadoc)
	 * 
	 * @see nl.mypalsports.service.common.GenericServiceImpl#getDao()
	 */
	@Override
	public AccountDao getDao() {
		return this.accountDao;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see nl.mypalsports.service.common.GenericServiceImpl#createCriteria(nl.
	 * mypalsports.zoekfilter.common.ZoekSortering)
	 */
	@Override
	protected Criteria createCriteria(AccountZoekFilter filter) {
		Criteria criteria = createCriteria(Account.class);
		return criteria;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * nl.mypalsports.service.account.AccountService#getAccountByGebruikersnaam
	 * (java.lang.String)
	 */
	@Override
	public Account getAccountByGebruikersnaam(String gebruikersnaam) {
		Criteria criteria = createCriteria(Account.class);
		criteria.add(Restrictions.eq("gebruikersnaam", gebruikersnaam));
		return unique(criteria, true);
	}

}
