/**
 * 
 */
package nl.mypalsports.service.common;

import java.io.Serializable;
import java.util.List;

import nl.mypalsports.entities.common.idObject;
import nl.mypalsports.service.zoekfilter.ZoekSortering;

import org.hibernate.Criteria;

/**
 * @author Niels
 * @param <T>
 * @param <Z>
 * 
 */
public interface GenericService<T extends idObject, Z extends ZoekSortering<T>> extends Serializable
{
	/**
	 * Standaard save actie.
	 * 
	 * @param dataObject
	 * @return persistent dataObject
	 */
	Serializable save(T dataObject);

	/**
	 * Delete het object.
	 * 
	 * @param dataObject
	 */
	void delete(T dataObject);

	/**
	 * get op basis van een id.
	 * 
	 * @param clazz
	 * @param id
	 * @return het dataObject.
	 */
	T get(Class<T> clazz, Serializable id);

	/**
	 * return list op basis van een zoekfilter
	 * 
	 * @param zoekFilter
	 * @return de lijst.
	 */
	List<T> list(Z zoekFilter);

	/**
	 * saven of updaten van het object.
	 * 
	 * @param dataObject
	 */
	void saveOrUpdate(T dataObject);

	/**
	 * dataprovider results met zoekfilter;
	 * 
	 * @param filter
	 * @param firstResult
	 * @param maxResults
	 * @return de lijst.
	 */
	public List<T> list(Z filter, long firstResult, long maxResults);

	/**
	 * Count op absis van een zoekfilter.
	 * 
	 * @param filter
	 * @return het aantal.
	 */
	public long listCount(Z filter);
	
	/**
	 * 
	 * @param criteria
	 * @param cacheable
	 * @return unique result.
	 */
	public <Y> Y unique(Criteria criteria, boolean cacheable);

}
