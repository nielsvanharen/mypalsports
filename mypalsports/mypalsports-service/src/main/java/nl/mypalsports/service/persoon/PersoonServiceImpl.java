/**
 * 
 */
package nl.mypalsports.service.persoon;

import nl.mypalsports.dao.persoon.PersoonDao;
import nl.mypalsports.entities.persoon.Persoon;
import nl.mypalsports.service.common.GenericServiceImpl;
import nl.mypalsports.service.zoekfilter.PersoonZoekFilter;

import org.hibernate.Criteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author Niels
 * 
 */
@Service
public class PersoonServiceImpl extends
		GenericServiceImpl<Persoon, PersoonZoekFilter> implements
		PersoonService {

	private static final long serialVersionUID = 1L;

	@Autowired
	private PersoonDao dao;

	/*
	 * (non-Javadoc)
	 * 
	 * @see nl.mypalsports.service.common.GenericServiceImpl#getDao()
	 */
	@Override
	protected PersoonDao getDao() {
		return this.dao;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see nl.mypalsports.service.common.GenericServiceImpl#createCriteria(nl.
	 * mypalsports.service.zoekfilter.ZoekSortering)
	 */
	@Override
	protected Criteria createCriteria(PersoonZoekFilter filter) {
		Criteria criteria = createCriteria(Persoon.class);
		return criteria;
	}

}
