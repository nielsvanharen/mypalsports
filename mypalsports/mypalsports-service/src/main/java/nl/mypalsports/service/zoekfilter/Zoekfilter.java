/**
 * 
 */
package nl.mypalsports.service.zoekfilter;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Niels
 * @param <T>
 * 
 */
public class Zoekfilter<T> implements ZoekSortering<T> {

	private static final long serialVersionUID = 1L;

	private boolean ascending = false;

	private List<String> orderByList = new ArrayList<String>();

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.apache.wicket.model.IDetachable#detach()
	 */
	@Override
	public void detach() {
		// TODO Auto-generated method stub

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see nl.mypalsports.service.zoekfilter.ZoekSortering#addOrderByProperty
	 * (java.lang.String)
	 */
	@Override
	public void addOrderByProperty(String property) {
		String[] properties = property.split(",");
		if (properties.length == 1) {
			// Controleer of het meegegeven property het eerste item in deze
			// lijst is.
			if (this.orderByList.size() > 0) {
				if (this.orderByList.get(0).equals(property)) {
					this.ascending = !this.ascending;
					return;
				}
			}
			this.ascending = true;
			this.orderByList.remove(property);
			this.orderByList.add(0, property);
		} else {
			if (this.orderByList.size() > 0
					&& this.orderByList.get(0).equals(properties[0].trim())) {
				this.ascending = !this.ascending;
				return;
			}
			for (int i = properties.length - 1; i >= 0; i--) {
				String trim = properties[i].trim();
				this.orderByList.remove(trim);
				this.orderByList.add(0, trim);
			}
			this.ascending = true;
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see nl.mypalsports.service.zoekfilter.ZoekSortering#getOrderBy()
	 */
	@Override
	public String getOrderBy() {
		if (this.orderByList.size() > 0)
			return this.orderByList.get(0);
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see nl.mypalsports.service.zoekfilter.ZoekSortering#isAscending()
	 */
	@Override
	public boolean isAscending() {
		return this.ascending;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * nl.mypalsports.service.zoekfilter.ZoekSortering#setAscending(boolean)
	 */
	@Override
	public void setAscending(boolean ascending) {
		this.ascending = ascending;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see nl.mypalsports.service.zoekfilter.ZoekSortering#getOrderByList()
	 */
	@Override
	public List<String> getOrderByList() {
		return this.orderByList;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see nl.mypalsports.service.zoekfilter.ZoekSortering#setOrderByList(java
	 * .util.List)
	 */
	@Override
	public void setOrderByList(List<String> orderByList) {
		this.orderByList = orderByList;
	}

}
