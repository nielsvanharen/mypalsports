/**
 * 
 */
package nl.mypalsports.service.zoekfilter;

import java.io.Serializable;
import java.util.Map;

import nl.mypalsports.entities.common.idObject;

/**
 * @author Niels
 * @param <T>
 *            entity
 * 
 */
public interface PropertyZoekFilter<T extends idObject> extends ZoekSortering<T>
{
	/**
	 * 
	 * @return map met required fields. Key = Value Value = type of argument.
	 */
	Map<String, Serializable> getRequiredFields();
}
