/**
 * 
 */
package nl.mypalsports.service.postcode;

import nl.mypalsports.entities.adres.Postcode;
import nl.mypalsports.service.common.GenericService;
import nl.mypalsports.service.zoekfilter.PostcodeZoekFilter;

/**
 * @author Niels
 *
 */
public interface PostcodeService extends GenericService<Postcode, PostcodeZoekFilter>
{
	/**
	 * @param postcode
	 * @return de postcode
	 */
	Postcode getPostcodeByPostcode(String postcode);
}
