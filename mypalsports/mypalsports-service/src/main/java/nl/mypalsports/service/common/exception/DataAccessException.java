package nl.mypalsports.service.common.exception;

/**
 * @author Niels
 * 
 */
public class DataAccessException extends RuntimeException
{

	private static final long serialVersionUID = 1L;

	/**
	 * Constructor.
	 * 
	 * @param message
	 * @param exception
	 */
	public DataAccessException(String message, RuntimeException exception)
	{
		super(message, exception);
	}

	/**
	 * Constructor.
	 * 
	 * @param exception
	 */
	public DataAccessException(RuntimeException exception)
	{
		super(exception);
	}

}
