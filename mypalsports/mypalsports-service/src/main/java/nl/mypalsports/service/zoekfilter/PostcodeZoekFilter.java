/**
 * 
 */
package nl.mypalsports.service.zoekfilter;

import nl.mypalsports.entities.adres.Postcode;

/**
 * @author Niels
 *
 */
public class PostcodeZoekFilter extends Zoekfilter<Postcode>
{
	private static final long serialVersionUID = 1L;
	
	private String postcode;

	/**
	 * @return the postcode
	 */
	public String getPostcode()
	{
		return postcode;
	}

	/**
	 * @param postcode the postcode to set
	 */
	public void setPostcode(String postcode)
	{
		this.postcode = postcode;
	}

}
