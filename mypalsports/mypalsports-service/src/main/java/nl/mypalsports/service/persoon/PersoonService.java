/**
 * 
 */
package nl.mypalsports.service.persoon;

import nl.mypalsports.entities.persoon.Persoon;
import nl.mypalsports.service.common.GenericService;
import nl.mypalsports.service.zoekfilter.PersoonZoekFilter;

/**
 * @author Niels
 * 
 */
public interface PersoonService extends GenericService<Persoon, PersoonZoekFilter>
{
}
