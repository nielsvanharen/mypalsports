/**
 * 
 */
package nl.mypalsports.service.postcode;

import nl.mypalsports.dao.postcode.PostcodeDao;
import nl.mypalsports.entities.adres.Postcode;
import nl.mypalsports.service.common.GenericServiceImpl;
import nl.mypalsports.service.zoekfilter.PostcodeZoekFilter;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author Niels
 * 
 */
@Service
public class PostcodeServiceImpl extends
		GenericServiceImpl<Postcode, PostcodeZoekFilter> implements
		PostcodeService {
	private static final long serialVersionUID = 1L;

	@Autowired
	private PostcodeDao dao;

	/*
	 * (non-Javadoc)
	 * 
	 * @see nl.mypalsports.service.common.GenericServiceImpl#getDao()
	 */
	@Override
	protected PostcodeDao getDao() {
		return this.dao;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see nl.mypalsports.service.common.GenericServiceImpl#createCriteria(nl.
	 * mypalsports.service.zoekfilter.ZoekSortering)
	 */
	@Override
	protected Criteria createCriteria(PostcodeZoekFilter filter) {
		Criteria criteria = createCriteria(Postcode.class);
		criteria.add(Restrictions.eqOrIsNull("postcode", filter.getPostcode()));
		return criteria;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * nl.mypalsports.service.postcode.PostcodeService#getPostcodeByPostcode
	 * (java.lang.String)
	 */
	@Override
	@Transactional
	public Postcode getPostcodeByPostcode(String postcode) {
		Criteria criteria = createCriteria(Postcode.class);
		criteria.add(Restrictions.eq("postcode", postcode));
		return unique(criteria, true);
	}
}
