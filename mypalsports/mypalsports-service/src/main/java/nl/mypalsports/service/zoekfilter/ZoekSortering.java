package nl.mypalsports.service.zoekfilter;

import java.util.List;

import org.apache.wicket.model.IDetachable;

/**
 * Interface voor zoeksortering.
 * 
 * @author Niels
 * @param <T>
 * 
 */
public interface ZoekSortering<T> extends IDetachable
{
	/**
	 * Voegt het property toe aan het begin van de order by list. Indien het
	 * property al het eerste element in de lijst is, wordt de sorteervolgorde
	 * geflipt.
	 * 
	 * @param property
	 *            Het property dat aan het begin van de order by list moet
	 *            komen.
	 */
	public void addOrderByProperty(String property);

	/**
	 * @return Het eerste property in de order by list, of null indien de lijst
	 *         leeg is.
	 */
	public String getOrderBy();

	/**
	 * 
	 * @return of het ascending is.
	 */
	public boolean isAscending();

	/**
	 * zet het ascending.
	 * 
	 * @param ascending
	 */
	public void setAscending(boolean ascending);

	/**
	 * 
	 * @return de lijst van orderby properties.
	 */
	public List<String> getOrderByList();

	/**
	 * Zet de lijst met order by properties.
	 * 
	 * @param orderByList
	 */
	public void setOrderByList(List<String> orderByList);
}
