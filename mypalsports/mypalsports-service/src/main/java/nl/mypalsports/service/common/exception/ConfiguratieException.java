package nl.mypalsports.service.common.exception;

/**
 * Configuratie error.
 * 
 * @author Niels
 * 
 */
public class ConfiguratieException extends RuntimeException
{
	private static final long serialVersionUID = 1L;

	/**
	 * Constructor.
	 */
	public ConfiguratieException()
	{
	}

	/**
	 * Constructor.
	 * 
	 * @param message
	 */
	public ConfiguratieException(String message)
	{
		super(message);
	}

	/**
	 * Constructor.
	 * 
	 * @param message
	 * @param cause
	 */
	public ConfiguratieException(String message, Throwable cause)
	{
		super(message, cause);
	}

	/**
	 * Constructor.
	 * 
	 * @param cause
	 */
	public ConfiguratieException(Throwable cause)
	{
		super(cause);
	}
}
