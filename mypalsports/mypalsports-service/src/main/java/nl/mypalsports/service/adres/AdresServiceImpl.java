/**
 * 
 */
package nl.mypalsports.service.adres;

import nl.mypalsports.dao.adres.AdresDao;
import nl.mypalsports.entities.adres.Adres;
import nl.mypalsports.service.common.GenericServiceImpl;
import nl.mypalsports.service.zoekfilter.AdresZoekFilter;

import org.hibernate.Criteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author Niels
 * 
 */
@Service
public class AdresServiceImpl extends
		GenericServiceImpl<Adres, AdresZoekFilter> implements AdresService {
	private static final long serialVersionUID = 1L;

	@Autowired
	private AdresDao dao;

	/*
	 * (non-Javadoc)
	 * 
	 * @see nl.mypalsports.service.common.GenericServiceImpl#getDao()
	 */
	@Override
	protected AdresDao getDao() {
		return this.dao;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @seenl.mypalsports.service.common.GenericServiceImpl#createCriteria(nl.
	 * mypalsports.service.zoekfilter.ZoekSortering)
	 */
	@Override
	protected Criteria createCriteria(AdresZoekFilter filter) {
		Criteria criteria = createCriteria(Adres.class);
		return criteria;
	}

}
