/**
 * 
 */
package nl.mypalsports.service.common;

import java.io.Serializable;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import nl.mypalsports.common.exception.DataAccessException;
import nl.mypalsports.common.util.Asserts;
import nl.mypalsports.common.util.Property;
import nl.mypalsports.common.util.ReflectionUtil;
import nl.mypalsports.dao.common.GenericDao;
import nl.mypalsports.entities.common.idObject;
import nl.mypalsports.service.common.exception.ConfiguratieException;
import nl.mypalsports.service.zoekfilter.ZoekSortering;

import org.hibernate.CacheMode;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.criterion.Projections;
import org.hibernate.internal.CriteriaImpl;
import org.hibernate.mapping.PersistentClass;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Niels
 * @param <T>
 * @param <Z>
 * 
 */
public abstract class GenericServiceImpl<T extends idObject, Z extends ZoekSortering<T>>
		implements GenericService<T, Z> {

	private static final Logger log = LoggerFactory
			.getLogger(GenericServiceImpl.class);

	private static final long serialVersionUID = 1L;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * nl.mypalsports.common.dao.GenericDao#list(nl.mypalsports.ZoekSortering)
	 */
	@Override
	public List<T> list(Z zoekFilter) {
		return list(zoekFilter, -1, -1);
	}

	/**
	 * @param criteria
	 * @param cacheable
	 * @return de unique value op basis van een criteria.
	 */
	@SuppressWarnings("unchecked")
	public <Y> Y unique(Criteria criteria, boolean cacheable) {
		Asserts.assertNotNull("criteria", criteria);
		try {
			return (Y) criteria.setCacheable(cacheable)
					.setCacheRegion(getQueryCacheRegion()).uniqueResult();
		} catch (HibernateException e) {
			String message = "Failed to get a unique result";
			if (criteria instanceof CriteriaImpl) {
				message = message + " of "
						+ ((CriteriaImpl) criteria).getEntityOrClassName();
			}
			log.error(message, e);
			throw new DataAccessException(message, e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * nl.mypalsports.common.dao.GenericDao#list(nl.mypalsports.ZoekSortering,
	 * long, long)
	 */
	@Override
	public List<T> list(Z filter, long firstResult, long maxResults) {
		Criteria criteria = createCriteria(filter);
		if (criteria != null)
			return criteriaList(criteria, filter, firstResult, maxResults);
		// Query query = createHQLQuery(filter);
		// if (query != null)
		// return hqlList(query, firstResult, maxResults, filter);
		// SQLQuery sqlQuery = createSQLQuery(filter);
		// if (sqlQuery != null)
		// return sqlList(sqlQuery, firstResult, maxResults);
		return Collections.emptyList();
	}

	protected final List<T> criteriaList(Criteria criteria, Z filter,
			long firstResult, long maxResults) {
		return criteriaList(criteria, filter, firstResult, maxResults,
				(CacheMode) null);
	}

	@SuppressWarnings("unchecked")
	protected <Y> List<Y> criteriaList(Criteria criteria, Z filter,
			long firstResult, long maxResults, CacheMode cacheMode) {
		Integer firstResultInt = (int) firstResult;
		Integer maxResultInt = (int) maxResults;
		if (firstResult > 0)
			criteria.setFirstResult(firstResultInt.intValue());
		if (maxResults > 0 && maxResults < Integer.MAX_VALUE)
			criteria.setMaxResults(maxResultInt.intValue());
		addOrders(criteria, filter.getOrderByList(), filter.isAscending());
		if (cacheMode != null)
			criteria.setCacheMode(cacheMode);
		// return
		// criteria.setCacheable(filter.isResultCacheable()).setCacheRegion(getQueryCacheRegion()).list();
		return criteria.setCacheable(true)
				.setCacheRegion(getQueryCacheRegion()).list();
	}

	/**
	 * Executes the criteria. returning the results as a list. Note that the
	 * result must match the type of the checked list. Het queryresultaat is
	 * cacheable. Deze methode mag niet gebruikt worden als je criteria een
	 * subquery middels een DetachedCriteria bevat!
	 * 
	 * @return the results of the criteria.
	 */
	protected final List<T> cachedTypedList(Criteria criteria) {
		return typedList(criteria, true);
	}

	/**
	 * Executes the criteria. returning the results as a list. Note that the
	 * result must match the type of the checked list.
	 * 
	 * @param criteria
	 * @param cache
	 *            Mag het resultaat gecached worden, en daardoor dus ook uit de
	 *            cache opgehaald worden? Zet deze parameter op false als je
	 *            criteria een subquery middels een DetachedCriteria bevat!
	 * @return the results of the criteria.
	 */
	protected final List<T> typedList(Criteria criteria, boolean cache) {
		return internalList(criteria, cache);
	}

	/**
	 * Executes the criteria. returning the results as a list. Note that the
	 * returned list is not required to contain the checked type of this helper.
	 * 
	 * @param criteria
	 * @return the list
	 */
	protected final <Y> List<Y> cachedList(Criteria criteria) {
		return internalList(criteria, true);
	}

	/**
	 * Executes the criteria. returning the results as a list. Note that the
	 * returned list is not required to contain the checked type of this helper.
	 * 
	 * @param criteria
	 * @return the list
	 */
	protected final <Y> List<Y> uncachedList(Criteria criteria) {
		return internalList(criteria, false);
	}

	/**
	 * Executes the criteria. returning the results as a list. Note that the
	 * returned list is not required to contain the checked type of this helper.
	 * 
	 * @param criteria
	 * @return the list
	 */
	protected final <Y> List<Y> list(Criteria criteria, boolean cached) {
		return internalList(criteria, cached);
	}

	@SuppressWarnings("unchecked")
	private <Y> List<Y> internalList(Criteria criteria, boolean cache) {
		Asserts.assertNotNull("criteria", criteria);

		try {
			List<Y> data = criteria.setCacheable(cache)
					.setCacheRegion(getQueryCacheRegion()).list();
			return data;
		} catch (HibernateException e) {
			String message = "Failed to get a list";
			if (criteria instanceof CriteriaImpl) {
				message = message + " of "
						+ ((CriteriaImpl) criteria).getEntityOrClassName();
			}
			log.error(message, e);
			throw new DataAccessException(message, e);
		}
	}

	protected String getQueryCacheRegion() {
		return "org.hibernate.cache.internal.StandardQueryCache";
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * nl.mypalsports.common.dao.GenericDao#listCount(nl.mypalsports.zoekfilter
	 * .ZoekSortering)
	 */
	@Override
	public long listCount(Z filter) {
		Criteria criteria = createCriteria(filter);
		if (criteria != null)
			return criteriaListCount(criteria, filter);
		// Query query = createHQLCountQuery(filter);
		// if (query != null)
		// return hqlListCount(query, filter);
		// SQLQuery sqlQuery = createSQLCountQuery(filter);
		// if (sqlQuery != null)
		// return sqlListCount(sqlQuery);
		return 0;
	}

	private int criteriaListCount(Criteria criteria, Z filter) {
		criteria.setProjection(Projections.rowCount());
		Object res = criteria.uniqueResult();
		// Object res =
		// criteria.setCacheable(filter.isResultCacheable()).setCacheRegion(getQueryCacheRegion())
		// .uniqueResult();
		if (res == null)
			return 0;
		return ((Number) res).intValue();
	}

	/**
	 * @param clazz
	 * @return criteria.
	 */
	public Criteria createCriteria(Class<T> clazz) {
		return getDao().createCriteria(clazz);
	}

	/**
	 * @return de implementatie van de generic dao;
	 */
	protected abstract GenericDao<T> getDao();

	protected abstract Criteria createCriteria(Z filter);

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * nl.mypalsports.service.common.GenericService#save(nl.mypalsports.entities
	 * .idObject)
	 */
	@Override
	public Serializable save(T dataObject) {
		return getDao().save(dataObject);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * nl.mypalsports.service.common.GenericService#delete(nl.mypalsports.entities
	 * .idObject)
	 */
	@Override
	public void delete(T dataObject) {
		getDao().delete(dataObject);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see nl.mypalsports.service.common.GenericService#get(java.lang.Class,
	 * java.io.Serializable)
	 */
	@Override
	public T get(Class<T> clazz, Serializable id) {
		return getDao().get(clazz, id);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * nl.mypalsports.service.common.GenericService#saveOrUpdate(nl.mypalsports
	 * .entities.idObject)
	 */
	@Override
	public void saveOrUpdate(T dataObject) {
		getDao().saveOrUpdate(dataObject);
	}

	@SuppressWarnings("unchecked")
	protected void addOrders(Criteria criteria, List<String> orderByList,
			boolean ascending) {
		if (ascending) {
			for (String orderBy : orderByList) {
				addAliasForOrderBy(criteria, orderBy);
				if (orderBy.indexOf(".") == orderBy.lastIndexOf("."))
					criteria.addOrder(org.hibernate.criterion.Order
							.asc(orderBy));
				else {
					criteria.addOrder(org.hibernate.criterion.Order.asc(orderBy
							.substring(orderBy.substring(0,
									orderBy.lastIndexOf(".")).lastIndexOf(".") + 1)));
				}
			}
		} else {
			for (String orderBy : orderByList) {
				addAliasForOrderBy(criteria, orderBy);
				if (orderBy.indexOf(".") == orderBy.lastIndexOf("."))
					criteria.addOrder(org.hibernate.criterion.Order
							.desc(orderBy));
				else {
					criteria.addOrder(org.hibernate.criterion.Order
							.desc(orderBy
									.substring(orderBy.substring(0,
											orderBy.lastIndexOf("."))
											.lastIndexOf(".") + 1)));
				}
			}
		}
		CriteriaImpl impl = (CriteriaImpl) criteria;
		String className = impl.getEntityOrClassName();
		PersistentClass clazz = getDao().getConfiguration().getClassMapping(
				className);
		if (clazz == null) {
			String message = "Vergeten om " + className
					+ " toe te voegen aan de Hibernate configuratie error";
			log.error(message);
			throw new ConfiguratieException(message);
		}
		Iterator<Column> it = clazz.getIdentifier().getColumnIterator();
		while (it.hasNext()) {
			Column column = it.next();
			if (!orderByList.contains(column.name())) {
				if (ascending) {
					criteria.addOrder(org.hibernate.criterion.Order.asc(column
							.name()));
				} else {
					criteria.addOrder(org.hibernate.criterion.Order.desc(column
							.name()));
				}
			}
		}
	}

	/**
	 * Check if the criteria already has an alias for this orderBy. When this
	 * property is a nullable (or collection) property we will not add an alias
	 * because this might probably need an outer join, which seriously decreases
	 * performance.
	 * 
	 * @param criteria
	 * @param orderBy
	 */
	@SuppressWarnings("unchecked")
	private void addAliasForOrderBy(Criteria criteria, String orderBy) {
		if (orderBy.contains(".")
				&& CriteriaImpl.class.isAssignableFrom(criteria.getClass())) {
			CriteriaImpl criteriaImpl = (CriteriaImpl) criteria;
			String totalOrderBy = orderBy;
			String processedOrderBy = "";
			String baseOrderBy = "";
			String orderByAlias = totalOrderBy.substring(0,
					totalOrderBy.indexOf("."));
			Class<T> entityClass;
			Property<?, ?, ?> orderByProperty;
			boolean endofalias = true;

			try {
				entityClass = (Class<T>) Class.forName(criteriaImpl
						.getEntityOrClassName());
			} catch (ClassNotFoundException e) {
				// Class niet gevonden, we kunnen dus geen alias maken.
				throw new HibernateException(
						"Kon geen alias toevoegen aan de criteria, reden: kon het root element van de criteria niet bepalen");
			}

			while (endofalias) {
				/*
				 * Check of de alias al bestaat.
				 */
				Iterator<Object> iter = criteriaImpl.iterateSubcriteria();
				boolean currentaliasexists = criteriaImpl.getAlias().equals(
						orderByAlias);
				while (!currentaliasexists && iter.hasNext()) {
					Criteria aliasCrit = (Criteria) iter.next();
					if (aliasCrit.getAlias().equals(orderByAlias))
						currentaliasexists = true;
				}

				/*
				 * Als we niks hebben gevonden maak hem dan aan, anders skip
				 * naar de volgende.
				 */
				if (!currentaliasexists) {
					orderByProperty = ReflectionUtil.findProperty(entityClass,
							baseOrderBy + orderByAlias);
					if (orderByProperty == null) {
						throw new HibernateException("Kon " + orderByAlias
								+ " niet vinden op " + entityClass);
					}

					ManyToOne orderByPropertyManyToOne = orderByProperty
							.getAnnotation(ManyToOne.class);
					JoinColumn orderByPropertyJoinColumn = orderByProperty
							.getAnnotation(JoinColumn.class);

					/*
					 * In dit geval zijn we alleen op zoek naar ManyToOne's
					 * omdat Column een simple type voorstelt en OneToMany een
					 * collectie. Wanneer we te maken hebben met een collectie
					 * of een OneToMany (ook een collectie) moet de gebruiker
					 * het zelf regelen, deze zijn beide nullable.
					 */
					if (orderByPropertyManyToOne == null
							|| orderByPropertyJoinColumn == null)
						throw new HibernateException(
								"Kon geen alias toevoegen aan de criteria, reden: property is geen ManyToOne of heeft geen JoinColumn annotatie. roep zelf `createAlias(\""
										+ baseOrderBy
										+ orderByAlias
										+ "\",\""
										+ orderByAlias
										+ "\", CriteriaSpecification.LEFT_JOIN);` aan.");
					/*
					 * wanneer alles klopt maar de property is nullable dan
					 * moeten we dit alsnog overslaan...
					 */
					else if (orderByPropertyJoinColumn.nullable())
						throw new HibernateException(
								"Kon geen alias toevoegen aan de criteria, reden: property is nullable. roep zelf `createAlias(\""
										+ orderByAlias
										+ "\",\""
										+ orderByAlias
										+ "\", CriteriaSpecification.LEFT_JOIN);` aan.");

					/*
					 * Alias bestaat nog niet, en de property klopt voor een
					 * inner join.
					 */
					criteria.createAlias(baseOrderBy + orderByAlias,
							orderByAlias);
				}

				baseOrderBy = orderByAlias + ".";
				processedOrderBy += baseOrderBy;
				if (totalOrderBy.indexOf(".", processedOrderBy.length()) > -1)
					orderByAlias = totalOrderBy.substring(processedOrderBy
							.length(), totalOrderBy.indexOf(".",
							processedOrderBy.length()));
				else
					endofalias = false;
			}
		}
	}

}
