/**
 * 
 */
package nl.mypalsports.service.adres;

import nl.mypalsports.entities.adres.Adres;
import nl.mypalsports.service.common.GenericService;
import nl.mypalsports.service.zoekfilter.AdresZoekFilter;

/**
 * @author Niels
 *
 */
public interface AdresService extends GenericService<Adres, AdresZoekFilter>
{

}
