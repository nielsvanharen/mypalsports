package nl.mypalsports.common.util;

import java.sql.Time;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;

import nl.mypalsports.common.util.klok.Dag;
import nl.mypalsports.common.util.klok.DefaultSysteemKlok;
import nl.mypalsports.common.util.klok.Klok;

/**
 * Simple utility class for working with dates. This class is not thread safe.
 */
public final class TimeUtil
{
	private static final Date MAX_DATE = new Date(Long.MAX_VALUE);

	private static final String[] DATE_FORMATS = new String[] { "dd-mm-yyyy", "ddmmyyyy", "dd-mmyyyy", "ddmm-yyyy",
			"d-mm-yyyy", "dd-m-yyyy", "dmyyyy", "d-m-yyyy", "dd-mm-yy", "ddmmyy", "d-mm-yy", "dd-m-yy", "d-m-yy",
			"ddmyy", "dmmyy", "dm-yy", "d-myy", "yyyymmdd", "yyyy-mm-dd", "dmyy", "d-m-y", "dd-m-y", "dd-mm-y",
			"d-mm-y" };

	/**
	 * Klok (voor testdoeleinden instelbare implementatie van de systeemklok)
	 */
	private static Klok klok = new DefaultSysteemKlok();

	/**
	 * Vervangt de klok implementatie met de nieuwe klok.
	 * <p>
	 * <B>GEBRUIK DEZE METHODE ALLEEN IN TESTS EN RESET DE KLOK IN DE
	 * TEARDOWN</B>
	 * 
	 * @return de vorige klok die vervangen is
	 */
	static Klok vervangKlok(Klok nieuweKlok)
	{
		Klok oudeKlok = klok;
		klok = nieuweKlok;
		return oudeKlok;
	}

	/**
	 * Reset de klok implementatie naar de Java systeemklok.
	 */
	static void resetKlok()
	{
		klok = new DefaultSysteemKlok();
	}

	/**
	 * Returns a new Date containing only the date information from the
	 * specified Date, all time information is set to 0.
	 * 
	 * @param date
	 * @return new date containing no time information.
	 * @deprecated gebruik {@link #datumVan(Date)} of
	 *             {@link #verwijderTijd(Date)}
	 */
	@Deprecated
	public static Date asDate(Date date)
	{
		return datumVan(date);
	}

	/**
	 * Returns a new Date containing only the date information from the
	 * specified Date, all time information is set to 0.
	 * 
	 * @param date
	 *            # ms since epoch
	 * @return new date containing no time information.
	 */
	public static Date asDate(long date)
	{
		Calendar calendar = Calendar.getInstance();
		calendar.setTimeInMillis(date);
		calendar.set(Calendar.HOUR_OF_DAY, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		calendar.set(Calendar.MILLISECOND, 0);
		return calendar.getTime();
	}

	/**
	 * Returns a date which corresponds to the parametervalues <br/>
	 * Month value is 0-based. e.g., 0 for January.
	 * 
	 * @param year
	 * @param month
	 * @param day
	 * @return The date
	 */
	public static Date asDate(int year, int month, int day)
	{
		Calendar calendar = Calendar.getInstance();
		calendar.set(year, month, day);
		calendar.set(Calendar.HOUR_OF_DAY, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		calendar.set(Calendar.MILLISECOND, 0);
		return calendar.getTime();
	}

	/**
	 * Returns a new date that is parsed from the given date string in iso
	 * format (yyyymmdd).
	 * 
	 * @param isoDateString
	 *            The date string to parse in yyyymmdd format.
	 * @return A new date.
	 */
	public static Date isoStringAsDate(String isoDateString)
	{
		if (isoDateString == null || isoDateString.equals("        "))
			return null;
		if (isoDateString.length() != 8)
			return null;
		int year = Integer.valueOf(isoDateString.substring(0, 4));
		int month = Integer.valueOf(isoDateString.substring(4, 6)) - 1; // -1
																		// omdat
																		// in
		// Java Januari=0,
		// en in het
		// bestand
		// Januari=1
		int day = Integer.valueOf(isoDateString.substring(6, 8));
		Calendar calendar = Calendar.getInstance();
		calendar.set(year, month, day);
		calendar.set(Calendar.HOUR_OF_DAY, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		calendar.set(Calendar.MILLISECOND, 0);
		return calendar.getTime();
	}

	/**
	 * Returns a new date/time that is parsed from the given date string in iso
	 * format (yyyymmddHHmmss).
	 * 
	 * @param isoDateTimeString
	 *            The date string to parse in yyyymmddHHmmss format.
	 * @return A new date.
	 */
	public static Date isoStringAsDateTime(String isoDateTimeString)
	{
		if (isoDateTimeString == null || isoDateTimeString.equals("") || isoDateTimeString.equals("              "))
			return null;
		if (isoDateTimeString.length() != 14)
			return null;
		int year = Integer.valueOf(isoDateTimeString.substring(0, 4));
		int month = Integer.valueOf(isoDateTimeString.substring(4, 6)) - 1; // -1
																			// omdat
																			// in
		// Java Januari=0,
		// en in het
		// bestand
		// Januari=1
		int day = Integer.valueOf(isoDateTimeString.substring(6, 8));
		int hour = Integer.valueOf(isoDateTimeString.substring(8, 10));
		int minute = Integer.valueOf(isoDateTimeString.substring(10, 12));
		int second = Integer.valueOf(isoDateTimeString.substring(12, 14));
		Calendar calendar = defaultCalendar();
		calendar.set(year, month, day);
		calendar.set(Calendar.HOUR_OF_DAY, hour);
		calendar.set(Calendar.MINUTE, minute);
		calendar.set(Calendar.SECOND, second);
		calendar.set(Calendar.MILLISECOND, 0);
		return calendar.getTime();
	}

	/**
	 * Tries to parse the given string as a date using the most common Dutch
	 * date formats.
	 * 
	 * @param value
	 *            The string to be parsed.
	 * @return The parsed date, or null if the string cannot be parsed as a
	 *         date.
	 */
	public static Date parseDateString(String value)
	{
		Date date = null;
		for (String dateFormat : DATE_FORMATS)
		{
			date = parseDateString(value, dateFormat);
			if (date != null)
				return date;
		}
		return null;
	}

	/**
	 * Parses the given date string into a date using the given date format.
	 * 
	 * @param dateString
	 *            The date string to be parsed.
	 * @param dateFormat
	 *            A string containing the format of the date string. MUST AT
	 *            LEAST contain the following: yyyy (the position of a 4-digit
	 *            year) or yy (the position of a 1-digit year) mm (the position
	 *            of a 2-digit month) or m (the position of a 1-digit month) dd
	 *            (the position of a 2-digit day of month) or d (the position of
	 *            a 1-digit day)
	 * @return The parsed date, or null if the String is not a valid date.
	 */
	public static Date parseDateString(String dateString, String dateFormat)
	{
		Integer[] dateValues = parseDateString(dateString, dateFormat, false);
		return parseDateValues(dateValues);
	}

	/**
	 * See {@link #parseDateString(String, String)}, only a GBA date allows for
	 * the day or month to be '0' or '00'. Thus a date such as 002010 or
	 * '00-00-2010' is allowed.
	 * 
	 * @param dateString
	 * @param dateFormat
	 * 
	 * @return The parsed date in the dd-mm-yyyy format, or null if the String
	 *         is not a valid date.
	 */
	public static String parseGBADateString(String dateString, String dateFormat)
	{
		final boolean isGBADate = true;
		Integer[] dateValues = parseDateString(dateString, dateFormat, isGBADate);
		return formatDateValues(dateValues, isGBADate);
	}

	/**
	 * @return Array containing the parsed day-month-year values, or null if
	 *         these are impossible.
	 */
	private static Integer[] parseDateString(String dateString, String dateFormat, boolean isGBADate)
	{
		if (dateString.length() != dateFormat.length())
			return null;

		int index = -1;
		while ((index = dateFormat.indexOf("-", index + 1)) > -1)
		{
			char checkChar = dateString.charAt(index);
			if (checkChar != '-' && checkChar != '.' && checkChar != ',' && checkChar != '/' && checkChar != '\\'
					&& checkChar != ' ' && checkChar != ':' && checkChar != ';')
				return null;
		}

		final int monthMinimum = (isGBADate) ? 0 : 1;
		final int dayMinimum = (isGBADate) ? 0 : 1;
		int year = 0;
		int month = 0;
		int day = 0;

		try
		{
			index = dateFormat.indexOf("yyyy");
			if (index > -1)
				year = Integer.valueOf(dateString.substring(index, index + 4));
			else if (dateFormat.indexOf("yy") > -1)
			{
				index = dateFormat.indexOf("yy");
				year = Integer.valueOf(dateString.substring(index, index + 2));

				if (year > -1 && index == dateString.length() - 2)
				{
					if (year > 30)
						year = 1900 + year;
					else
						year = 2000 + year;
				}
				// situatie voor bijvoorbeeld: 11-05-985
				// else if (year > -1 && index == dateString.length()-3)
				// }
				else
					return null;
			}
			else
			{
				index = dateFormat.indexOf("y");
				year = Integer.valueOf(dateString.substring(index, index + 1));
				if (year > -1 && index == dateString.length() - 1)
				{
					year = 2000 + year;
				}
				else
					return null;

			}
			// A date before 1900 or 'now'+20 is probably not valid.
			if (year < 1900 || (year - getCurrentYear() > 20))
				return null;

			index = dateFormat.indexOf("mm");
			if (index > -1)
			{
				month = Integer.valueOf(dateString.substring(index, index + 2));
			}
			else
			{
				index = dateFormat.indexOf("m");
				month = Integer.valueOf(dateString.substring(index, index + 1));
			}
			// No GBA means month 1 = 0 (January) along with maximum 11
			// (December).
			if (month < monthMinimum || month > 12)
				return null;

			index = dateFormat.indexOf("dd");
			if (index > -1)
			{
				day = Integer.valueOf(dateString.substring(index, index + 2));
			}
			else
			{
				index = dateFormat.indexOf("d");
				day = Integer.valueOf(dateString.substring(index, index + 1));
			}
			// if (!isGBADate && day == 0) // correct a possible 0 'typo'
			// day = 1;
			if (day < dayMinimum || day > 31)
				return null;
		}
		catch (NumberFormatException e)
		{
			return null;
		}
		catch (IndexOutOfBoundsException e)
		{
			return null;
		}
		catch (IllegalArgumentException e)
		{
			return null;
		}
		return new Integer[] { day, month, year };
	}

	private static Date parseDateValues(Integer[] dateValues)
	{
		if (dateValues == null || dateValues.length != 3)
			return null;

		int day = dateValues[0];
		int month = dateValues[1] - 1; // 1 = 0 = January
		int year = dateValues[2];

		// Wanneer de dag waarde voor die maand fout is dan mag de calendar
		// de maand niet wijzigen (bv 30-02-2009). We stellen dit tijdelijk
		// in om niet alles in deze Util te laten crashen.
		Calendar calendar = defaultCalendar();
		calendar.setLenient(false);
		calendar.set(year, month, day);
		calendar.set(Calendar.HOUR_OF_DAY, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		calendar.set(Calendar.MILLISECOND, 0);

		// Retrieve date, then back to default lenient.
		Date date = null;
		try
		{
			date = calendar.getTime();
		}
		catch (IllegalArgumentException e)
		{
		}
		finally
		{
			calendar.setLenient(true);
		}
		return date;
	}

	/**
	 * @return formats the given numbers as a date in the dd-mm-yyyy format.
	 *         Thus 1-1-2010 becomes 01-01-2010.
	 */
	final static String formatDateValues(Integer[] dateValues, boolean isGBADate)
	{
		if (dateValues == null || dateValues.length != 3)
			return null;

		int day = dateValues[0];
		int month = dateValues[1];
		if ((!isGBADate || (day != 0 && month != 0)) && parseDateValues(dateValues) == null)
			return null;

		String dayText = StringUtil.voegVoorloopnullenToe(day, 2);
		String monthText = StringUtil.voegVoorloopnullenToe(month, 2);
		return String.format("%s-%s-%d", dayText, monthText, Integer.valueOf(dateValues[2]));
	}

	/**
	 * @param date
	 * @return The date in format d MMMMM yyyy. Example: 3 maart 2007
	 */
	public static String formatDateOfficial(Date date)
	{
		if (date == null)
			return null;
		Locale loc = new Locale("nl", "NL");
		DateFormat format = new SimpleDateFormat("d MMMM yyyy", loc);

		return format.format(date);
	}

	/**
	 * Formats the given date to a String according to the normal Dutch date
	 * notation.
	 * 
	 * @param date
	 *            The date to format.
	 * @return A String containing the date
	 */
	public static String formatDate(Date date)
	{
		return formatDate(date, "-");
	}

	/**
	 * Formats the given date to a String according to the normal Dutch date
	 * notation.
	 * 
	 * @param date
	 *            The date to format.
	 * @param separator
	 * @return A String containing the date
	 */
	public static String formatDate(Date date, String separator)
	{
		if (date == null)
			return null;
		Calendar calendar = calendar(date);

		String day = String.valueOf(calendar.get(Calendar.DAY_OF_MONTH));
		if (day.length() == 1)
			day = "0" + day;
		String month = String.valueOf(calendar.get(Calendar.MONTH) + 1);
		if (month.length() == 1)
			month = "0" + month;
		String year = String.valueOf(calendar.get(Calendar.YEAR));
		return day + separator + month + separator + year;
	}

	/**
	 * @param date
	 * @param separator
	 * @return A String containing the date
	 */
	public static String formatDateZonderJaar(Date date, String separator)
	{
		Calendar calendar = calendar(date);

		String day = String.valueOf(calendar.get(Calendar.DAY_OF_MONTH));
		if (day.length() == 1)
			day = "0" + day;
		String month = String.valueOf(calendar.get(Calendar.MONTH) + 1);
		if (month.length() == 1)
			month = "0" + month;
		return day + separator + month;

	}

	/**
	 * Formateert de gegeven datum in de vorm 'Vrijdag 4 april 2008 11:00'
	 * 
	 * @param date
	 * @return de datumstring
	 */
	public static String formatDateTimeLong(Date date)
	{
		if (date == null)
			return null;
		Locale loc = new Locale("nl", "NL");
		DateFormat format = new SimpleDateFormat("EEEE d MMMM yyyy HH:mm", loc);

		return format.format(date);
	}

	/**
	 * Formateert de gegeven datum in het meegegeven format volgens
	 * SimpleDateFormat'
	 * 
	 * @param date
	 * @param stringFormat
	 * @return de datumstring
	 */
	public static String formatDateTime(Date date, String stringFormat)
	{
		if (date == null || stringFormat == null || stringFormat.isEmpty())
			return null;
		Locale loc = new Locale("nl", "NL");
		DateFormat format = new SimpleDateFormat(stringFormat, loc);
		return format.format(date);
	}

	/**
	 * Formats the given date/time to a String according to the normal Dutch
	 * date/time notation.
	 * 
	 * @param date
	 * @return A String containing the date
	 */
	public static String formatDateTime(Date date)
	{
		if (date == null)
			return null;

		Calendar calendar = calendar(date);
		String dateString = formatDate(date);
		String separator = ":";

		String hour = String.valueOf(calendar.get(Calendar.HOUR_OF_DAY));
		if (hour.length() == 1)
			hour = "0" + hour;
		String minute = String.valueOf(calendar.get(Calendar.MINUTE));
		if (minute.length() == 1)
			minute = "0" + minute;
		String second = String.valueOf(calendar.get(Calendar.SECOND));
		if (second.length() == 1)
			second = "0" + second;

		return dateString + " " + hour + separator + minute + separator + second;
	}

	/**
	 * Formats the given date/time to a String according to the normal Dutch
	 * date/time notation with the exception of showing the seconds.
	 * 
	 * @param date
	 * @return A String with the corresponding notation
	 */
	public static String formatDateTimeNoSeconds(Date date)
	{
		if (date == null)
			return "";

		Calendar calendar = calendar(date);

		String dateString = formatDate(date);
		String separator = ":";

		String hour = String.valueOf(calendar.get(Calendar.HOUR_OF_DAY));
		if (hour.length() == 1)
			hour = "0" + hour;
		String minute = String.valueOf(calendar.get(Calendar.MINUTE));
		if (minute.length() == 1)
			minute = "0" + minute;

		return dateString + " " + hour + separator + minute;
	}

	/**
	 * Formats the given date/time to a String according to the normal Dutch
	 * time notation.
	 * 
	 * @param date
	 * @param separator
	 * @return A String containing the hour and minute of the date
	 */
	public static String formatHourMinute(Date date, String separator)
	{
		Calendar calendar = calendar(date);
		String hour = String.valueOf(calendar.get(Calendar.HOUR_OF_DAY));
		if (hour.length() == 1)
			hour = "0" + hour;
		String minute = String.valueOf(calendar.get(Calendar.MINUTE));
		if (minute.length() == 1)
			minute = "0" + minute;

		return hour + separator + minute;
	}

	/**
	 * Formats the given date to a String according to the ISO standard, i.e.
	 * yyyy-mm-dd.
	 * 
	 * @param date
	 * @return A String containing the date
	 */
	public static String formatDateAsIsoString(Date date)
	{
		Calendar calendar = calendar(date);

		String day = String.valueOf(calendar.get(Calendar.DAY_OF_MONTH));
		if (day.length() == 1)
			day = "0" + day;
		String month = String.valueOf(calendar.get(Calendar.MONTH) + 1);
		if (month.length() == 1)
			month = "0" + month;
		String year = String.valueOf(calendar.get(Calendar.YEAR));
		return year + "-" + month + "-" + day;
	}

	/**
	 * Formats the given date to a compact String for use in for example id's or
	 * exports. The format is 'yyyymmddHHmiss'
	 * 
	 * @param date
	 *            The date to format
	 * @return A string containing the date/time.
	 */
	public static String formatDateTimeSystem(Date date)
	{
		Calendar calendar = calendar(date);

		String day = String.valueOf(calendar.get(Calendar.DAY_OF_MONTH));
		if (day.length() == 1)
			day = "0" + day;
		String month = String.valueOf(calendar.get(Calendar.MONTH) + 1);
		if (month.length() == 1)
			month = "0" + month;
		String year = String.valueOf(calendar.get(Calendar.YEAR));
		String hour = String.valueOf(calendar.get(Calendar.HOUR_OF_DAY));
		if (hour.length() == 1)
			hour = "0" + hour;
		String minute = String.valueOf(calendar.get(Calendar.MINUTE));
		if (minute.length() == 1)
			minute = "0" + minute;
		String second = String.valueOf(calendar.get(Calendar.SECOND));
		if (second.length() == 1)
			second = "0" + second;

		return year + month + day + hour + minute + second;
	}

	/**
	 * Formateert de gegeven datum naar het formaat dat gebruikt wordt in
	 * SchoolPlus databases. Dit komt overeen met yyyy-m-d, waarbij de maanden
	 * van 0 t/m 11 lopen.
	 * 
	 * @param date
	 * @return A String containing the date
	 */
	public static String formatDateAsSPlusDateString(Date date)
	{
		Calendar calendar = calendar(date);

		String day = String.valueOf(calendar.get(Calendar.DAY_OF_MONTH));
		String month = String.valueOf(calendar.get(Calendar.MONTH));
		String year = String.valueOf(calendar.get(Calendar.YEAR));
		return year + "-" + month + "-" + day;
	}

	/**
	 * Formats the given date to a String according to the format 'ddd dd-mm',
	 * for example: Ma 03-04
	 * 
	 * @param date
	 * @return A String containing the date
	 */
	public static String formatDate_ddd_dd_mm(Date date)
	{
		Calendar calendar = calendar(date);
		int dayOfWeek = calendar.get(Calendar.DAY_OF_WEEK) - Calendar.MONDAY + 1;
		String dayName = Dag.getDag(dayOfWeek).getAfkorting();
		String day = String.valueOf(calendar.get(Calendar.DAY_OF_MONTH));
		if (day.length() == 1)
			day = "0" + day;
		String month = String.valueOf(calendar.get(Calendar.MONTH) + 1);
		if (month.length() == 1)
			month = "0" + month;
		return dayName + " " + day + "-" + month;
	}

	/**
	 * Formateert de gegeven datum naar het formaat 'mm-yyyy', en geeft het als
	 * string terug. Bijvoorbeeld: 02-2010.
	 * 
	 * @param date
	 * @return Een string met de maand en het jaar.
	 */
	public static String formatDateAs_mm_yyyy(Date date)
	{
		Calendar calendar = calendar(date);
		String month = String.valueOf(calendar.get(Calendar.MONTH) + 1);
		if (month.length() == 1)
			month = "0" + month;
		String year = String.valueOf(calendar.get(Calendar.YEAR));
		return month + "-" + year;
	}

	/**
	 * Formateert de gegeven datum naar het formaat 'yyyy', en geeft het als
	 * string terug. Bijvoorbeeld: 2010.
	 * 
	 * @param date
	 * @return Een string met het jaar.
	 */
	public static String formatDateAs_yyyy(Date date)
	{
		Calendar calendar = calendar(date);

		String year = String.valueOf(calendar.get(Calendar.YEAR));
		return year;
	}

	/**
	 * Returns a new Date containing only time information, all date information
	 * is set to the epoch.
	 * 
	 * @param date
	 * @return new Date containing only time information
	 */
	public static Date asTime(Date date)
	{
		if (date == null)
			return null;
		return asTime(date.getTime());
	}

	/**
	 * Returns a new Date containing only time information, all date information
	 * is set to the epoch.
	 * 
	 * @param time
	 *            # ms since epoch
	 * @return new Date containing only time information
	 */
	public static Date asTime(long time)
	{
		Calendar calendar = defaultCalendar();
		calendar.setTimeInMillis(time);
		if (calendar instanceof GregorianCalendar)
			calendar.set(Calendar.ERA, GregorianCalendar.AD);
		// else we skip the era since it is subclass depended
		calendar.set(Calendar.YEAR, 1970);
		calendar.set(Calendar.DAY_OF_YEAR, 1);
		return calendar.getTime();
	}

	/**
	 * Returns a new Date containing date and time information. Milliseconds are
	 * set to 0 since not all databases support that field in a datetime field.
	 * 
	 * @param date
	 * @return new Date containing date and time informtaion
	 */
	public static Date asDateTime(Date date)
	{
		if (date == null)
			return null;
		return asDateTime(date.getTime());
	}

	/**
	 * Returns a new Date containing date and time information. Milliseconds are
	 * set to 0 since not all databases support that field in a datetime field.
	 * 
	 * @param datetime
	 *            # ms since epoch
	 * @return new Date containing date and time informtaion
	 */
	public static Date asDateTime(long datetime)
	{
		Calendar calendar = defaultCalendar();
		calendar.setTimeInMillis(datetime);
		calendar.set(Calendar.MILLISECOND, 0);
		return calendar.getTime();
	}

	/**
	 * @param date1
	 * @param date2
	 * @return true als de datums van date1 en date2 gelijk zijn. Time gedeelte
	 *         van de dates wordt genegeerd. Als beide dates null zijn, wordt
	 *         true teruggegeven. Als een van de twee dates null is, en de
	 *         andere niet, wordt false teruggegeven.
	 */
	public static boolean datesEqual(Date date1, Date date2)
	{
		if (date1 == null && date2 == null)
			return true;
		if (date1 == null || date2 == null)
			return false;
		return datumVan(date1).equals(datumVan(date2));
	}

	/**
	 * Returns true if the given check date is between the begin- and end date.
	 * The end date may be null.
	 * 
	 * @param beginDate
	 *            The begin of the interval (inclusive). May not be null.
	 * @param endDate
	 *            The end of the interval (inclusive). May be null.
	 * @param checkDate
	 *            The date to check.
	 * @return true if checkDate is between beginDate and endDate
	 */
	public static boolean dateBetween(Date beginDate, Date endDate, Date checkDate)
	{
		Asserts.assertNotNull("beginDate", beginDate);
		Asserts.assertNotNull("checkDate", checkDate);
		if (checkDate.before(beginDate))
			return false;
		if (endDate != null && checkDate.after(endDate))
			return false;

		return true;
	}

	/**
	 * Returns true if the given check date is between the begin and end date.
	 * The begin and end date may be null.
	 * 
	 * @param beginDate
	 *            The begin of the interval (inclusive). May be null.
	 * @param endDate
	 *            The end of the interval (inclusive). May be null.
	 * @param checkDate
	 *            The date to check
	 * @return true if checkDate is between beginDate and endDate. Begin date is
	 *         disregarded if null. End date is disregarded if null.
	 */
	public static boolean dateBetweenOrBeginEndIsNull(Date beginDate, Date endDate, Date checkDate)
	{
		Asserts.assertNotNull("checkDate", checkDate);
		if (beginDate != null && checkDate.before(beginDate))
			return false;
		if (endDate != null && checkDate.after(endDate))
			return false;

		return true;
	}

	/**
	 * Returns true if two intervals are overlapping.
	 * 
	 * @param beginDate1
	 *            Begin date of first interval.
	 * @param endDate1
	 *            End date of first interval.
	 * @param beginDate2
	 *            Begin date of second interval.
	 * @param endDate2
	 *            End date of sdecond interval.
	 * @return overlapping.
	 */
	public static boolean isOverlapping(Date beginDate1, Date endDate1, Date beginDate2, Date endDate2)
	{
		Asserts.assertNotNull("beginDate1", beginDate1);
		Asserts.assertNotNull("beginDate2", beginDate2);

		Date endDate1b = (endDate1 != null ? endDate1 : MAX_DATE);
		Date endDate2b = (endDate2 != null ? endDate2 : MAX_DATE);

		return (beginDate1.before(endDate2b) && beginDate2.before(endDate1b));
	}

	/**
	 * Returns a new instance of a TimeUtil. Previously, this method returned a
	 * thread local instance, but this could lead to corruption of the calendar
	 * instance between request, due to subtile changes to the fields of the
	 * calendar object, such as the timezone.
	 * 
	 * @return a TimeUtil.
	 * @deprecated gebruik de methodes static
	 */
	@Deprecated
	public static TimeUtil getInstance()
	{
		return new TimeUtil();
	}

	/**
	 * Returns a new Date representing the current date. time information is
	 * reset to 0.
	 * 
	 * @return new Date.
	 * @deprecated gebruik {@link #vandaag()}
	 */
	@Deprecated
	public static Date currentDate()
	{
		return vandaag();
	}

	/**
	 * Returns a new Date representing the current time. date information is
	 * reset to the epoch.
	 * 
	 * @return new Date.
	 */
	public static Date currentTime()
	{
		return asTime(System.currentTimeMillis());
	}

	/**
	 * Returns a new Date representing the current date and time. milliseconds
	 * are set to 0.
	 * 
	 * @return new Date.
	 * @deprecated gebruik {@link #nu()}
	 */
	@Deprecated
	public static Date currentDateTime()
	{
		return asDateTime(System.currentTimeMillis());
	}

	/**
	 * Returns the years as an int
	 * 
	 * @param date
	 * @return the year
	 * @deprecated gebruik {@link #jaar(Date)}
	 */
	@Deprecated
	public static int getYear(Date date)
	{
		return jaar(date);
	}

	/**
	 * Maak een nieuw Date object met dezelfde datum/tijd als het gegeven Date
	 * en met het gegeven jaartal.
	 * 
	 * @param date
	 * @param year
	 *            het jaartal
	 * @return gegeven datum in het gegeven jaar
	 */
	public static Date setYear(Date date, int year)
	{
		Calendar calendar = calendar(date);
		calendar.set(Calendar.YEAR, year);
		return datumVan(calendar.getTime());
	}

	/**
	 * Returns the current year
	 * 
	 * @return current year
	 */
	public static int getCurrentYear()
	{
		return jaar(nu());
	}

	/**
	 * Returns the day of the year
	 * 
	 * @param date
	 * @return the day of the year
	 */
	public static int getDayOfYear(Date date)
	{
		Calendar calendar = defaultCalendar();
		calendar.setTime(date);
		return calendar.get(Calendar.DAY_OF_YEAR);
	}

	/**
	 * Returns the time between date1 and date2.
	 * 
	 * @param date1
	 * @param date2
	 * @return Difference in time. The result is positive if date1 is after
	 *         date2.
	 */
	public static long getDifferenceInMillis(Date date1, Date date2)
	{
		if (date1 == null || date2 == null)
			return 0;
		return date1.getTime() - date2.getTime();
	}

	/**
	 * Returns the number of minutes between date1 and date2.
	 * 
	 * @param date1
	 * @param date2
	 * @return Difference in minutes. The result is positive if date1 is after
	 *         date2.
	 */
	public static int getDifferenceInMinutes(Date date1, Date date2)
	{
		if (date1 == null || date2 == null)
			return 0;
		long millis = date1.getTime() - date2.getTime();
		long seconds = millis / 1000;
		long minutes = seconds / 60;
		return (int) minutes;
	}

	/**
	 * Returns the number of full years between two dates.
	 * 
	 * @param date1
	 * @param date2
	 * @return Number of years. The result is positive if date1 is after date2.
	 */
	public static int getDifferenceInYears(Date date1, Date date2)
	{
		if (date1 == null || date2 == null)
			return 0;
		int years = jaar(date1) - jaar(date2);
		int months = javaMaand(date1) - javaMaand(date2);
		int days = dag(date1) - dag(date2);
		if (months > 0 || (months == 0 && days >= 0))
			return years;
		return years - 1;
	}

	/**
	 * geeft het verschil in dagen terug
	 * 
	 * @param a
	 * @param b
	 * @return aantal dagen
	 */
	public static int getDifferenceInDays(Date a, Date b)
	{
		int tempDifference = 0;
		int difference = 0;
		Calendar earlier = Calendar.getInstance();
		Calendar later = Calendar.getInstance();

		if (a.compareTo(b) < 0)
		{
			earlier.setTime(a);
			later.setTime(b);
		}
		else
		{
			earlier.setTime(b);
			later.setTime(a);
		}

		while (earlier.get(Calendar.YEAR) != later.get(Calendar.YEAR))
		{
			tempDifference = 365 * (later.get(Calendar.YEAR) - earlier.get(Calendar.YEAR));
			difference += tempDifference;

			earlier.add(Calendar.DAY_OF_YEAR, tempDifference);
		}

		if (earlier.get(Calendar.DAY_OF_YEAR) != later.get(Calendar.DAY_OF_YEAR))
		{
			tempDifference = later.get(Calendar.DAY_OF_YEAR) - earlier.get(Calendar.DAY_OF_YEAR);
			difference += tempDifference;
		}
		return difference;
	}

	/**
	 * geeft het verschil in hele maanden terug
	 * 
	 * @param a
	 * @param b
	 * @return aantal maanden
	 */
	public static int getDifferenceInMonths(Date a, Date b)
	{
		int difference = 0;
		Calendar earlier = Calendar.getInstance();
		Calendar later = Calendar.getInstance();

		if (a.compareTo(b) < 0)
		{
			earlier.setTime(a);
			later.setTime(b);
		}
		else
		{
			earlier.setTime(b);
			later.setTime(a);
		}

		difference += 12 * (later.get(Calendar.YEAR) - earlier.get(Calendar.YEAR));
		earlier.set(Calendar.YEAR, later.get(Calendar.YEAR));
		difference += later.get(Calendar.MONTH) - earlier.get(Calendar.MONTH);
		earlier.set(Calendar.MONTH, later.get(Calendar.MONTH));
		if (earlier.after(later))
			difference--;
		return difference;
	}

	/**
	 * Returns the number of workdays between two dates
	 * 
	 * @param a
	 * @param b
	 * @return the number of workdays
	 */
	public static int getDifferenceInWorkDays(Date a, Date b)
	{
		Date earlier = a;
		Date later = b;
		if (later.before(earlier))
		{
			earlier = b;
			later = a;
		}
		int days = getDifferenceInDays(earlier, later);
		return getWorkDays(earlier, days);
	}

	/**
	 * @return Een datum in de verre toekomst die gebruikt kan worden voor
	 *         queries die geacht worden om alle records ongeacht de tijd te
	 *         returneren.
	 */
	public static Date getMaxDate()
	{
		// Datum in de verre toekomst.
		Calendar calendar = defaultCalendar();
		calendar.set(2999, 11, 31);
		return datumVan(calendar.getTime());
	}

	/**
	 * @return Een datum in het verre verleden die gebruikt kan worden voor
	 *         queries die geacht worden om alle records ongeacht de tijd te
	 *         returneren.
	 */
	public static Date getMinDate()
	{
		// Datum in het verre verleden.
		Calendar calendar = defaultCalendar();
		calendar.set(1900, 0, 1);
		return datumVan(calendar.getTime());
	}

	/**
	 * Voegt days dagen toe aan de gegeven datum. days mag negatief zijn.
	 * 
	 * @param date
	 * @param days
	 * @return nieuwe date
	 * @deprecated gebruik {@link #addDag(Date, int)}
	 */
	@Deprecated
	public static Date addDays(Date date, int days)
	{
		return addDag(date, days);
	}

	/**
	 * Voegt weeks weken toe aan de gegeven datum. Weeks mag negatief zijn.
	 * 
	 * @param date
	 * @param weeks
	 * @return nieuwe date
	 * @deprecated gebruik {@link #addWeek(Date, int)}
	 */
	@Deprecated
	public static Date addWeeks(Date date, int weeks)
	{
		return addWeek(date, weeks);
	}

	/**
	 * Voegt months maanden toe aan de gegeven datum. Months mag negatief zijn.
	 * 
	 * @param date
	 * @param months
	 * @return nieuwe month
	 * @deprecated gebruik {@link #addMaand(Date, int)}
	 */
	@Deprecated
	public static Date addMonths(Date date, int months)
	{
		return addMaand(date, months);
	}

	/**
	 * Voegt years jaren toe aan de gegeven datum. years mag negatief zijn
	 * 
	 * @param date
	 * @param years
	 * @return De gegeven datum + years jaren
	 * @deprecated gebruik {@link #addJaar(Date, int)}
	 */
	@Deprecated
	public static Date addYears(Date date, int years)
	{
		return addJaar(date, years);
	}

	/**
	 * Voegt hours toe aan de gegeven datum.
	 * 
	 * @param date
	 * @param hours
	 * @return De gegeven datum + hours uren
	 * @deprecated gebruik {@link #addUur(Date, int)}
	 */
	@Deprecated
	public static Date addHours(Date date, int hours)
	{
		return addUur(date, hours);
	}

	/**
	 * Voegt minutes minuten toe aan de gegeven datum.
	 * 
	 * @param date
	 * @param minutes
	 * @return De gegeven datum + minutes minuten
	 * @deprecated gebruik {@link #addMinuut(Date, int)}
	 */
	@Deprecated
	public static Date addMinutes(Date date, int minutes)
	{
		return addMinuut(date, minutes);
	}

	/**
	 * Voegt seconds seconden toe aan de gegeven datum.
	 * 
	 * @param date
	 * @param seconds
	 * @return De gegeven datum + seconds seconden
	 * @deprecated gebruik {@link #addSeconde(Date, int)}
	 */
	@Deprecated
	public static Date addSeconds(Date date, int seconds)
	{
		return addSeconde(date, seconds);
	}

	/**
	 * Voegt milliseconds seconden toe aan de gegeven datum.
	 * 
	 * @param date
	 * @param milliseconds
	 * @return De gegeven datum + milliseconds milliseconden
	 * @deprecated gebruik {@link #addMillis(Date, int)}
	 */
	@Deprecated
	public static Date addMiliseconds(Date date, int milliseconds)
	{
		return addMillis(date, milliseconds);
	}

	/**
	 * geeft de datum van volgende werkdag op basis van de opgegeven datum
	 * 
	 * @param date
	 * @return De volgende werkdag.
	 */
	public static Date nextWorkDay(Date date)
	{
		int day = getDayOfWeek(date);
		if (day == Calendar.FRIDAY)
			return addDag(date, 3);
		return addDag(date, 1);
	}

	/**
	 * vergelijkt de twee dates op dagen, maanden en jaren
	 * 
	 * @param date1
	 * @param date2
	 * @return <code>true</code> als de twee dezelfde dag zijn
	 */
	public static boolean isZelfdeDatum(Date date1, Date date2)
	{
		return JavaUtil.equalsOrBothNull(dag(date1), dag(date2))
				&& JavaUtil.equalsOrBothNull(javaMaand(date1), javaMaand(date2))
				&& JavaUtil.equalsOrBothNull(jaar(date1), jaar(date2));
	}

	/**
	 * vergelijkt de twee dates op dag in de week (ma, di, etc), maanden en
	 * jaren
	 * 
	 * @param date1
	 * @param date2
	 * @return of het dezelfde dag is.
	 */
	public static boolean isZelfdeDagInWeek(Date date1, Date date2)
	{
		return (getDayOfWeek(date1) == getDayOfWeek(date2) && javaMaand(date1) == javaMaand(date2) && jaar(date1) == jaar(date2));
	}

	/**
	 * geeft de datum van de werkdag die volgt na de gegeven datum
	 * 
	 * @param date
	 * @return de vorige werkdag (previousWorkDay(maandag)==vrijdag)
	 */
	public static Date previousWorkDay(Date date)
	{
		int day = getDayOfWeek(date);
		if (day == Calendar.MONDAY)
		{
			int days = -3;
			return addDag(date, days);
		}
		int days1 = -1;
		return addDag(date, days1);
	}

	/**
	 * Geeft de datum terug na het aantal
	 * 
	 * @param date
	 *            huidige datum
	 * @param workdays
	 *            het aantal werkdagen
	 * @return De date
	 */
	public static Date addWorkDays(Date date, int workdays)
	{
		Calendar calendar = calendar(date);
		Date returnDate = calendar.getTime();

		if (workdays < 0)
		{
			for (int i = 0; i > workdays; i--)
			{
				returnDate = previousWorkDay(returnDate);
			}
		}
		else
		{
			for (int i = 0; i < workdays; i++)
			{
				returnDate = nextWorkDay(returnDate);
			}
		}

		return returnDate;
	}

	/**
	 * geeft het aantal werkdagen, telt vanaf date+1 t/m date+days
	 * 
	 * @param date
	 * @param days
	 * @return het aantal werkdagen vanaf date tot het aantal dagen daarna
	 */
	public static int getWorkDays(Date date, int days)
	{
		int workDays = days;
		if (workDays < 0)
			Math.abs(workDays);
		workDays = workDays - getWeekendDays(date, days);
		return workDays;
	}

	/**
	 * geeft het aantal weekenddagen, telt vanaf date+1 t/m date+days
	 * 
	 * @param date
	 * @param days
	 * @return het aantal weekenddagen
	 */
	public static int getWeekendDays(Date date, int days)
	{
		Date tempDate = date;
		int tempDays = days;

		boolean future = days >= 0;
		if (!future)
		{
			int days1 = tempDays - 1;
			tempDate = addDag(tempDate, days1);
			tempDays = Math.abs(tempDays);
		}
		int fixedSunday = Calendar.SUNDAY;
		int dateDay = getDayOfWeek(tempDate);
		if (Calendar.SUNDAY < Calendar.SATURDAY)
		{
			fixedSunday = Calendar.SATURDAY + 1;
			if (dateDay == Calendar.SUNDAY)
				dateDay = fixedSunday;
		}
		int weekendDays = (tempDays / 7) * 2;
		int dayRest = tempDays % 7;
		if (dayRest > 0)
		{
			if (dateDay < Calendar.SATURDAY)
			{
				weekendDays += (dateDay + dayRest) / Calendar.SATURDAY;
				weekendDays += (dateDay + dayRest) / fixedSunday;
			}
			else
			{
				if (dateDay == Calendar.SATURDAY)
					weekendDays += 1;
				else if (dayRest == 6) // sunday
					weekendDays += 1;
			}
		}
		return weekendDays;
	}

	/**
	 * Geeft het ISO weeknummer van de gegeven datum.
	 * 
	 * @param date
	 * @return het weeknummer
	 */
	public static int getWeekOfYear(Date date)
	{
		Calendar calendar = calendar(date);
		return calendar.get(Calendar.WEEK_OF_YEAR);
	}

	/**
	 * Geeft het ISO weeknummer van de huidige systeemdatum.
	 * 
	 * @return het weeknummer
	 */
	public static int getCurrentWeekOfYear()
	{
		return getWeekOfYear(nu());
	}

	/**
	 * Geeft de dag van de week van de gegeven datum als getal.
	 * 
	 * @param date
	 * @return De dag van de week. Zondag=1, Maandag=2 etc.
	 */
	public static int getDayOfWeek(Date date)
	{
		Calendar calendar = calendar(date);
		return calendar.get(Calendar.DAY_OF_WEEK);
	}

	/**
	 * Geeft een list met de data van de werkdagen van de gegeven week.
	 * 
	 * @param year
	 * @param week
	 * @return lijst van data
	 */
	public static List<Date> getWeekDates(int year, int week)
	{
		List<Date> weekDates = new ArrayList<Date>(5);
		for (int i = 1; i <= 5; i++)
		{
			Calendar calendar = defaultCalendar();
			calendar.set(Calendar.YEAR, year);
			calendar.set(Calendar.WEEK_OF_YEAR, week);
			calendar.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY + i - 1);
			weekDates.add(datumVan(calendar.getTime()));
		}
		return weekDates;
	}

	/**
	 * Geeft de datum van de gegeven combinatie van jaar, week en weekdagnummer.
	 * 
	 * @param year
	 * @param week
	 * @param weekday
	 * @return de datum
	 */
	public static Date getDate(int year, int week, int weekday)
	{
		Calendar calendar = defaultCalendar();
		calendar.set(Calendar.YEAR, year);
		calendar.set(Calendar.WEEK_OF_YEAR, week);
		calendar.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY + weekday - 1);

		return datumVan(calendar.getTime());
	}

	/**
	 * Geeft de eerste dag van de gegeven maand.
	 * 
	 * @param year
	 * @param month
	 *            De maand (0-11)
	 * @return De eerste dag van de gegeven maand
	 */
	public static Date getFirstDayOfMonth(int year, int month)
	{
		Calendar calendar = defaultCalendar();
		calendar.set(Calendar.YEAR, year);
		calendar.set(Calendar.MONTH, month);
		calendar.set(Calendar.DAY_OF_MONTH, 1);

		return datumVan(calendar.getTime());
	}

	/**
	 * @param date
	 * @return first day of the month.
	 */
	public static Date getFirstDayOfMonth(Date date)
	{
		Calendar calendar = calendar(date);
		calendar.set(Calendar.DAY_OF_MONTH, 1);

		return datumVan(calendar.getTime());
	}

	/**
	 * @param date
	 * @return first day of the week.
	 */
	public static Date getFirstDayOfWeek(Date date)
	{
		Calendar calendar = calendar(date);
		calendar.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);

		return datumVan(calendar.getTime());
	}

	/**
	 * @param date
	 * @return first day of the year.
	 */
	public static Date getFirstDayOfYear(Date date)
	{
		Calendar calendar = calendar(date);
		calendar.set(Calendar.DAY_OF_YEAR, 1);

		return datumVan(calendar.getTime());
	}

	/**
	 * Geeft een array met de begin- en einddatum van de gegeven week. De
	 * begindatum van de week is altijd een maandag, en de einddatum is altijd
	 * een zondag.
	 * 
	 * @param jaar
	 * @param week
	 * @return een array met twee data
	 */
	public static Date[] getWeekBeginEnEindDatum(int jaar, int week)
	{
		Date[] dates = new Date[2];
		Calendar calendar = defaultCalendar();
		calendar.set(Calendar.YEAR, jaar);
		calendar.set(Calendar.WEEK_OF_YEAR, week);
		calendar.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
		dates[0] = datumVan(calendar.getTime());
		calendar.set(Calendar.YEAR, jaar);
		calendar.set(Calendar.WEEK_OF_YEAR, week);
		calendar.set(Calendar.DAY_OF_WEEK, Calendar.SUNDAY);
		dates[1] = datumVan(calendar.getTime());

		return dates;
	}

	/**
	 * Geeft een array met de begin- en einddatum van de werk week. De
	 * begindatum van de week is altijd een maandag (00:00), en de einddatum is
	 * altijd een zaterdag (00:00).
	 * 
	 * @param jaar
	 * @param week
	 * @return een array met twee data
	 */
	public static Date[] getWerkWeekBeginEnEindDatum(int jaar, int week)
	{
		Date[] dates = new Date[2];
		Calendar calendar = defaultCalendar();
		calendar.set(Calendar.YEAR, jaar);
		calendar.set(Calendar.WEEK_OF_YEAR, week);
		calendar.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
		dates[0] = datumVan(calendar.getTime());
		calendar.set(Calendar.YEAR, jaar);
		calendar.set(Calendar.WEEK_OF_YEAR, week);
		calendar.set(Calendar.DAY_OF_WEEK, Calendar.SATURDAY);
		dates[1] = datumVan(calendar.getTime());

		return dates;
	}

	/**
	 * Geeft een tijd terug die overeenkomt met de string.
	 * 
	 * @param value
	 * @return De tijd die correspondeert met de string, <code>null</code> als
	 *         niet succesvol
	 */
	public static Time parseTimeString(String value)
	{
		if (StringUtil.isEmpty(value))
			return null;
		Time tijd = null;

		if (value.contains(":"))
		{
			tijd = parseTimeString(value, "HH:mm");
			if (tijd != null)
				return tijd;
			tijd = parseTimeString(value, "H:mm");
			if (tijd != null)
				return tijd;
		}
		if (value.contains("."))
		{
			tijd = parseTimeString(value, "HH.mm");
			if (tijd != null)
				return tijd;
			tijd = parseTimeString(value, "H.mm");
			if (tijd != null)
				return tijd;
		}

		tijd = parseTimeString(value, "HHmm");
		if (tijd != null)
			return tijd;
		tijd = parseTimeString(value, "Hmm");
		if (tijd != null)
			return tijd;
		tijd = parseTimeString(value, "HH");
		if (tijd != null)
			return tijd;

		return null;
	}

	/**
	 * Parst een string naar een datum adhv een formaat
	 * 
	 * @param dateString
	 * @param dateFormat
	 * @return De datum en <code>null</code> bij fout
	 */
	public static Time parseTimeString(String dateString, String dateFormat)
	{
		int index = 0;

		int hour = 0;
		int minute = 0;
		int second = 0;
		try
		{
			index = dateFormat.indexOf("HH");
			if (index > -1)
			{
				hour = Integer.valueOf(dateString.substring(index, index + 2));
			}
			else
			{
				index = dateFormat.indexOf("H");
				if (index > -1)
				{
					hour = Integer.valueOf(dateString.substring(index, index + 1));
					if (hour < 0 || hour > 23)
						hour = 0;
				}
			}

			index = dateFormat.indexOf("mm");
			if (index > -1)
			{
				minute = Integer.valueOf(dateString.substring(index, index + 2));
			}
			else
			{
				index = dateFormat.indexOf("m");
				if (index > -1)
				{
					minute = Integer.valueOf(dateString.substring(index, index + 1));
					if (minute < 0 || minute > 60)
						minute = 0;
				}
			}

			index = dateFormat.indexOf("ss");
			if (index > -1)
			{
				second = Integer.valueOf(dateString.substring(index, index + 2));
			}
			else
			{
				index = dateFormat.indexOf("s");
				if (index > -1)
				{
					second = Integer.valueOf(dateString.substring(index, index + 1));
					if (second < 0 || second > 60)
						second = 0;
				}
			}
		}
		catch (NumberFormatException e)
		{
			return null;
		}
		catch (IndexOutOfBoundsException e)
		{
			return null;
		}

		Calendar cal = new GregorianCalendar();
		cal.set(Calendar.HOUR_OF_DAY, hour);
		cal.set(Calendar.MINUTE, minute);
		cal.set(Calendar.SECOND, second);

		return new Time(asTime(cal.getTime()).getTime());
	}

	/**
	 * @param nummer
	 * @return De naam van de gegeven weekdag. 1=Zondag etc.
	 */
	public static String getWeekdagNaamMetZondagIs_1(int nummer)
	{
		switch (nummer)
		{
		case 1:
			return "Zondag";
		case 2:
			return "Maandag";
		case 3:
			return "Dinsdag";
		case 4:
			return "Woensdag";
		case 5:
			return "Donderdag";
		case 6:
			return "Vrijdag";
		case 7:
			return "Zaterdag";

		default:
			return "Onbekende dag";
		}

	}

	/**
	 * @param nummer
	 * @return De naam van de gegeven weekdag. 1=Maandag etc.
	 */
	public static String getWeekdagNaam(int nummer)
	{
		switch (nummer)
		{
		case 1:
			return "Maandag";
		case 2:
			return "Dinsdag";
		case 3:
			return "Woensdag";
		case 4:
			return "Donderdag";
		case 5:
			return "Vrijdag";
		case 6:
			return "Zaterdag";
		case 7:
			return "Zondag";

		default:
			return "Onbekende dag";
		}

	}

	/**
	 * @param nummer
	 *            Het nummer van de maand, 1=Jan, 2=Feb etc.
	 * @return De naam van de gegeven maand. 1=Jan, 2=Feb etc.
	 */
	public static String getMaandNaam(int nummer)
	{
		switch (nummer)
		{
		case 1:
			return "Jan";
		case 2:
			return "Feb";
		case 3:
			return "Mar";
		case 4:
			return "Apr";
		case 5:
			return "Mei";
		case 6:
			return "Jun";
		case 7:
			return "Jul";
		case 8:
			return "Aug";
		case 9:
			return "Sep";
		case 10:
			return "Okt";
		case 11:
			return "Nov";
		case 12:
			return "Dec";

		default:
			return "Onbekende maand";
		}
	}

	/**
	 * @param nummer
	 *            Het nummer van de maand, 1=Januari, 2=Februari etc.
	 * @return De naam van de gegeven maand. 1=Januari, 2=Februari etc.
	 */
	public static String getMaandNaamVolledig(int nummer)
	{
		switch (nummer)
		{
		case 1:
			return "Januari";
		case 2:
			return "Februari";
		case 3:
			return "Maart";
		case 4:
			return "April";
		case 5:
			return "Mei";
		case 6:
			return "Juni";
		case 7:
			return "Juli";
		case 8:
			return "Augustus";
		case 9:
			return "September";
		case 10:
			return "Oktober";
		case 11:
			return "November";
		case 12:
			return "December";

		default:
			return "Onbekende maand";
		}
	}

	/**
	 * @param naam
	 *            Tekstuele representatie van een maand, bijvoorbeeld "April".
	 * @return Het maandnummer, bijvoorbeeld 4.
	 */
	public static Integer getMaandNummer(String naam)
	{
		if (StringUtil.isEmpty(naam))
			return null;

		for (int i = 1; i <= 12; i++)
		{
			if (naam.toLowerCase().trim().equals(getMaandNaamVolledig(i).toLowerCase()))
				return i;
		}

		return null;
	}

	/**
	 * Voegt de tijd toe aan de date
	 * 
	 * @param date
	 * @param tijd
	 * @return De nieuwe date met de tijd informatie
	 */
	public static Date setTimeOnDate(Date date, Time tijd)
	{
		Calendar calendar = calendar(tijd);

		int hours = calendar.get(Calendar.HOUR_OF_DAY);
		int minutes = calendar.get(Calendar.MINUTE);
		int seconds = calendar.get(Calendar.SECOND);

		calendar.setTime(date);
		calendar.set(Calendar.HOUR_OF_DAY, hours);
		calendar.set(Calendar.MINUTE, minutes);
		calendar.set(Calendar.SECOND, seconds);

		Date datum = calendar.getTime();
		return datum;
	}

	/**
	 * Zet de tijd van de gegeven datum op het einde van de dag.
	 * 
	 * Als je wilt controleren of een datum voor het einde van een bepaalde dag
	 * valt, gebruik dan "x < maakBeginVanVolgendeDagVanDatum(y)" in plaats van
	 * "x <= maakEindeVanDagVanDatum(y)"
	 * 
	 * @param datum
	 *            De datum waarvan de tijd gezet moet worden
	 * @return een datum waarvan de tijd gelijk is gezet aan 23:59:59
	 */
	public static Date maakEindeVanDagVanDatum(Date datum)
	{
		if (datum == null)
			return null;

		Calendar calendar = calendar(datum);
		calendar.set(Calendar.HOUR_OF_DAY, 23);
		calendar.set(Calendar.MINUTE, 59);
		calendar.set(Calendar.SECOND, 59);
		calendar.set(Calendar.MILLISECOND, 0);

		return calendar.getTime();
	}

	/**
	 * Zet de tijd van de gegeven datum op het einde van de dag.
	 * 
	 * @param datum
	 *            De datum waarvan de tijd gezet moet worden
	 * @return een datum waarvan de tijd gelijk is gezet aan 00:00:00
	 */
	public static Date maakBeginVanDagVanDatum(Date datum)
	{
		if (datum == null)
			return null;
		Calendar calendar = calendar(datum);
		calendar.set(Calendar.HOUR_OF_DAY, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		calendar.set(Calendar.MILLISECOND, 0);

		return calendar.getTime();
	}

	/**
	 * Zet de tijd van de gegeven datum op het begin van de volgende dag.
	 * 
	 * @param datum
	 *            De datum dat gewijzigd moet worden
	 * @return een datum waarvan de tijd gelijk is gezet aan 00:00:00 op de
	 *         volgende dag
	 */
	public static Date maakBeginVanVolgendeDagVanDatum(Date datum)
	{
		if (datum == null)
			return null;
		Calendar calendar = calendar(datum);
		calendar.add(Calendar.DATE, 1);
		calendar.set(Calendar.HOUR_OF_DAY, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		calendar.set(Calendar.MILLISECOND, 0);

		return calendar.getTime();
	}

	/**
	 * Geeft een string terug met daarin de tijd-informatie
	 * 
	 * @param date
	 * @return Een string van de tijd
	 */
	public static String getTimeString(Date date)
	{
		Calendar calendar = calendar(date);
		int hours = calendar.get(Calendar.HOUR_OF_DAY);
		int minutes = calendar.get(Calendar.MINUTE);
		String min;
		if (minutes < 10)
		{
			min = "0" + String.valueOf(minutes);
		}
		else
		{
			min = String.valueOf(minutes);
		}
		return hours + ":" + min;
	}

	/**
	 * Geeft een string terug met daarin een omschrijving van de datum in de
	 * vorm: toekomstig, vandaag, gisteren, deze maand, dit schooljaar,
	 * schooljaar XXXX
	 * 
	 * @param date
	 * @return omschrijving van de datum
	 */
	public static String getDateGroup(Date date)
	{
		// morgen of later
		Calendar calendar = calendar(vandaag());
		calendar.add(Calendar.DATE, 1);

		if (!calendar.getTime().after(date))
			return "Toekomstig";

		// vandaag
		calendar.add(Calendar.DATE, -1);
		if (!calendar.getTime().after(date))
			return "Vandaag";

		// gisteren
		calendar.add(Calendar.DATE, -1);
		if (!calendar.getTime().after(date))
			return "Gisteren";

		// deze maand
		calendar.add(Calendar.DATE, 1); // naar vandaag
		calendar.set(Calendar.DAY_OF_MONTH, 1); // naar begin van de maand
		if (!calendar.getTime().after(date))
			return "Deze maand";

		// dit schooljaar
		calendar.add(Calendar.MONTH, -Calendar.AUGUST); // naar kalenderjaar van
														// begin
		// schooljaar
		calendar.set(Calendar.MONTH, Calendar.JULY); // naar 1 juli
		if (!calendar.getTime().after(date))
			return "Dit schooljaar";

		// bereken schooljaar van de datum
		calendar.setTime(date);
		calendar.add(Calendar.MONTH, -Calendar.AUGUST); // naar kalenderjaar van
														// begin
		// schooljaar
		int kalenderjaar = calendar.get(Calendar.YEAR);

		return "Schooljaar " + Integer.toString(kalenderjaar) + " / " + Integer.toString(kalenderjaar + 1);
	}

	/**
	 * @param datum
	 * @param tijd
	 * @return de gemergde datum.
	 */
	public static Date mergeDateAndTime(Date datum, Date tijd)
	{
		Calendar calendarDate = calendar(datum);

		calendarDate.set(Calendar.HOUR_OF_DAY, uur(tijd));
		calendarDate.set(Calendar.MINUTE, minuut(tijd));

		return calendarDate.getTime();
	}

	/**
	 * @param datum
	 * @return de datum in tijd.
	 */
	public static Time getDateAsTime(Date datum)
	{
		Calendar cal = new GregorianCalendar();
		cal.setTime(datum);
		return new Time(asTime(cal.getTime()).getTime());
	}

	/**
	 * @param date
	 * @param month
	 * @param day
	 * @return De eerstvolgende datum na de gegeven datum die op de gegeven
	 *         maand en dag valt. Als date bijvoorbeeld 01-10-2009 is, en
	 *         month=Februari en day=1, geeft de methode 01-02-2010. De maand is
	 *         0-based, oftewel Jan=0, Feb=1, ..., Dec=11
	 */
	public static Date getNextOccurrenceOfDate(Date date, int month, int day)
	{
		Date res = asDate(jaar(date), month, day);
		if (!res.after(date))
		{
			res = addJaar(res, 1);
		}
		return res;
	}

	/**
	 * @param time
	 * @return de tijd zonder seconden.
	 */
	public static Date getTimeWithoutSeconds(Date time)
	{
		Calendar calendar = defaultCalendar();
		calendar.setTime(time);
		calendar.set(Calendar.SECOND, 0);
		return new Time(asTime(time.getTime()).getTime());
	}

	/**
	 * @param jaar
	 * @return Het aantal weken in het gegeven jaar volgens ISO 8601. De laatste
	 *         week van het jaar is de week die de datum 28 december bevat. Dit
	 *         houdt in dat week 1 van het volgende jaar in potentie de datum 29
	 *         december van het jaar daarvoor kan bevatten.
	 */
	public static int getAantalWekenInJaar(int jaar)
	{
		Date datum = isoStringAsDate(jaar + "1228");
		return getWeekOfYear(datum);
	}

	/**
	 * Bepaalt het jaartal (inclusief de eeuw notatie) van de datum.
	 * "01-01-1990" levert dus 1990 op, en niet 90 (zoals
	 * {@link java.util.Date#getYear()} doet). Geeft <tt>null</tt> als datum
	 * <tt>null</tt> is.
	 * 
	 * @param datum
	 * @return het jaartal.
	 */
	public static Integer jaar(Date datum)
	{
		return getDatumVeld(datum, Calendar.YEAR);
	}

	/**
	 * Bepaalt de maand van de datum. Januari is 1, december is 12. Geeft
	 * <tt>null</tt> als datum <tt>null</tt> is.
	 * 
	 * @param datum
	 * @return het maandnummer.
	 */
	public static Integer maand(Date datum)
	{
		return getDatumVeld(datum, Calendar.MONTH) + 1;
	}

	/**
	 * Bepaalt de maand van de datum. Januari is 0, december is 11, undecimber
	 * is 12. Geeft <tt>null</tt> als datum <tt>null</tt> is.
	 * 
	 * @param datum
	 * @return het nummer van de maand.
	 */
	public static Integer javaMaand(Date datum)
	{
		return getDatumVeld(datum, Calendar.MONTH);
	}

	/**
	 * Bepaalt het dagnummer in de maand van de datum, startend vanaf 1. Geeft
	 * <tt>null</tt> als datum <tt>null</tt> is.
	 * 
	 * @param datum
	 * @return dagnummer in de maand van de datum.
	 */
	public static Integer dag(Date datum)
	{
		return getDatumVeld(datum, Calendar.DAY_OF_MONTH);
	}

	/**
	 * Bepaalt het uur van de dag op basis van de 24-uurs klok. Geeft
	 * <tt>null</tt> als datum <tt>null</tt> is.
	 * 
	 * @param datum
	 * @return het uur van de dag.
	 */
	public static Integer uur(Date datum)
	{
		return getDatumVeld(datum, Calendar.HOUR_OF_DAY);
	}

	/**
	 * Bepaalt de minuten in het uur. Geeft <tt>null</tt> als datum
	 * <tt>null</tt> is.
	 * 
	 * @param datum
	 * @return de minuut van het uur.
	 */
	public static Integer minuut(Date datum)
	{
		return getDatumVeld(datum, Calendar.MINUTE);
	}

	/**
	 * Bepaalt de seconde binnen de minuut. Geeft <tt>null</tt> als datum
	 * <tt>null</tt> is.
	 * 
	 * @param datum
	 * @return de seconden binnen de minuut.
	 */
	public static Integer seconde(Date datum)
	{
		return getDatumVeld(datum, Calendar.SECOND);
	}

	/**
	 * Bepaalt de milliseconde binnen de seconde. Geeft <tt>null</tt> als datum
	 * <tt>null</tt> is.
	 * 
	 * @param datum
	 * @return de milliseconden binnen de seconde.
	 */
	public static Integer milliseconde(Date datum)
	{
		return getDatumVeld(datum, Calendar.MILLISECOND);
	}

	/**
	 * Verwijdert de tijd informatie van de datum zodat er makkelijk met datums
	 * vergeleken kan worden. Zo worden "1990-01-01 00:13:12.123" en
	 * "1990-01-01 00:13:12.124" gelijke datums.
	 * 
	 * @param datum
	 * @return de datum zonder tijd.
	 */
	public static Date verwijderTijd(Date datum)
	{
		if (datum == null)
			return null;
		Calendar cal = calendar(datum);
		cal.set(Calendar.HOUR_OF_DAY, 0);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.MILLISECOND, 0);
		return cal.getTime();
	}

	/**
	 * Bepaalt de datum informatie van de datum. De tijd informatie is dus
	 * verwijderd.
	 * 
	 * @param datum
	 * @return de datum zonder tijd.
	 */
	public static Date datumVan(Date datum)
	{
		return verwijderTijd(datum);
	}

	/**
	 * Verwijdert de datum informate van de datum zodat er alleen de relevante
	 * tijdgegevens overblijven. Hierdoor is het mogelijk om tijdstippen op
	 * verschillende dagen met elkaar te vergelijken.
	 * 
	 * @param datum
	 * @return de tijd zonder datum.
	 */
	public static Date verwijderDatum(Date datum)
	{
		if (datum == null)
			return null;
		Calendar cal = calendar(datum);
		cal.set(Calendar.YEAR, 1970);
		cal.set(Calendar.MONTH, 0);
		cal.set(Calendar.DAY_OF_MONTH, 1);
		return cal.getTime();
	}

	/**
	 * Bepaalt de relevante tijdgegevens van de datum. Hierdoor is het mogelijk
	 * om tijdstippen op verschillende dagen met elkaar te vergelijken.
	 * 
	 * @param datum
	 * @return de tijd van de datum.
	 */
	public static Date tijdVan(Date datum)
	{
		return verwijderDatum(datum);
	}

	private static Integer getDatumVeld(Date datum, int field)
	{
		if (datum != null)
			return calendar(datum).get(field);
		return null;
	}

	private static Calendar calendar(Date datum)
	{
		Calendar calendar = defaultCalendar();
		calendar.setTime(datum);
		return calendar;
	}

	/**
	 * @return de standaard calendar.
	 */
	public static Calendar defaultCalendar()
	{
		Calendar calendar = Calendar.getInstance();
		if (calendar instanceof GregorianCalendar)
			calendar.set(Calendar.ERA, GregorianCalendar.AD);
		calendar.setMinimalDaysInFirstWeek(4);
		calendar.setFirstDayOfWeek(Calendar.MONDAY);
		return calendar;
	}

	/**
	 * Bepaalt of datum1 op of na datum2 ligt.
	 * 
	 * @param datum1
	 * @param datum2
	 * @return resultaat.
	 */
	public static boolean opOfNa(Date datum1, Date datum2)
	{
		if (datum1 != null && datum2 != null)
		{
			datum1 = datumVan(datum1);
			datum2 = datumVan(datum2);

			return datum1.equals(datum2) || datum1.after(datum2);
		}
		return false;
	}

	/**
	 * Bepaalt of datum1 na datum2 ligt.
	 * 
	 * @param datum1
	 * @param datum2
	 * @return resultaat.
	 */
	public static boolean na(Date datum1, Date datum2)
	{
		if (datum1 != null && datum2 != null)
		{
			datum1 = datumVan(datum1);
			datum2 = datumVan(datum2);

			return datum1.after(datum2);
		}
		return false;
	}

	/**
	 * Bepaalt of datum1 op of voor datum2 ligt.
	 * 
	 * @param datum1
	 * @param datum2
	 * @return resultaat.
	 */
	public static boolean opOfVoor(Date datum1, Date datum2)
	{
		if (datum1 != null && datum2 != null)
		{
			datum1 = datumVan(datum1);
			datum2 = datumVan(datum2);

			return datum1.equals(datum2) || datum1.before(datum2);
		}
		return false;
	}

	/**
	 * @param datum1
	 * @param datum2
	 * @return resultaat.
	 */
	public static boolean voor(Date datum1, Date datum2)
	{
		if (datum1 != null && datum2 != null)
		{
			datum1 = datumVan(datum1);
			datum2 = datumVan(datum2);

			return datum1.before(datum2);
		}
		return false;
	}

	/**
	 * @param peildatum
	 * @param datums
	 * @return resultaat.
	 */
	public static boolean allenNa(Date peildatum, Date... datums)
	{
		if (peildatum != null && datums != null && datums.length != 0)
		{
			for (Date datum : datums)
			{
				if (!na(datum, peildatum))
					return false;
			}
			return true;
		}
		return false;
	}

	/**
	 * @param peildatum
	 * @param datums
	 * @return resultaat.
	 */
	public static boolean allenOpOfNa(Date peildatum, Date... datums)
	{
		if (peildatum != null && datums != null && datums.length != 0)
		{
			for (Date datum : datums)
			{
				if (!opOfNa(datum, peildatum))
					return false;
			}
			return true;
		}
		return false;
	}

	/**
	 * @param peildatum
	 * @param datums
	 * @return resultaat.
	 */
	public static boolean allenVoor(Date peildatum, Date... datums)
	{
		if (peildatum != null && datums != null && datums.length != 0)
		{
			for (Date datum : datums)
			{
				if (!voor(datum, peildatum))
					return false;
			}
			return true;
		}
		return false;
	}

	/**
	 * @param peildatum
	 * @param datums
	 * @return resultaat.
	 */
	public static boolean allenOpOfVoor(Date peildatum, Date... datums)
	{
		if (peildatum != null && datums != null && datums.length != 0)
		{
			for (Date datum : datums)
			{
				if (!opOfVoor(datum, peildatum))
					return false;
			}
			return true;
		}
		return false;
	}

	/**
	 * Bepaalt of de peildatum tussen min en max ligt (inclusief). 1 oktober
	 * 2010 ligt dus tussen 1 oktober 2013 en 1 oktober 2013.
	 * 
	 * @param peildatum
	 * @param min
	 * @param max
	 * @return resultaat.
	 */
	public static boolean tussen(Date peildatum, Date min, Date max)
	{
		return opOfNa(peildatum, min) && opOfVoor(peildatum, max);
	}

	/**
	 * @param datum
	 * @return de dag na de gegeven datum.
	 */
	public static Date dagNa(Date datum)
	{
		return addDag(datum, 1);
	}

	/**
	 * @param datum
	 * @return de dag voor de gegeven datum.
	 */
	public static Date dagVoor(Date datum)
	{
		return addDag(datum, -1);
	}

	/**
	 * De datum van de dag voor gisteren (zonder tijd).
	 * 
	 * @return de dag voor gisteren.
	 */
	public static Date eergisteren()
	{
		return addDag(vandaag(), -2);
	}

	/**
	 * De datum van de dag voor vandaag (zonder tijd).
	 * 
	 * @return gisteren.
	 */
	public static Date gisteren()
	{
		return addDag(vandaag(), -1);
	}

	/**
	 * De datum van vandaag (zonder tijd).
	 * 
	 * @return vandaag.
	 */
	public static Date vandaag()
	{
		return verwijderTijd(nu());
	}

	/**
	 * De datum en tijd van <em>nu</em> zonder millisecondes (staan op 0).
	 * 
	 * @return op dit moment.
	 */
	public static Date nu()
	{
		Calendar cal = calendar(klok.nu());
		cal.set(Calendar.MILLISECOND, 0);
		return cal.getTime();
	}

	/**
	 * De datum van de dag na vandaag (zonder tijd).
	 * 
	 * @return morgen.
	 */
	public static Date morgen()
	{
		return addDag(vandaag(), 1);
	}

	/**
	 * De datum van de dag na morgen (zonder tijd).
	 * 
	 * @return de dag na morgen.
	 */
	public static Date overmorgen()
	{
		return addDag(vandaag(), 2);
	}

	/**
	 * Telt het aantal jaren op bij de datum.
	 * 
	 * @param datum
	 * @param aantal
	 * @return opgeteld aantal.
	 */
	public static Date addJaar(Date datum, int aantal)
	{
		return addDatumVeld(datum, Calendar.YEAR, aantal);
	}

	/**
	 * Telt het aantal maanden op bij de datum.
	 * 
	 * @param datum
	 * @param aantal
	 * @return opgeteld aantal.
	 */
	public static Date addMaand(Date datum, int aantal)
	{
		return addDatumVeld(datum, Calendar.MONTH, aantal);
	}

	/**
	 * Telt het aantal weken op bij de datum.
	 * 
	 * @param datum
	 * @param aantal
	 * @return opgeteld aantal.
	 */
	public static Date addWeek(Date datum, int aantal)
	{
		return addDatumVeld(datum, Calendar.WEEK_OF_YEAR, aantal);
	}

	/**
	 * Telt het aantal dagen op bij de datum.
	 * 
	 * @param datum
	 * @param aantal
	 * @return opgeteld aantal.
	 */
	public static Date addDag(Date datum, int aantal)
	{
		return addDatumVeld(datum, Calendar.DAY_OF_MONTH, aantal);
	}

	/**
	 * Telt het aantal uren op bij de datum.
	 * 
	 * @param datum
	 * @param aantal
	 * @return opgeteld aantal.
	 */
	public static Date addUur(Date datum, int aantal)
	{
		return addDatumVeld(datum, Calendar.HOUR_OF_DAY, aantal);
	}

	/**
	 * Telt het aantal minuten op bij de datum.
	 * 
	 * @param datum
	 * @param aantal
	 * @return opgeteld aantal.
	 */
	public static Date addMinuut(Date datum, int aantal)
	{
		return addDatumVeld(datum, Calendar.MINUTE, aantal);
	}

	/**
	 * Telt het aantal seconden op bij de datum.
	 * 
	 * @param datum
	 * @param aantal
	 * @return opgeteld aantal.
	 */
	public static Date addSeconde(Date datum, int aantal)
	{
		return addDatumVeld(datum, Calendar.SECOND, aantal);
	}

	/**
	 * Telt het aantal milliseconden op bij de datum.
	 * 
	 * @param datum
	 * @param aantal
	 * @return opgeteld aantal.
	 */
	public static Date addMillis(Date datum, int aantal)
	{
		return addDatumVeld(datum, Calendar.MILLISECOND, aantal);
	}

	/**
	 * Telt het aantal op bij het veld van de datum.
	 */
	private static Date addDatumVeld(Date datum, int veld, int aantal)
	{
		if (datum == null)
			return null;
		Calendar cal = calendar(datum);
		cal.add(veld, aantal);
		return cal.getTime();
	}

	/**
	 * Bepaalt het maximum van de opgegeven datums.
	 * 
	 * @param dates
	 * @return de hoogste datum.
	 */
	public static Date max(Date... dates)
	{
		if (dates == null || dates.length == 0)
			return null;

		Date max = dates[0];
		for (int i = 1; i < dates.length; i++)
		{
			Date current = dates[i];
			if (max == null)
				max = current;
			else if (current != null && current.after(max))
				max = current;
		}
		return max;
	}

	/**
	 * Bepaalt het minimum van de opgegeven datums.
	 * 
	 * @param dates
	 * @return de laagste datum.
	 */
	public static Date min(Date... dates)
	{
		if (dates == null || dates.length == 0)
			return null;

		Date max = dates[0];
		for (int i = 1; i < dates.length; i++)
		{
			Date current = dates[i];
			if (max == null)
				max = current;
			else if (current != null && current.before(max))
				max = current;
		}
		return max;
	}

	/**
	 * Formatteert de datum als een string volgens de standaard notatie.
	 * 
	 * @param date
	 * @return de datum volgens het standaard formaat.
	 */
	public static String asString(Date date)
	{
		return formatDate(date);
	}

	/**
	 * Formatteert de datum als een string volgens het gegeven formaat.
	 * 
	 * @param date
	 * @param format
	 * @return datum volgens het formaat.
	 */
	public static String asString(Date date, String format)
	{
		return formatDate(date, format);
	}

	/**
	 * Converteert de opgegeven tekst naar een date object.
	 * 
	 * @param date
	 * @return String to date.
	 */
	public static Date toDate(String date)
	{
		return parseDateString(date);
	}
}
