package nl.mypalsports.common.util.factory;

import java.lang.reflect.Field;
import java.lang.reflect.Method;

import nl.mypalsports.common.util.Asserts;
import nl.mypalsports.common.util.EmbeddedProperty;
import nl.mypalsports.common.util.Property;

/**
 * @author Niels
 * 
 * @param <X>
 */
public class EmbeddedPropertyFactory<X> implements IPropertyFactory<X>
{
	private Property<X, ?, ?> parentProperty;

	/**
	 * Constructor.
	 * 
	 * @param parent
	 */
	public EmbeddedPropertyFactory(Property<X, ?, ?> parent)
	{
		Asserts.assertNotEmpty("parent", parent);

		this.parentProperty = parent;
	}

	@Override
	public Property<X, ?, ?> createProperty(Method method)
	{
		return new EmbeddedProperty<X, Object, Object>(parentProperty, method);
	}

	@Override
	public Property<X, ?, ?> createProperty(Field field)
	{
		return new EmbeddedProperty<X, Object, Object>(parentProperty, field);
	}

	@Override
	public <Y, Z> Property<X, Y, Z> createProperty(Class<Y> declaringClass, String name, Class<Z> type, Field field,
			Method getMethod, Method setMethod)
	{
		return new EmbeddedProperty<X, Y, Z>(parentProperty, declaringClass, name, type, field, getMethod, setMethod);
	}
}
