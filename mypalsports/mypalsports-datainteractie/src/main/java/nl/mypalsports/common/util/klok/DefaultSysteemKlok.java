package nl.mypalsports.common.util.klok;

import java.util.Date;

/**
 * Standaard Tijd.
 * 
 * @author Niels
 * 
 */
public class DefaultSysteemKlok implements Klok
{
	@Override
	public Date nu()
	{
		return new Date();
	}
}
