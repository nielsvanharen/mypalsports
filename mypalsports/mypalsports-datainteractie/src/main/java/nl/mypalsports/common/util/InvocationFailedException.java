package nl.mypalsports.common.util;

/**
 * Invoken van call mislukt.
 * @author Niels
 *
 */
public class InvocationFailedException extends RuntimeException
{
	private static final long serialVersionUID = 1L;

	/**
	 * Constructor.
	 */
	public InvocationFailedException()
	{
	}

	/**
	 * Constructor.
	 * @param message
	 */
	public InvocationFailedException(String message)
	{
		super(message);
	}

	/**
	 * Constructor.
	 * @param cause
	 */
	public InvocationFailedException(Throwable cause)
	{
		super(cause);
	}

	/**
	 * Constructor.
	 * @param message
	 * @param cause
	 */
	public InvocationFailedException(String message, Throwable cause)
	{
		super(message, cause);
	}

	/**
	 * @return of het door deze exception is veroorzaakt.
	 */
	public boolean isCausedByMethodNotFound()
	{
		return getCause() instanceof MethodNotFoundException;
	}
}
