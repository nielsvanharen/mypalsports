package nl.mypalsports.common.util;

import java.util.Arrays;
import java.util.List;

/**
 * @author Niels
 * 
 */
public class AmbiguousMethodException extends RuntimeException
{
	private static final long serialVersionUID = 1L;

	/**
	 * Constructor.
	 * 
	 * @param applicable
	 * @param args
	 */
	public <T> AmbiguousMethodException(List<Callable<T>> applicable, Class<?>... args)
	{
		super("Method invocation is ambiguous for " + Arrays.asList(args) + ". Applicable methods: " + applicable);
	}

}
