package nl.mypalsports.common.util.klok;

import java.util.Date;

/**
 * Clock interface.
 * @author Niels
 *
 */
public interface Klok
{
	/**
	 * @return current time.
	 */
	Date nu();
}
