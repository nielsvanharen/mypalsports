package nl.mypalsports.common.util.klok;

import java.util.Date;

/**
 * Mock voor standaard tijd.
 * @author Niels
 *
 */
public class MockSysteemKlok implements Klok
{
	private Date dummyTijd;

	/**
	 * @param dummyTijd
	 */
	public void setDummyTijd(Date dummyTijd)
	{
		this.dummyTijd = dummyTijd;
	}

	@Override
	public Date nu()
	{
		if (dummyTijd == null)
			return new Date();
		return dummyTijd;
	}
}
