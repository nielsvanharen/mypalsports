package nl.mypalsports.common.util.klok;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import nl.mypalsports.common.util.TimeUtil;

/**
 * @author Niels
 */
@SuppressWarnings("javadoc")
public enum Dag
{
	Maandag
	{
		@Override
		public int getDagNummer()
		{
			return 1;
		}
	},
	Dinsdag
	{
		@Override
		public int getDagNummer()
		{
			return 2;
		}
	},
	Woensdag
	{
		@Override
		public int getDagNummer()
		{
			return 3;
		}
	},
	Donderdag
	{
		@Override
		public int getDagNummer()
		{
			return 4;
		}
	},
	Vrijdag
	{
		@Override
		public int getDagNummer()
		{
			return 5;
		}
	},
	Zaterdag
	{
		@Override
		public int getDagNummer()
		{
			return 6;
		}
	},
	Zondag
	{
		@Override
		public int getDagNummer()
		{
			return 0;
		}
	};

	/**
	 * Geeft het dagnummer zoals dit in de meeste importbestanden e.d. wordt gebruikt.
	 * Maandag = 1, Dinsdag = 2 etc.
	 * @return het dagnummer.
	 * 
	 */
	public abstract int getDagNummer();

	/**
	 * Geeft een afkorting van de gegeven dag. Bijv: Ma, Di, Wo, Do, Vr, Za, Zo.
	 * @return de afkorting.
	 */
	public String getAfkorting()
	{
		return toString().substring(0, 2);
	}

	/**
	 * Geeft de dag met het gegeven nummer.
	 * @param nummer 
	 */
	public static Dag getDag(int nummer)
	{
		for (Dag dag : Dag.values())
		{
			if (dag.getDagNummer() == nummer)
			{
				return dag;
			}
		}
		return null;
	}

	/**
	 * Geeft de weekdag van de gegeven datum.
	 * @param date 
	 * @return  de dag.
	 */
	public static Dag getDag(Date date)
	{
		return getDag(TimeUtil.getDayOfWeek(date) - 1);
	}

	private static final List<Dag> werkDagen = new ArrayList<Dag>(5);
	static
	{
		werkDagen.add(Maandag);
		werkDagen.add(Dinsdag);
		werkDagen.add(Woensdag);
		werkDagen.add(Donderdag);
		werkDagen.add(Vrijdag);
	}

	/**
	 * Geeft een lijst met alle werkdagen (Ma t/m Vr).
	 * @return de lijst.
	 */
	public static List<Dag> getWerkDagen()
	{
		return werkDagen;
	}

	/**
	 * @param cached
	 * @return de lijst van dagen.
	 */
	public static List<Dag> getDagen(String cached)
	{
		if (cached == null)
			return null;
		List<Dag> ret = new ArrayList<Dag>();
		String[] dagen = (cached).split(",");
		for (String dag : dagen)
		{
			if (!dag.isEmpty())
				ret.add(Dag.valueOf(dag));
		}

		return ret;
	}

	/**
	 * @param value
	 * @return de waarde.
	 */
	public static String getWaarde(List<Dag> value)
	{
		String ret = new String();
		for (Dag dag : value)
		{
			if (dag != null)
				ret = ret.concat(dag.toString() + ",");
		}
		if (!ret.isEmpty())
			ret = ret.substring(0, ret.length() - 1);
		return ret;
	}

}
