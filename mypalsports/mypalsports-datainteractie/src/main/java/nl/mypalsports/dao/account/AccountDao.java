/**
 * 
 */
package nl.mypalsports.dao.account;

import nl.mypalsports.dao.common.GenericDao;
import nl.mypalsports.entities.account.Account;

/**
 * @author Niels
 * 
 */
public interface AccountDao extends GenericDao<Account>
{

}
