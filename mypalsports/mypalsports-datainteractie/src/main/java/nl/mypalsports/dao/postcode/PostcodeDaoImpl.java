/**
 * 
 */
package nl.mypalsports.dao.postcode;

import nl.mypalsports.dao.common.GenericDaoImpl;
import nl.mypalsports.entities.adres.Postcode;

import org.springframework.stereotype.Repository;

/**
 * @author Niels
 *
 */
@Repository
public class PostcodeDaoImpl extends GenericDaoImpl<Postcode> implements PostcodeDao
{
	private static final long serialVersionUID = 1L;

}
