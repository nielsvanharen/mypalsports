/**
 * 
 */
package nl.mypalsports.dao.adres;

import nl.mypalsports.dao.common.GenericDaoImpl;
import nl.mypalsports.entities.adres.Adres;

import org.springframework.stereotype.Repository;

/**
 * @author Niels
 * 
 */
@Repository
public class AdresDaoImpl extends GenericDaoImpl<Adres> implements AdresDao
{

	private static final long serialVersionUID = 1L;

}
