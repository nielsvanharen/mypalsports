/**
 * 
 */
package nl.mypalsports.dao.adres;

import nl.mypalsports.dao.common.GenericDao;
import nl.mypalsports.entities.adres.Adres;

/**
 * @author Niels
 *
 */
public interface AdresDao extends GenericDao<Adres>
{

}
