/**
 * 
 */
package nl.mypalsports.dao.common;

import java.io.Serializable;

import nl.mypalsports.common.exception.DataAccessException;
import nl.mypalsports.entities.common.idObject;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.orm.hibernate4.LocalSessionFactoryBean;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author Niels
 * @param <T>
 * 
 */
public abstract class GenericDaoImpl<T extends idObject> implements
		GenericDao<T> {

	private static final long serialVersionUID = 1L;

	private static Logger log = LoggerFactory.getLogger(GenericDaoImpl.class);

	@Autowired
	private SessionFactory sessionFactory;

	@Autowired
	private ApplicationContext context;

	/*
	 * (non-Javadoc)
	 * 
	 * @see nl.mypalsports.common.dao.GenericDao#save(nl.reddot.groover.entities
	 * .Entiteit)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public Serializable save(T dataObject) {
		try {
			return getSession().save(dataObject);
		} catch (HibernateException e) {
			String message = "Failed to batch save " + dataObject;
			log.error(message, e);
			getSession().getTransaction().rollback();
			throw new DataAccessException(message, e);
		} catch (RuntimeException e) {
			getSession().getTransaction().rollback();
			throw new DataAccessException(e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * nl.mypalsports.common.dao.GenericDao#delete(nl.reddot.groover.entities
	 * .Entiteit)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public void delete(T dataObject) {
		try {
			getSession().delete(dataObject);
		} catch (HibernateException e) {
			String message = "Failed to batch delete " + dataObject;
			log.error(message, e);
			getSession().getTransaction().rollback();
			throw new DataAccessException(message, e);
		} catch (RuntimeException e) {
			getSession().getTransaction().rollback();
			throw new DataAccessException(e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see nl.mypalsports.common.dao.GenericDao#get(java.lang.Class,
	 * java.io.Serializable)
	 */
	@SuppressWarnings("unchecked")
	@Override
	@Transactional(readOnly = true)
	public T get(Class<T> clazz, Serializable id) {
		try {
			return (T) getSession().get(clazz, id);
		} catch (HibernateException e) {
			String message = "Failed to get a(n)" + clazz + " with id " + id;
			log.error(message, e);
			throw new DataAccessException(message, e);
		} catch (RuntimeException e) {
			log.error(e.toString(), e);
			throw new DataAccessException(e);
		}
	}

	/**
	 * 
	 * @return de current session.
	 */
	public Session getSession() {
		return this.sessionFactory.getCurrentSession();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see nl.mypalsports.common.dao.GenericDao#saveOrUpdate(nl.reddot.groover
	 * .entities.idObject)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public void saveOrUpdate(T dataObject) {
		try {
			getSession().saveOrUpdate(dataObject);
		} catch (HibernateException e) {
			String message = "Failed to batch save or update " + dataObject;
			log.error(message, e);
			getSession().getTransaction().rollback();
			throw new DataAccessException(message, e);
		} catch (RuntimeException e) {
			getSession().getTransaction().rollback();
			throw new DataAccessException(e);
		}
	}

	/**
	 * @param clazz
	 * @return de criteria.
	 */
	@Transactional(readOnly = true)
	public Criteria createCriteria(Class<T> clazz) {
		return getSession().createCriteria(clazz);
	}

	@Override
	public Configuration getConfiguration() {
		LocalSessionFactoryBean bean = (LocalSessionFactoryBean) context
				.getBean("&sessionFactory");
		return bean.getConfiguration();
	}

}
