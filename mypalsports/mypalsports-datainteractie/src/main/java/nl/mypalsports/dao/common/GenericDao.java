/**
 * 
 */
package nl.mypalsports.dao.common;

import java.io.Serializable;

import nl.mypalsports.entities.common.idObject;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.cfg.Configuration;

/**
 * @author Niels
 * @param <T>
 * 
 */
public interface GenericDao<T extends idObject> extends Serializable
{
	/**
	 * Standaard save actie.
	 * 
	 * @param dataObject
	 * @return persistent dataObject
	 */
	Serializable save(T dataObject);

	/**
	 * Delete het object.
	 * 
	 * @param dataObject
	 */
	void delete(T dataObject);

	/**
	 * get op basis van een id.
	 * 
	 * @param clazz
	 * @param id
	 * @return het dataObject.
	 */
	T get(Class<T> clazz, Serializable id);

	/**
	 * saven of updaten van het object.
	 * 
	 * @param dataObject
	 */
	void saveOrUpdate(T dataObject);

	/**
	 * 
	 * @param clazz
	 * @return criteria.
	 */
	Criteria createCriteria(Class<T> clazz);

	/**
	 * 
	 * @return de current hibernate session.
	 */
	Session getSession();
	
	
	/**
	 * @return de hibernate configuratie.
	 */
	Configuration getConfiguration();

}
