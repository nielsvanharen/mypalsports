/**
 * 
 */
package nl.mypalsports.dao.persoon;

import nl.mypalsports.dao.common.GenericDao;
import nl.mypalsports.entities.persoon.Persoon;

/**
 * @author Niels
 *
 */
public interface PersoonDao extends GenericDao<Persoon>
{

}
