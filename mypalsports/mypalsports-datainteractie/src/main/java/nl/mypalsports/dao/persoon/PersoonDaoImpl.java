/**
 * 
 */
package nl.mypalsports.dao.persoon;

import nl.mypalsports.dao.common.GenericDaoImpl;
import nl.mypalsports.entities.persoon.Persoon;

import org.springframework.stereotype.Repository;

/**
 * @author Niels
 * 
 */
@Repository
public class PersoonDaoImpl extends GenericDaoImpl<Persoon> implements PersoonDao
{
	private static final long serialVersionUID = 1L;

}
