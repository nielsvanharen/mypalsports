/**
 * 
 */
package nl.mypalsports.dao.account;

import nl.mypalsports.dao.common.GenericDaoImpl;
import nl.mypalsports.entities.account.Account;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author Niels
 *
 */
@Repository
@Transactional
public class AccountDaoImpl extends GenericDaoImpl<Account> implements AccountDao
{

	private static final long serialVersionUID = 1L;
}
