/**
 * 
 */
package nl.mypalsports.dao.postcode;

import nl.mypalsports.dao.common.GenericDao;
import nl.mypalsports.entities.adres.Postcode;

/**
 * @author Niels
 *
 */
public interface PostcodeDao extends GenericDao<Postcode>
{

}
