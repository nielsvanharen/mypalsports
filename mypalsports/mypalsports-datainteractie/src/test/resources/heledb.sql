CREATE TABLE ACCOUNT
(
	ID BIGINT GENERATED BY DEFAULT AS IDENTITY PRIMARY KEY ,
	CREATED_AT DATE, 
	LAST_MODIFIED_AT DATE,
	GEBRUIKERSNAAM VARCHAR(100),
	ENCRYPTEDPASSWORD VARCHAR(100),
	CREATED_BY BIGINT,
	PERSOON_FK BIGINT,
	LAST_MODIFIED_BY BIGINT,
	EMAIL VARCHAR(100)
);

CREATE TABLE PERSOON
(
	ID BIGINT GENERATED BY DEFAULT AS IDENTITY PRIMARY KEY ,
	CREATED_AT DATE, 
	LAST_MODIFIED_AT DATE,
	ACHTERNAAM VARCHAR(100),
	VOORNAAM VARCHAR(100),
	ADRES_FK BIGINT,
	CREATED_BY BIGINT,
	DTYPE VARCHAR(100),
	LAST_MODIFIED_BY BIGINT
);

CREATE TABLE ADRES
(
	ID BIGINT GENERATED BY DEFAULT AS IDENTITY PRIMARY KEY ,
	CREATED_AT DATE, 
	LAST_MODIFIED_AT DATE,
	HUISNUMMER BIGINT,
	PLAATS VARCHAR(100),
	STRAATNAAM VARCHAR(100),
	POSTCODE_FK BIGINT,
	CREATED_BY BIGINT,
	LAST_MODIFIED_BY BIGINT
);

CREATE TABLE POSTCODE 
(
  ID BIGINT GENERATED BY DEFAULT AS IDENTITY PRIMARY KEY,
  CREATED_AT DATE DEFAULT NULL,
  LAST_MODIFIED_AT DATE DEFAULT NULL,
  HOOG VARCHAR(255) DEFAULT NULL,
  LAAG VARCHAR(255) DEFAULT NULL,
  LATITUDE VARCHAR(255) DEFAULT NULL,
  LONGITUDE VARCHAR(255) DEFAULT NULL,
  PLAATS VARCHAR(255) DEFAULT NULL,
  POSTCODE VARCHAR(255) DEFAULT NULL,
  STRAAT VARCHAR(255) DEFAULT NULL,
  TYPE VARCHAR(255) DEFAULT NULL,
  CREATED_BY BIGINT DEFAULT NULL,
  LAST_MODIFIED_BY BIGINT DEFAULT NULL
);

ALTER TABLE ACCOUNT ADD FOREIGN KEY (PERSOON_FK) REFERENCES PERSOON(ID);
ALTER TABLE PERSOON ADD FOREIGN KEY (ADRES_FK) REFERENCES ADRES(ID);
ALTER TABLE ADRES ADD FOREIGN KEY (POSTCODE_FK) REFERENCES POSTCODE(ID);