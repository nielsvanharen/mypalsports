/**
 * 
 */
package nl.mypalsports.generic.dao.postcode;

import junit.framework.Assert;
import nl.mypalsports.dao.postcode.PostcodeDao;
import nl.mypalsports.entities.adres.Postcode;
import nl.mypalsports.generic.tester.AbstractTester;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author Niels
 * 
 */
@Ignore
public class PostcodeDaoTest extends AbstractTester
{
	@Autowired
	private PostcodeDao dao;

	/**
	 * 
	 */
	@Test
	public void readPostcodeByPostcode()
	{
		Criteria criteria = this.dao.createCriteria(Postcode.class);
		criteria.add(Restrictions.eq("postcode", "1234 HW"));
		Postcode postcode = (Postcode) criteria.uniqueResult();
		Assert.assertNotNull(postcode);
	}
}
