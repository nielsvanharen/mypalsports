/**
 * 
 */
package nl.mypalsports.generic.dao.account;

import junit.framework.Assert;
import nl.mypalsports.dao.account.AccountDao;
import nl.mypalsports.entities.account.Account;
import nl.mypalsports.generic.tester.AbstractTester;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author Niels
 * 
 */
@Ignore
public class AccountDaoTest extends AbstractTester
{
	@Autowired
	private AccountDao dao;

	/**
	 * 
	 */
	@Test
	public void readAccount()
	{
		Account account = this.dao.get(Account.class, new Long(1));
		Assert.assertNotNull(account);
	}
	
	/**
	 * 
	 */
	@Test
	public void readAccountByUsername()
	{
		Criteria criteria = this.dao.createCriteria(Account.class);
		criteria.add(Restrictions.eq("gebruikersnaam", "TEST01"));
		Account account = (Account) criteria.uniqueResult();
		Assert.assertNotNull(account);
	}
}
