package nl.mypalsports.generic.mock;

/**
 * 
 */

import nl.mypalsports.dao.common.GenericDaoImpl;
import nl.mypalsports.entities.account.Account;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author Niels
 * 
 */
@Repository
@Transactional
public class MockGenericDao extends GenericDaoImpl<Account>
{

	static final long serialVersionUID = 1L;



}
