/**
 * 
 */
package nl.mypalsports.generic.dao;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.io.Serializable;

import nl.mypalsports.dao.account.AccountDao;
import nl.mypalsports.entities.account.Account;
import nl.mypalsports.generic.tester.AbstractTester;

import org.hibernate.Criteria;
import org.hibernate.cfg.Configuration;
import org.hibernate.criterion.Restrictions;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author Niels
 * 
 */
@Ignore
public class GenericDaoTest extends AbstractTester
{
	@Autowired
	private AccountDao dao;

	/**
	 * Setup test.
	 * 
	 * @throws Exception
	 */
	@Before
	public void setUp() throws Exception
	{

	}

	/**
	 * save actie.
	 */
	@Test
	public void testSave()
	{
		Account account = createEntiteit(new Long(2));
		Serializable id = dao.save(account);
		assertEquals(new Long(2), id);
	}

	/**
	 * 
	 */
	@Test
	public void testFindById()
	{
		Serializable id = dao.get(Account.class, new Long(1));
		assertNotNull(id);
	}

	/**
	 * 
	 */
	@Test
	public void testNotFoundById()
	{
		Serializable id = dao.get(Account.class, new Long(3));
		assertNull(id);
	}
	
	/**
	 * 
	 */
	@Test
	public void testgetConfiguration()
	{
		Configuration config = dao.getConfiguration();
		assertNotNull(config);
	}

	/**
	 * 
	 */
	@Test
	public void testFindByCriteria()
	{
		Criteria criteria = dao.createCriteria(Account.class);
		criteria.add(Restrictions.eq("id", new Long(1)));
		Account account = (Account) criteria.uniqueResult();
		assertEquals(new Long(1), account.getId());
	}

	private Account createEntiteit(Long id)
	{
		Account entiteit = new Account();
		entiteit.setId(id);
		return entiteit;
	}

}
