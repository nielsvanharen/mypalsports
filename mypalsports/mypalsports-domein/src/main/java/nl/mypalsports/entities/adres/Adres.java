/**
 * 
 */
package nl.mypalsports.entities.adres;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import nl.mypalsports.entities.common.Entiteit;

/**
 * @author Niels
 * 
 */
@Entity
@Table(name = "ADRES")
public class Adres extends Entiteit
{

	private static final long serialVersionUID = 1L;

	@Column(name = "PLAATS", length = 100)
	private String plaats;

	@ManyToOne
	@JoinColumn(name = "POSTCODE_FK")
	private Postcode postcode;

	@Column(name = "HUISNUMMER", length = 4)
	private String huisnummer;

	@Column(name = "STRAATNAAM", length = 100)
	private String straatnaam;

	/**
	 * @return plaats
	 */
	public String getPlaats()
	{
		return plaats;
	}

	/**
	 * @param plaats
	 */
	public void setPlaats(String plaats)
	{
		this.plaats = plaats;
	}

	/**
	 * @return postcode
	 */
	public Postcode getPostcode()
	{
		return postcode;
	}

	/**
	 * @param postcode
	 */
	public void setPostcode(Postcode postcode)
	{
		this.postcode = postcode;
	}

	/**
	 * @return huisnummer
	 */
	public String getHuisnummer()
	{
		return huisnummer;
	}

	/**
	 * @param huisnummer
	 */
	public void setHuisnummer(String huisnummer)
	{
		this.huisnummer = huisnummer;
	}

	/**
	 * @return straatnaam
	 */
	public String getStraatnaam()
	{
		return straatnaam;
	}

	/**
	 * @param straatnaam
	 */
	public void setStraatnaam(String straatnaam)
	{
		this.straatnaam = straatnaam;
	}

}
