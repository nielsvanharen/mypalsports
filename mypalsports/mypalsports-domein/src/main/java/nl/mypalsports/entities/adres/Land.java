package nl.mypalsports.entities.adres;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

import nl.mypalsports.entities.common.Entiteit;

@Entity
@Table(name = "LAND")
public class Land extends Entiteit {

	private static final long serialVersionUID = 1L;

	@NotNull
	@Column(name = "LANDCODE", nullable = false, length = 4)
	private String landCode;

	@Column(name = "OMSCHRIJVING", nullable = false)
	@NotNull
	private String omschrijving;

	@Temporal(TemporalType.DATE)
	@Column(name = "DATUMINGANG")
	private Date datumIngang;

	@Temporal(TemporalType.DATE)
	@Column(name = "DATUMEINDE")
	private Date datumEinde;
	
	@Temporal(TemporalType.DATE)
	@Column(name = "FICTIEFDATUMEINDE")
	private Date fictiefDatumEinde;

	public String getLandCode() {
		return landCode;
	}

	public void setLandCode(String landCode) {
		this.landCode = landCode;
	}

	public String getOmschrijving() {
		return omschrijving;
	}

	public void setOmschrijving(String omschrijving) {
		this.omschrijving = omschrijving;
	}

	public Date getDatumIngang() {
		return datumIngang;
	}

	public void setDatumIngang(Date datumIngang) {
		this.datumIngang = datumIngang;
	}

	public Date getDatumEinde() {
		return datumEinde;
	}

	public void setDatumEinde(Date datumEinde) {
		this.datumEinde = datumEinde;
	}

	public Date getFictiefDatumEinde() {
		return fictiefDatumEinde;
	}

	public void setFictiefDatumEinde(Date fictiefDatumEinde) {
		this.fictiefDatumEinde = fictiefDatumEinde;
	}
	

}
