/**
 * 
 */
package nl.mypalsports.entities.persoon;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Max;
import javax.validation.constraints.NotNull;

import nl.mypalsports.entities.adres.Adres;
import nl.mypalsports.entities.common.Entiteit;

/**
 * @author Niels
 * 
 */
@Entity
@Table(name = "PERSOON")
public class Persoon extends Entiteit {

	public enum AanhefEnum {

		DHR,

		MEVR;
	}

	private static final long serialVersionUID = 1L;

	@Column(name = "AANHEF", length = 10, nullable = false)
	@Enumerated(EnumType.STRING)
	@NotNull
	private AanhefEnum aanhef = AanhefEnum.DHR;

	@Column(name = "TITEL", length = 100)
	@Max(value = 100)
	private String titel;

	@Column(name = "ACHTERNAAM", length = 100, nullable = false)
	@NotNull
	private String achternaam;

	@Column(name = "VOORNAAM", length = 100, nullable = false)
	@NotNull
	private String voornaam;

	@Column(name = "VOORLETTERS", length = 6)
	@Max(value = 6)
	private String voorletters;

	@Column(name = "VOORVOEGSEL", length = 6)
	@Max(value = 6)
	private String voorvoegsel;

	@Column(name = "DTYPE", length = 100, insertable = false, updatable = false)
	private String dtype;

	@ManyToOne
	@JoinColumn(name = "ADRES_FK")
	private Adres adres;

	@Column(name = "GEBOORTEDATUM")
	@Temporal(TemporalType.DATE)
	private Date geboortedatum;

	/**
	 * @return achternaam
	 */
	public String getAchternaam() {
		return achternaam;
	}

	/**
	 * @param achternaam
	 */
	public void setAchternaam(String achternaam) {
		this.achternaam = achternaam;
	}

	/**
	 * @return voornaam
	 */
	public String getVoornaam() {
		return voornaam;
	}

	/**
	 * @param voornaam
	 */
	public void setVoornaam(String voornaam) {
		this.voornaam = voornaam;
	}

	/**
	 * @return the dtype
	 */
	public String getDtype() {
		return dtype;
	}

	/**
	 * @param dtype
	 *            the dtype to set
	 */
	public void setDtype(String dtype) {
		this.dtype = dtype;
	}

	/**
	 * @return the adres
	 */
	public Adres getAdres() {
		return adres;
	}

	/**
	 * @param adres
	 *            the adres to set
	 */
	public void setAdres(Adres adres) {
		this.adres = adres;
	}

	public AanhefEnum getAanhef() {
		return aanhef;
	}

	public void setAanhef(AanhefEnum aanhef) {
		this.aanhef = aanhef;
	}

	public String getTitel() {
		return titel;
	}

	public void setTitel(String titel) {
		this.titel = titel;
	}

	public String getVoorletters() {
		return voorletters;
	}

	public void setVoorletters(String voorletters) {
		this.voorletters = voorletters;
	}

	public String getVoorvoegsel() {
		return voorvoegsel;
	}

	public void setVoorvoegsel(String voorvoegsel) {
		this.voorvoegsel = voorvoegsel;
	}

	public Date getGeboortedatum() {
		return geboortedatum;
	}

	public void setGeboortedatum(Date geboortedatum) {
		this.geboortedatum = geboortedatum;
	}

}
