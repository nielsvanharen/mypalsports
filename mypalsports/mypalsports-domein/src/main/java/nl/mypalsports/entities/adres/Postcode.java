/**
 * 
 */
package nl.mypalsports.entities.adres;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import nl.mypalsports.entities.common.Entiteit;

/**
 * @author Niels
 * 
 */
@Table(name = "POSTCODE")
@Entity
public class Postcode extends Entiteit
{
	private static final long serialVersionUID = 1L;
	
	@Column(name="POSTCODE")
	private String postcode;
	
	@Column(name="PLAATS")
	private String plaats;
	
	@Column(name="STRAAT")
	private String straat;
	
	@Column(name="HOOG")
	private String hoog;
	
	@Column(name="LAAG")
	private String laag;
	
	@Column(name="TYPE")
	private String type;

	@Column(name = "LATITUDE", nullable = true)
	private String latitude;

	@Column(name = "LONGITUDE", nullable = true)
	private String longitude;

	/**
	 * @return the postcode
	 */
	public String getPostcode()
	{
		return postcode;
	}

	/**
	 * @param postcode the postcode to set
	 */
	public void setPostcode(String postcode)
	{
		this.postcode = postcode;
	}

	/**
	 * @return the plaats
	 */
	public String getPlaats()
	{
		return plaats;
	}

	/**
	 * @param plaats the plaats to set
	 */
	public void setPlaats(String plaats)
	{
		this.plaats = plaats;
	}

	/**
	 * @return the straat
	 */
	public String getStraat()
	{
		return straat;
	}

	/**
	 * @param straat the straat to set
	 */
	public void setStraat(String straat)
	{
		this.straat = straat;
	}

	/**
	 * @return the hoog
	 */
	public String getHoog()
	{
		return hoog;
	}

	/**
	 * @param hoog the hoog to set
	 */
	public void setHoog(String hoog)
	{
		this.hoog = hoog;
	}

	/**
	 * @return the laag
	 */
	public String getLaag()
	{
		return laag;
	}

	/**
	 * @param laag the laag to set
	 */
	public void setLaag(String laag)
	{
		this.laag = laag;
	}

	/**
	 * @return the type
	 */
	public String getType()
	{
		return type;
	}

	/**
	 * @param type the type to set
	 */
	public void setType(String type)
	{
		this.type = type;
	}

	/**
	 * @return the latitude
	 */
	public String getLatitude()
	{
		return latitude;
	}

	/**
	 * @param latitude the latitude to set
	 */
	public void setLatitude(String latitude)
	{
		this.latitude = latitude;
	}

	/**
	 * @return the longitude
	 */
	public String getLongitude()
	{
		return longitude;
	}

	/**
	 * @param longitude the longitude to set
	 */
	public void setLongitude(String longitude)
	{
		this.longitude = longitude;
	}
	
	
}
