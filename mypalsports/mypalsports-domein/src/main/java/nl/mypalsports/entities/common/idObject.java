package nl.mypalsports.entities.common;

/**
 * 
 */

import java.io.Serializable;

/**
 * @author Niels
 * 
 */
public interface idObject extends Serializable
{
	/**
	 * 
	 * @return het id.
	 */
	Serializable getId();
}
