package nl.mypalsports.entities.persoon;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

import nl.mypalsports.entities.common.Entiteit;

@Entity
@Table(name = "NATIONALITEIT")
public class Nationaliteit extends Entiteit {

	private static final long serialVersionUID = -7713828388078838249L;

	@Column(name = "CODE", nullable = false, length = 4)
	@NotNull
	private String code;

	@Column(name = "OMSCHRIJVING", nullable = false)
	@NotNull
	private String omschrijving;

	@Temporal(TemporalType.DATE)
	@Column(name = "DATUMINGANG")
	private Date datumIngang;

	@Temporal(TemporalType.DATE)
	@Column(name = "DATUMEINDE")
	private Date datumEinde;

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getOmschrijving() {
		return omschrijving;
	}

	public void setOmschrijving(String omschrijving) {
		this.omschrijving = omschrijving;
	}

	public Date getDatumIngang() {
		return datumIngang;
	}

	public void setDatumIngang(Date datumIngang) {
		this.datumIngang = datumIngang;
	}

	public Date getDatumEinde() {
		return datumEinde;
	}

	public void setDatumEinde(Date datumEinde) {
		this.datumEinde = datumEinde;
	}

}
