package nl.mypalsports.entities.account;

/**
 * 
 */

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import nl.mypalsports.entities.common.Entiteit;
import nl.mypalsports.entities.persoon.Persoon;

/**
 * @author Niels
 * 
 */
@Entity
@Table(name = "ACCOUNT")
public class Account extends Entiteit {

	private static final long serialVersionUID = 1L;

	@Column(name = "GEBRUIKERSNAAM", length = 100)
	private String gebruikersnaam;

	@Column(name = "ENCRYPTEDPASSWORD")
	private String wachtwoord;

	@Column(name = "EMAIL", length = 100)
	private String email;

	@OneToOne
	@JoinColumn(name = "PERSOON_FK")
	private Persoon persoon;

	/**
	 * 
	 * @return gebruikersnaam.
	 */
	public String getGebruikersnaam() {
		return gebruikersnaam;
	}

	/**
	 * 
	 * @param gebruikersnaam
	 */
	public void setGebruikersnaam(String gebruikersnaam) {
		this.gebruikersnaam = gebruikersnaam;
	}

	/**
	 * 
	 * @return encryptedwachtwoord.
	 */
	public String getWachtwoord() {
		return wachtwoord;
	}

	/**
	 * 
	 * @param wachtwoord
	 */
	public void setWachtwoord(String wachtwoord) {
		this.wachtwoord = wachtwoord;
	}

	/**
	 * 
	 * @return email adres.
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * 
	 * @param email
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * @return the persoon
	 */
	public Persoon getPersoon() {
		return persoon;
	}

	/**
	 * @param persoon
	 *            the persoon to set
	 */
	public void setPersoon(Persoon persoon) {
		this.persoon = persoon;
	}

}
